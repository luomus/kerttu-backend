from app import app, db
from app.models.models import Audio
import csv
from datetime import datetime

DATA_PATH = 'datat/paikkatiedot.txt'


@app.cli.command('load_location_data')
def load_location_data():
    with open(DATA_PATH) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)  # header
        for row in reader:
            if row[0] == '':
                continue

            audio = get_audio(row[1])
            if audio is None:
                print('Audio {} is missing'.format(row[1]))
                continue

            audio.date_time = datetime.strptime(row[4], '%d.%m.%Y %H:%M')
            audio.latitude = float(row[5])
            audio.longitude = float(row[6])
            audio.municipality = row[7]
            db.session.commit()


def get_audio(file_name):
    return Audio.query.filter(
        Audio.file_name.like(file_name)
    ).first()
