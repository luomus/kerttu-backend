from app import app, db
from app.kerttu_global.models.enums import TaxonTypeEnum
from app.kerttu_global.models.models import GlobalSpecies
import csv

DATA_FILE = 'datat/bats.csv'
TAXON_TYPE = TaxonTypeEnum.BAT
HAS_HEADER = True
NAME_COLUMN = 1


@app.cli.command('load_global_taxon_data_simple')
def load_global_taxon_data_simple():
    with open(DATA_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')

        if HAS_HEADER:
            next(reader)

        for row in reader:
            add_species(row)


def add_species(row):
    species = GlobalSpecies.query.filter(
        GlobalSpecies.scientific_name.like(row[NAME_COLUMN])
    ).first()

    if species is None:
        species = GlobalSpecies(
            scientific_name=row[NAME_COLUMN],
            taxon_type=TAXON_TYPE
        )

        db.session.add(species)
        db.session.commit()
