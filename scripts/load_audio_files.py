from app import app, db
from app.models.models import Audio
import csv

DATA_PATH = 'datat/audiofiles.txt'


# script assigns ids automatically
@app.cli.command('load_audio_files')
def load_audio_files():
    with open(DATA_PATH) as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        for row in reader:
            audio = Audio(file_name=row[1])
            db.session.add(audio)

    db.session.commit()
