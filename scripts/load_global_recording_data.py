from app import app, db
from app.kerttu_global.models.enums import TaxonTypeEnum
from app.kerttu_global.models.models import GlobalRecording, GlobalSpecies, GlobalRecordingGroup
import csv
from datetime import datetime
from sqlalchemy import func

DATA_PATH = 'datat/batwav10s_part10_list.txt'
FORMAT = 5  # 1 = files starting with "A-", 2 = German data, 3 = files starting with "xA-", 4 = insect data, 5 = bat data
SITE_ID = 10001


@app.cli.command('load_global_recording_data')
def load_global_recording_data():
    new_recordings = []
    group_by_name = {}

    with open(DATA_PATH) as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        for row in reader:
            if row[0] == '':
                continue

            if FORMAT == 1:
                url = get_url(row[0])
            else:
                url = get_url_with_prefix(row[0])

            parts = row[0].split('_')
            p1 = float(row[1])
            p2 = float(row[2])
            p3 = float(row[3])

            name_base = row[0].rsplit('_', 1)[0]
            name_last_part = parts[-1].split('.')[0]

            recording = GlobalRecording.query.filter(
                GlobalRecording.url == url,
                GlobalRecording.x1 == p1,
                GlobalRecording.x2 == p1 + p2
            ).first()
            if recording is not None:
                if recording.group is not None:
                    group_by_name[name_base] = recording.group
                continue

            date_time = None
            original_recording_name = None
            original_recording_part = None
            target_species_id = None
            taxon_type = TaxonTypeEnum.BIRD

            if FORMAT == 1:
                site_id = int(parts[0].split('-')[1])
                date_time = datetime.strptime('{} {} UTC'.format(parts[-3], parts[-2]), '%Y%m%d %H%M%S %Z')
            elif FORMAT == 2:
                site_id = int(parts[0][1:])
                original_recording_name = name_base
                original_recording_part = int((int(name_last_part) / 10) + 1)
            elif FORMAT == 3:
                site_id = int(parts[1])
                date_time = datetime.strptime('{} {} UTC'.format(parts[2], parts[3]), '%Y%m%d %H%M%S %Z')
            elif FORMAT in [4, 5, 6]:
                site_id = SITE_ID
                has_last_part = True
                try:
                    original_recording_part = int(name_last_part)
                    original_recording_name = name_base
                except ValueError:
                    has_last_part = False

                if FORMAT == 4:
                    target_species_id = get_target_species_id_for_insect_recording(parts, has_last_part)
                    taxon_type = TaxonTypeEnum.INSECT
                elif FORMAT == 5:
                    target_species_id = get_species_id_for_bat(parts[0], parts[1])
                    taxon_type = TaxonTypeEnum.BAT
                else:
                    target_species_id = get_species_id_for_frog(parts[0], parts[1])
                    taxon_type = TaxonTypeEnum.FROG

            group = None
            if original_recording_name is not None:
                if original_recording_name not in group_by_name:
                    group_by_name[original_recording_name] = GlobalRecordingGroup()
                    db.session.add(group_by_name[original_recording_name])
                    db.session.commit()
                group = group_by_name[original_recording_name]

            recording = GlobalRecording(
                url=url,
                site_id=site_id,
                x1=p1,
                x2=p1 + p2,
                duration=p1 + p2 + p3,
                date_time=date_time,
                group_id=group.id if group is not None else None,
                group_order_number=original_recording_part,
                target_species_id=target_species_id,
                taxon_type=taxon_type
            )

            new_recordings.append(recording)

    db.session.bulk_save_objects(new_recordings)
    db.session.commit()


def get_url(file_name):
    return '{}/{}/{}'.format(
        app.config['IMAGE_LAJI_URL'],
        file_name.split('_')[0],
        file_name
    )


def get_url_with_prefix(file_name):
    return '{}/A-{}/{}'.format(
        app.config['IMAGE_LAJI_URL'],
        file_name.split('_')[0],
        file_name
    )


def get_target_species_id_for_insect_recording(parts, has_last_part):
    name_parts = []
    index = -2 if has_last_part else -1

    while index >= len(parts) * -1:
        name = parts[index].split('.')[0] if index == -1 else parts[index]
        if name == 'Background':
            index -= 2
            continue
        if name not in ['cf', 'crop', 'crop1', 'crop2', 'crop3', 'crop4', 'crop5']:
            name_parts.insert(0, name)
        index -= 1

        if not name.islower():
            break

    if len(name_parts) == 1 and name_parts[0] == 'Aethiomerus':
        name_parts.append('madagassus')

    scientific_name = ' '.join(name_parts)
    if not scientific_name.strip():
        return None

    species = GlobalSpecies.query.filter(
        GlobalSpecies.scientific_name == scientific_name
    ).first()

    return species.id


def get_species_id_for_bat(part1, part2):
    name_map = {
        'Pohjanlepakko': 'Eptesicus nilssonii',
        'Pikkulepakko': 'Pipistrellus nathusii'
    }

    return get_species_id(part1, part2, name_map)


def get_species_id_for_frog(part1, part2):
    name_map = {
        'Pelophylax': 'Pelophylax sp',
        'Bufotes': 'Bufotes sp'
    }

    return get_species_id(part1, part2, name_map)


def get_species_id(part1, part2, name_map):
    name = name_map[part1] if part1 in name_map else part1 + ' ' + part2

    species = GlobalSpecies.query.filter(
        func.lower(GlobalSpecies.scientific_name) == func.lower(name)
    ).first()

    return species.id
