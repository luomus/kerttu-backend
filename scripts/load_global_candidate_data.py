from app import app, db
from app.kerttu_global.models.models import *
from app.utils.general_utils import get_time_range_for_x_coordinates, get_frequency_range_for_y_coordinates
import csv
import os

DATA_FOLDERS = [
    'datat/global_letters/letters_nonpasserines_seg',
    'datat/global_letters/letters_passerines_seg'
]

number_of_candidates_for_audio = {}
added = []


@app.cli.command('load_global_candidate_data')
def load_global_candidate_data():
    for data_folder in DATA_FOLDERS:
        for subdir, dirs, files in os.walk(data_folder):
            for file in sorted(files):
                if file[-4:] != '.txt':
                    continue
                with open(os.path.join(subdir, file)) as csvfile:
                    reader = csv.reader(csvfile, delimiter=' ')

                    for line in reader:
                        filename = line[0]
                        asset_id = filename.split('_')[0]
                        position = next(reader)
                        time = get_time_range_for_x_coordinates([int(position[0]), int(position[1])])
                        frequency = get_frequency_range_for_y_coordinates([int(position[2]), int(position[3])])

                        key = '{}:{}:{}:{}:{}'.format(filename, time[0], time[1], frequency[0], frequency[1])
                        if key in added:
                            continue
                        else:
                            added.append(key)

                        audio_id = get_audio_id(filename, asset_id)
                        add_letter(audio_id, time[0], time[1], frequency[0], frequency[1])


def get_audio_id(filename, asset_id):
    url = '{}/cornell{}/{}'.format(
        app.config['IMAGE_LAJI_URL'],
        asset_id,
        filename
    )
    audio = GlobalAudio.query.filter(
        GlobalAudio.url.like(url)
    ).first()

    return audio.id


def add_letter(audio_id, x1, x2, y1, y2):
    if audio_id not in number_of_candidates_for_audio:
        number_of_candidates_for_audio[audio_id] = 0

    candidate_number = number_of_candidates_for_audio[audio_id] + 1
    number_of_candidates_for_audio[audio_id] += 1

    letter = GlobalCandidate.query.filter(
        GlobalCandidate.audio_id.like(audio_id),
        GlobalCandidate.x1.like(x1),
        GlobalCandidate.x2.like(x2),
        GlobalCandidate.y1.like(y1),
        GlobalCandidate.y2.like(y2)
    ).first()

    if letter is not None:
        return

    letter = GlobalCandidate(
        audio_id=audio_id,
        x1=x1,
        x2=x2,
        y1=y1,
        y2=y2,
        candidate_number=candidate_number
    )

    db.session.add(letter)
    db.session.commit()
