from app import app, db
from app.kerttu_global.models.models import *
import csv

DATA_FILE = 'datat/Macaulay_audio_metadata_new_210602.csv'


@app.cli.command('load_global_audio_metadata')
def load_global_audio_metadata():
    with open(DATA_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)  # header

        new_objects = []

        for row in reader:
            audio = add_audio(row)
            if audio is not None:
                new_objects.append(audio)

        db.session.bulk_save_objects(new_objects)
        db.session.commit()


def add_audio(row):
    specimen_url = row[46]

    audio = GlobalAudioMetadata.query.filter(
        GlobalAudioMetadata.specimen_url.like(specimen_url)
    ).first()

    if audio is not None:
        return None

    species_code = row[43]
    species_id = GlobalSpecies.query.filter(
        GlobalSpecies.species_code.like(species_code)
    ).first().id

    recordist = row[9]
    year = row[10]
    month = row[11]
    day = row[12]
    country = row[14]
    state = row[16]
    ebird_checklist_id = row[38]

    asset_id = specimen_url.split('/')[-1]

    ebird_checklist_url = None
    if ebird_checklist_id != '':
        ebird_checklist_url = 'https://ebird.org/checklist/{}'.format(ebird_checklist_id)

    sound_type = row[24]

    audio = GlobalAudioMetadata(
        species_id=species_id,
        recordist=recordist,
        country=country,
        state=state,
        year=to_int(year),
        month=to_int(month),
        day=to_int(day),
        asset_id=asset_id,
        specimen_url=specimen_url,
        ebird_checklist_id=ebird_checklist_id,
        ebird_checklist_url=ebird_checklist_url,
        sound_type=sound_type
    )

    return audio


def to_int(value):
    return int(value) if value != '' else None
