from app import app, db, cache
from config import TestingConfig
import unittest
import click
import os


@app.cli.command('test')
@click.argument('name', nargs=-1)
def test(name):
    app.config.from_object(TestingConfig)
    db.init_app(app)
    cache.init_app(app)

    loader = unittest.TestLoader()
    if len(name) == 0:
        suite = loader.discover('tests', pattern='*.py')
    else:
        path = 'tests.' + name[0]
        if os.path.isdir(path.replace('.', '/')):
            suite = loader.discover(path, pattern='*.py')
        else:
            suite = loader.loadTestsFromName(path)

    runner = unittest.TextTestRunner()
    runner.run(suite)
