from app import app, db
from app.kerttu_global.models.models import GlobalRecording, GlobalSpecies
import csv
from datetime import datetime
import re
import json

DATA_PATH = 'datat/data4_mp3_20s.list.txt'
SITE_ID = 10000

with open('scripts/species_code_map.json') as f:
    species_code_map = json.load(f)

species_id_by_name = {}


@app.cli.command('load_global_targeted_recording_data')
def load_global_targeted_recording_data():
    with open(DATA_PATH) as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        for row in reader:
            if row[0] == '':
                continue

            url = get_url(row[0])
            parts = row[0].split('_')

            date_time = None
            locality = None

            date_regex = r'^B(\d{2})h(\d{2})m(\d{2})s(\d{2})([a-z]{3})(\d{4})$'
            for index in [2, 3, 4]:
                if index >= len(parts):
                    break
                date_match = re.match(date_regex, parts[index])
                if date_match is not None:
                    match_index = index
                    break

            if date_match is not None:
                date_time = datetime.strptime('{}{}{} {}{}{} UTC'.format(
                    date_match.group(6),
                    date_match.group(5),
                    date_match.group(4),
                    date_match.group(1),
                    date_match.group(2),
                    date_match.group(3)
                ), '%Y%b%d %H%M%S %Z')
                locality = parts[match_index - 1]
                if locality == 'tp7':
                    locality = parts[match_index - 2]
            else:
                date_regex = r'^(\d{4})(\d{2})(\d{2})d$'
                date_match = re.match(date_regex, parts[1])
                if date_match is not None:
                    date_time = datetime.strptime('{}{}{} UTC'.format(
                        date_match.group(1),
                        date_match.group(2),
                        date_match.group(3)
                    ), '%Y%m%d %Z')
                elif re.match(r'^\d{8}$', parts[1]) and re.match(r'^\d{4}$', parts[2]):
                    date_time = datetime.strptime('{} {} UTC'.format(parts[1], parts[2]), '%Y%m%d %H%M %Z')
                    locality = parts[3]

            species_code = parts[0].split('-')[-1].lower()
            species_name = species_code_map[species_code] if species_code in species_code_map else None
            species_id = get_species_id(species_name)
            if species_name is not None and species_id is None:
                raise Exception(species_name)

            original_recording_name = row[0].rsplit('_', 1)[0]
            original_recording_part = int((int(parts[-1].split('.')[0]) / 10) + 1)

            recording = GlobalRecording.query.filter(
                GlobalRecording.url == url
            ).first()

            if recording is None:
                p1 = float(row[1])
                p2 = float(row[2])
                p3 = float(row[3])

                recording = GlobalRecording(
                    url=url,
                    site_id=SITE_ID,
                    date_time=date_time,
                    x1=p1,
                    x2=p1 + p2,
                    duration=p1 + p2 + p3,
                    target_species_id=species_id,
                    locality=locality,
                    original_recording_name=original_recording_name,
                    original_recording_part=original_recording_part
                )
                db.session.add(recording)
                db.session.commit()


def get_url(file_name):
    return '{}/A-{}/{}'.format(
        app.config['IMAGE_LAJI_URL'],
        file_name.split('_')[0],
        file_name
    )


def get_species_id(scientific_name):
    if scientific_name not in species_id_by_name:
        species = GlobalSpecies.query.filter(
            GlobalSpecies.scientific_name == scientific_name
        ).first()

        species_id_by_name[scientific_name] = species.id if species is not None else None

    return species_id_by_name[scientific_name]
