from app import app, db
from app.models.enums import LetterTemplateStatusEnum, LetterAnnotationEnum
from app.models.models import LetterTemplate, LetterCandidate, LetterAnnotation, User
from app.services.database import letter_service
import csv
import os
import numpy as np
import pandas as pd

DATA_FOLDER = 'datat/letters_text'


@app.cli.command('load_annotation_data')
def load_annotation_data():
    user_id = app.config['DEFAULT_ANNOTATIONS_MADE_BY_USER']
    create_user(user_id)

    for subdir, dirs, files in os.walk(DATA_FOLDER):
        for file in sorted(files):
            file_name_parts = file.split('_')
            if file_name_parts[0] == 'TBI':
                continue

            with open(os.path.join(DATA_FOLDER, file)) as csvfile:
                reader = csv.reader(csvfile, delimiter=' ')
                file_name = next(reader)[0]
                position = next(reader)

                template_id = get_template_id(file)

                # print('processing template {}'.format(file))
                add_annotations(reader, template_id, user_id)


def create_user(user_id):
    user = User.query.get(user_id)
    if user is None:
        user = User(id=user_id)
        db.session.add(user)
        db.session.commit()


def get_template_id(file_name):
    template = LetterTemplate.query.filter(
        LetterTemplate.original_file_name.like(file_name)
    ).first()

    return template.id


def add_annotations(reader, template_id, user_id):
    audio_ids_annotation = []
    audio_ids_uncertain = []

    annotations = {}
    for row in reader:
        if len(row) < 6:
            continue

        if row[5] == '' or row[5] == 'NaN':
            continue

        a = int(row[5])
        audio_id = int(row[0])
        annotations[audio_id] = a

        if a != LetterAnnotationEnum.UNSURE:
            audio_ids_annotation.append(audio_id)
        else:
            audio_ids_uncertain.append(audio_id)

    if len(audio_ids_annotation) < app.config['TARGET_ANNOTATION_COUNT']:
        return

    audio_ids_annotation = list(
        np.random.choice(audio_ids_annotation, app.config['TARGET_ANNOTATION_COUNT'], replace=False))
    audio_ids = audio_ids_annotation + audio_ids_uncertain

    print('adding {} annotations'.format(len(audio_ids)))

    query = LetterCandidate.query.filter(
        LetterCandidate.template_id == template_id
    ).with_entities(LetterCandidate.id, LetterCandidate.audio_id)
    df = pd.read_sql(query.statement, query.session.bind)

    objects = []
    for audio_id in audio_ids:
        a = annotations[audio_id]
        annotation = LetterAnnotation(
            user_id=user_id,
            annotation=a,
            candidate_id=int(df.loc[df['audio_id'] == audio_id].iloc[0]['id'])
        )
        objects.append(annotation)

    db.session.bulk_save_objects(objects)
    db.session.commit()

    letter_service.set_letter_template_finished(user_id, template_id)
