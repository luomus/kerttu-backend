from app import app, db
from app.kerttu_global.models.models import GlobalSite, GlobalRecording
import csv
import math

DATA_PATH = 'datat/sitetest.csv'
LIMIT_ACCESS = True


@app.cli.command('load_global_site_data')
def load_global_site_date():
    with open(DATA_PATH) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)  # header
        for row in reader:
            latitude = float(row[3])
            longitude = float(row[4])
            coordinates_coarse = approximate_coordinates(latitude, longitude)

            site = GlobalSite.query.filter(
                GlobalSite.id == int(row[0])
            ).first()

            if site is None:
                site = GlobalSite(
                    id=int(row[0]),
                    name=row[1],
                    country=row[6],
                    latitude_exact=latitude,
                    longitude_exact=longitude,
                    latitude_coarse=coordinates_coarse[0],
                    longitude_coarse=coordinates_coarse[1],
                    limit_access=LIMIT_ACCESS
                )
                db.session.add(site)

    db.session.commit()


def approximate_coordinates(latitude, longitude, grid_size_km=10):
    earth_radius_km = 6371
    degrees_lat_grid = math.degrees(grid_size_km / earth_radius_km)

    cos = math.cos(math.radians(latitude))
    degrees_lon_grid = degrees_lat_grid / cos

    return (
        round(latitude / degrees_lat_grid) * degrees_lat_grid,
        round(longitude / degrees_lon_grid) * degrees_lon_grid
    )
