from app import app, db
from app.kerttu_global.models.models import GlobalSpecies
import csv

DATA_FILE = 'datat/Eurasian eBird taxonomy updates v2019 Full ES_ES.csv'
LANGUAGE = 'es'


@app.cli.command('load_global_taxon_translations')
def load_global_taxon_translations():
    translation_map = {}

    with open(DATA_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        # next(reader)  # header

        for row in reader:
            translation_map[row[2]] = row[3]

    for species in GlobalSpecies.query.all():
        if species.scientific_name not in translation_map:
            print(species.species_code)
            print(species.scientific_name)
            print(species.category)
            print()
            continue
        setattr(species, 'common_name_{}'.format(LANGUAGE), translation_map[species.scientific_name])

    db.session.commit()
