from app import app, db
from app.kerttu_global.models.models import *
import os

AUDIO_FOLDER = '../laji.fi-front/projects/kerttu-global/generate-spectrograms/input'


@app.cli.command('load_global_xeno_canto_audio_data')
def load_global_xeno_canto_audio_data():
    new_audio = []

    for audio_metadata in GlobalAudioMetadata.query.filter(
        GlobalAudioMetadata.specimen_url.contains('xeno-canto'),
        ~GlobalAudioMetadata.audio.any()
    ).all():
        folder_url = '{}/xeno-canto{}'.format(
            app.config['IMAGE_LAJI_URL'],
            audio_metadata.asset_id,
        )
        audio_url = '{}/{}.mp3'.format(folder_url, audio_metadata.asset_id)
        spectrogram_url = '{}/{}.jpg'.format(folder_url, audio_metadata.asset_id)

        if os.path.exists('{}/{}.mp3'.format(AUDIO_FOLDER, audio_metadata.asset_id)):
            audio = GlobalAudio(
                url=audio_url,
                spectrogram_url=spectrogram_url,
                metadata_id=audio_metadata.id
            )
            new_audio.append(audio)

    db.session.bulk_save_objects(new_audio)
    db.session.commit()
