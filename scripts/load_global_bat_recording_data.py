from app import app, db
from app.kerttu_global.models.enums import TaxonTypeEnum
from app.kerttu_global.models.models import GlobalRecording
import csv

DATA_PATH = 'datat/battest_list.txt'


@app.cli.command('load_global_bat_recording_data')
def load_global_bat_recording_data():
    new_recordings = []

    with open(DATA_PATH) as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        for row in reader:
            if row[0] == '':
                continue

            url = get_url_with_prefix(row[0])

            recording = GlobalRecording.query.filter(
                GlobalRecording.url == url
            ).first()
            if recording is not None:
                continue

            recording = GlobalRecording(
                url=url,
                site_id=10001,
                x1=5,
                x2=10,
                taxon_type=TaxonTypeEnum.BAT
            )

            new_recordings.append(recording)

    db.session.bulk_save_objects(new_recordings)
    db.session.commit()


def get_url_with_prefix(file_name):
    return '{}/B-{}/{}'.format(
        app.config['IMAGE_LAJI_URL'],
        file_name.split('_')[0],
        file_name
    )
