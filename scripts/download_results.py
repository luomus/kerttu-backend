from app import app
from app.models.models import *
from app.utils.database_utils import query_to_dataframe, dataframe_to_tsv
from app.utils.general_utils import *
from app.services import lajistore
from sqlalchemy import or_, and_, union
import os
import shutil
import requests
import random
import pandas as pd
import numpy as np
from natsort import natsort_keygen

result_path = 'results'
user_pseudonyms = {}


@app.cli.command('download_results')
def download_results():
    if os.path.exists(result_path):
         shutil.rmtree(result_path)
    os.makedirs(result_path)

    skipped_audio = RecordingAnnotation.query.filter(
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY,
        RecordingAnnotation.contains_human_speech == True
    ).join(
        RecordingAnnotation.recording
    ).with_entities(
        Recording.audio_id
    ).distinct()

    included_template_audio = LetterTemplateStatus.query.filter(
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY
    ).join(
        LetterTemplateStatus.template
    ).filter(
        LetterTemplate.audio_id.not_in(skipped_audio)
    ).with_entities(
        LetterTemplate.audio_id
    ).distinct()

    included_candidate_audio = get_ready_annotations_query().join(
        LetterAnnotation.candidate
    ).filter(
        LetterCandidate.audio_id.not_in(skipped_audio)
    ).with_entities(
        LetterCandidate.audio_id
    ).distinct()

    q = union(included_template_audio, included_candidate_audio).alias('alias_name')
    included_audio = db.session.query(q)

    download_audio_files(included_audio)
    taxon_expertise = download_users(included_audio)
    download_species(included_audio, taxon_expertise)
    download_letter_results(included_audio)
    download_recording_results(included_audio)


def download_recording_results(included_audio):
    recording_result_path = os.path.join(result_path, 'clips')
    os.makedirs(recording_result_path)

    download_recordings(included_audio, recording_result_path)
    download_recording_annotations(included_audio, recording_result_path)
    download_recording_taxon_annotations(included_audio, recording_result_path)


def download_recording_taxon_annotations(included_audio, recording_result_path):
    annotations = RecordingAnnotation.query.filter(
        RecordingAnnotation.recording.has(
            Recording.audio_id.in_(included_audio)
        ),
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
    ).join(
        RecordingAnnotation.taxon_annotations
    ).with_entities(
        RecordingTaxonAnnotation.recording_annotation_id.label('clip_annotation_id'),
        RecordingTaxonAnnotation.taxon_id,
        RecordingTaxonAnnotation.annotation,
        RecordingTaxonAnnotation.type.label('is_extra_annotation')
    )

    df = query_to_dataframe(annotations)
    df['is_extra_annotation'] = df['is_extra_annotation'].apply(lambda x: False if x == 0 else True)
    df = df.sort_values(by=['clip_annotation_id'], key=natsort_keygen())

    file_path = os.path.join(recording_result_path, 'clip_taxon_annotations.tsv')
    dataframe_to_tsv(df, file_path)


def download_recording_annotations(included_audio, recording_result_path):
    annotations = RecordingAnnotation.query.filter(
        RecordingAnnotation.recording.has(
            Recording.audio_id.in_(included_audio)
        ),
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
    ).with_entities(
        RecordingAnnotation.id.label('clip_annotation_id'),
        RecordingAnnotation.recording_id.label('clip_id'),
        RecordingAnnotation.user_id,
        RecordingAnnotation.is_low_quality,
        RecordingAnnotation.contains_noise_caused_by_human_activity,
        RecordingAnnotation.contains_unknown_birds,
        RecordingAnnotation.does_not_contain_birds
    )

    df = query_to_dataframe(annotations)
    df['user_id'] = df['user_id'].apply(lambda x: user_pseudonyms[x])
    df = df.sort_values(by=['clip_annotation_id'], key=natsort_keygen())

    file_path = os.path.join(recording_result_path, 'clip_annotations.tsv')
    dataframe_to_tsv(df, file_path)


def download_recordings(included_audio, recording_result_path):
    recordings = Recording.query.filter(
        Recording.audio_id.in_(included_audio),
        Recording.annotations.any(
            RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
        )
    ).join(
        Recording.audio
    ).with_entities(
        Recording.id.label('clip_id'),
        Audio.file_name.label('audio_file'),
        Recording.x1,
        Recording.x2
    )

    df = query_to_dataframe(recordings)
    df['audio_file'] = df['audio_file'].apply(lambda x: get_file_name(x))
    df = df.apply(lambda x: replace_coordinates(x), axis=1)
    df = df.sort_values(by='clip_id', key=natsort_keygen())

    file_path = os.path.join(recording_result_path, 'clips.tsv')
    dataframe_to_tsv(df, file_path)


def download_letter_results(included_audio):
    letter_result_path = os.path.join(result_path, 'letters')
    os.makedirs(letter_result_path)
    download_letter_templates(included_audio, letter_result_path)
    download_letter_candidates(included_audio, letter_result_path)
    download_letter_annotations(included_audio, letter_result_path)


def download_letter_annotations(included_audio, letter_result_path):
    annotations = get_ready_annotations_query(included_audio).with_entities(
        LetterAnnotation.candidate_id,
        LetterAnnotation.user_id,
        LetterAnnotation.annotation
    )

    df = query_to_dataframe(annotations)
    df['user_id'] = df['user_id'].apply(lambda x: user_pseudonyms[x])
    df = df.sort_values(by=['candidate_id', 'user_id'], key=natsort_keygen())

    file_path = os.path.join(letter_result_path, 'annotations.tsv')
    dataframe_to_tsv(df, file_path)


def download_letter_candidates(included_audio, letter_result_path):
    candidates = get_ready_annotations_query(included_audio).join(
        LetterAnnotation.candidate
    ).join(
        LetterCandidate.template
    ).join(
        LetterCandidate.audio
    ).with_entities(
        LetterCandidate.id.label('candidate_id'),
        LetterCandidate.template_id,
        Audio.file_name.label('audio_file'),
        LetterCandidate.x1,
        LetterCandidate.x2,
        LetterCandidate.y_diff,
        LetterTemplate.y1.label('template_y1'),
        LetterTemplate.y2.label('template_y2')
    ).distinct()

    df = query_to_dataframe(candidates)
    df['audio_file'] = df['audio_file'].apply(lambda x: get_file_name(x))
    df = df.apply(lambda x: replace_coordinates(x), axis=1)
    df = df.drop(columns=['y_diff', 'template_y1', 'template_y2'])
    df = df.sort_values(by='candidate_id', key=natsort_keygen())

    file_path = os.path.join(letter_result_path, 'candidates.tsv')
    dataframe_to_tsv(df, file_path)


def download_letter_templates(included_audio, letter_result_path):
    templates = LetterTemplate.query.filter(
        LetterTemplate.audio_id.in_(included_audio),
        LetterTemplate.status.any(LetterTemplateStatus.status == LetterTemplateStatusEnum.READY)
    ).join(
        LetterTemplate.audio
    ).with_entities(
        LetterTemplate.id.label('template_id'),
        Audio.file_name.label('audio_file'),
        LetterTemplate.taxon_id,
        LetterTemplate.x1,
        LetterTemplate.x2,
        LetterTemplate.y1,
        LetterTemplate.y2
    )

    df = query_to_dataframe(templates)
    df['audio_file'] = df['audio_file'].apply(lambda x: get_file_name(x))
    df = df.apply(lambda x: replace_coordinates(x), axis=1)
    df = df.sort_values(by='template_id', key=natsort_keygen())

    file_path = os.path.join(letter_result_path, 'templates.tsv')
    dataframe_to_tsv(df, file_path)


def download_species(included_audio, taxon_expertise):
    letter_species = LetterTemplateStatus.query.filter(
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY,
        LetterTemplateStatus.template.has(LetterTemplate.audio_id.in_(included_audio))
    ).join(
        LetterTemplateStatus.template
    ).with_entities(
        LetterTemplate.taxon_id
    ).distinct()
    letter_species = flatten_result(letter_species.all())

    recording_species = RecordingAnnotation.query.filter(
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY,
        RecordingAnnotation.recording.has(Recording.audio_id.in_(included_audio))
    ).join(
        RecordingAnnotation.taxon_annotations
    ).with_entities(
        RecordingTaxonAnnotation.taxon_id
    ).distinct()
    recording_species = flatten_result(recording_species.all())

    all_species = np.unique(np.concatenate([taxon_expertise, letter_species, recording_species]))

    rows = []
    for species in all_species:
        info = fetch_species_info(species)
        rows.append({
            'taxon_id': species,
            'scientific_name': info['scientificName'],
            'vernacular_name_en': info['vernacularName']['en'],
            'vernacular_name_fi': info['vernacularName']['fi'],
            'taxon_rank': info['taxonRank'].replace('MX.', ''),
            'is_bird': True if 'class' in info['parent'] and info['parent']['class']['id'] == 'MX.37580' else False
        })
    df = pd.DataFrame.from_records(rows)
    df = df.sort_values(by='taxon_id', key=natsort_keygen())

    file_path = os.path.join(result_path, 'taxa.tsv')
    dataframe_to_tsv(df, file_path)


def download_users(included_audio):
    users_with_letters = LetterTemplateStatus.query.filter(
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY,
        LetterTemplateStatus.template.has(LetterTemplate.audio_id.in_(included_audio))
    ).with_entities(
        LetterTemplateStatus.user_id
    ).distinct()

    users_with_recordings = RecordingAnnotation.query.filter(
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY,
        RecordingAnnotation.recording.has(Recording.audio_id.in_(included_audio))
    ).with_entities(
        RecordingAnnotation.user_id
    ).distinct()

    users = User.query.filter(
        or_(
            User.id.in_(users_with_letters),
            User.id.in_(users_with_recordings)
        )
    ).with_entities(
        User.id
    ).distinct()

    user_ids = flatten_result(users)
    random.shuffle(user_ids)
    for idx, user_id in enumerate(user_ids):
        user_pseudonyms[user_id] = 'user{}'.format(idx + 1)

    data = lajistore.get_expertise_data(user_ids)
    users_in_data = [d['user_id'] for d in data]
    missing_users = [u for u in user_ids if u not in users_in_data]
    for u in missing_users:
        data.append({'user_id': u, 'taxon_expertise': []})

    df = pd.DataFrame(data)

    skill_level_map = {}
    activity_level_map = {}
    for i in range(1, 5):
        skill_level_map['MA.finnishBirdSongRecognitionSkillLevelEnum{}'.format(i)] = '{}'.format(i)
        activity_level_map['MA.birdwatchingActivityLevelEnum{}'.format(i)] = '{}'.format(i)
    df = df.replace({
        'user_id': user_pseudonyms,
        'finnish_bird_song_recognition_skill_level': skill_level_map,
        'birdwatching_activity_level': activity_level_map
    })
    df = df.sort_values(by='user_id', key=natsort_keygen())

    taxon_expertise = np.unique(df['taxon_expertise'].agg(np.concatenate))

    file_path = os.path.join(result_path, 'users.tsv')
    dataframe_to_tsv(df, file_path, ['taxon_expertise'])

    return taxon_expertise


def download_audio_files(included_audio):
    audio_path = os.path.join(result_path, 'audio_files')
    os.makedirs(audio_path)

    audio = Audio.query.filter(
        Audio.id.in_(included_audio)
    ).with_entities(
        Audio.file_name
    ).all()

    for a in flatten_result(audio):
        url = get_audio_file_location(a)

        with open(os.path.join(audio_path, get_file_name(a)), 'wb') as f:
            response = requests.get(url)
            f.write(response.content)


def fetch_species_info(taxon_id):
    lajiapi_url = app.config['LAJIAPI_URL']
    access_token = app.config['LAJIAPI_TOKEN']

    params = {
        'access_token': access_token,
        'selectedFields': 'vernacularName,scientificName,taxonRank,parent',
        'lang': 'multi'
    }
    result = requests.get(
        '{}taxa/{}'.format(lajiapi_url, taxon_id),
        params=params
    ).json()

    if 'error' in result:
        return None
    else:
        return result


def flatten_result(result):
    return [r[0] for r in result]


def get_file_name(original_file_name):
    return '{}.mp3'.format(original_file_name.split('.')[0])


def replace_coordinates(row):
    time_range = get_time_range_for_x_coordinates([row['x1'], row['x2']])
    row['x1'] = round(time_range[0], 3)
    row['x2'] = round(time_range[1], 3)

    if 'y1' in row or 'y_diff' in row:
        if 'y_diff' in row:
            y_range = [
                row['template_y1'] + row['y_diff'],
                row['template_y2'] + row['y_diff'],
            ]
        else:
            y_range = [
                row['y1'],
                row['y2']
            ]

        freq_range = get_frequency_range_for_y_coordinates(y_range)
        row['y1'] = round(freq_range[0], 3)
        row['y2'] = round(freq_range[1], 3)

    return row


def get_ready_annotations_query(included_audio=None):
    query = LetterTemplateStatus.query.filter(
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY
    )
    if included_audio is not None:
        query = query.filter(
            LetterTemplateStatus.template.has(LetterTemplate.audio_id.in_(included_audio))
        )

    query = query.join(
        LetterAnnotation, and_(
            LetterAnnotation.user_id == LetterTemplateStatus.user_id,
            LetterAnnotation.template_id == LetterTemplateStatus.template_id
        )
    ).filter(
        LetterAnnotation.annotation != LetterAnnotationEnum.UNSURE
    )

    if included_audio is not None:
        query = query.filter(
            LetterAnnotation.candidate.has(
                LetterCandidate.audio_id.in_(included_audio)
            )
        )

    return query
