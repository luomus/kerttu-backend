from app import app, db
from app.kerttu_global.models.models import *
from app.kerttu_global.models.enums import *
import csv

DATA_FILE = 'datat/Global_templates_species_list_eBird_v2019.csv'
ORDER_ORDER_FILE = 'datat/Order_order.csv'
FAMILY_ORDER_FILE = 'datat/Family_order.csv'

continents = []
order_order = {}
family_order = {}


@app.cli.command('load_global_taxon_data')
def load_global_taxon_data():
    load_order_and_family_orders()

    with open(DATA_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        add_continents(next(reader))  # header

        for row in reader:
            add_species(row)


def add_continents(row):
    for col in row[10:]:
        name = col.replace('_', ' ')

        continent = GlobalContinent.query.filter(
            GlobalContinent.name.like(name)
        ).first()

        if continent is None:
            continent = GlobalContinent(
                name=name
            )
            db.session.add(continent)
            db.session.commit()

        continents.append(continent)


def add_species(row):
    species = GlobalSpecies.query.filter(
        GlobalSpecies.scientific_name.like(row[2])
    ).first()

    if species is None:
        species = GlobalSpecies(
            taxon_order=int(row[0]),
            category=get_category(row[1]),
            scientific_name=row[2],
            species_code=row[3],
            common_name=row[7]
        )
        order_id = get_order_id(row[8])
        family_id = get_family_id(row[9], order_id)
        species.order_id = order_id
        species.family_id = family_id

        if species.category != TaxonCategoryEnum.SPECIES:
            parent_species = GlobalSpecies.query.filter(GlobalSpecies.scientific_name.like(row[5])).first()
            if parent_species is None:
                print('Parent species is missing from taxon {}, skipping'.format(row[2]))
                return
            species.parent_species_id = parent_species.id

        for idx, col in enumerate(row[10:]):
            if int(col) == 1:
                species.continents.append(continents[idx])

        db.session.add(species)
        db.session.commit()


def get_order_id(name):
    db_object = GlobalOrder.query.filter(
        GlobalOrder.scientific_name.like(name)
    ).first()

    if db_object is None:
        db_object = GlobalOrder(scientific_name=name, order_order=order_order[name])
        db.session.add(db_object)
        db.session.commit()

    return db_object.id


def get_family_id(name, order_id=None):
    db_object = GlobalFamily.query.filter(
        GlobalFamily.scientific_name.like(name)
    ).first()

    if db_object is None:
        db_object = GlobalFamily(scientific_name=name, order_id=order_id, family_order=family_order[name])
        db.session.add(db_object)
        db.session.commit()

    return db_object.id


def get_category(name):
    if name == 'species':
        return TaxonCategoryEnum.SPECIES
    elif name == 'issf':
        return TaxonCategoryEnum.ISSF
    elif name == 'form':
        return TaxonCategoryEnum.FORM
    elif name == 'domestic':
        return TaxonCategoryEnum.DOMESTIC
    elif name == 'intergrade':
        return TaxonCategoryEnum.INTERGRADE


def load_order_and_family_orders():
    with open(ORDER_ORDER_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)  # header

        for row in reader:
            order_order[row[1]] = int(row[0])

    with open(FAMILY_ORDER_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)  # header

        for row in reader:
            family_order[row[1]] = int(row[0])
