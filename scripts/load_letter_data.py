from app import app, db
from app.models.models import Audio, LetterTemplate, LetterCandidate
import csv
import os
import requests
import numpy as np
import pandas as pd

DATA_FOLDER = 'datat/letters_4_text'
species_cache = {}


@app.cli.command('load_letter_data')
def load_letter_data():
    audio_ids = [audio_id for (audio_id,) in db.session.query(Audio.id).filter(Audio.id <= 51720).all()]
    removed_audio = np.setdiff1d(np.arange(1, 51721), np.array(audio_ids))

    for subdir, dirs, files in os.walk(DATA_FOLDER):
        for file in sorted(files):
            file_name_parts = file.split('_')
            scientific_name = file_name_parts[0] + ' ' + file_name_parts[1]
            taxon_id, show_location = get_taxon_id(scientific_name)

            with open(os.path.join(DATA_FOLDER, file)) as csvfile:
                reader = csv.reader(csvfile, delimiter=' ')
                file_name = next(reader)[0]
                position = next(reader)

                audio_id = get_audio_id(file_name)

                print('processing template {}'.format(file))
                template = add_template(audio_id, taxon_id, show_location, position, file)

                add_candidates(reader, template.id, removed_audio)


def get_audio_id(file_name):
    audio = Audio.query.filter(
        Audio.file_name.like(file_name)
    ).first()

    if audio is None:
        audio = Audio(file_name=file_name)
        db.session.add(audio)
        db.session.commit()

    return audio.id


def get_taxon_id(species_name):
    show_location = True

    if species_name not in species_cache:
        params = {
            'access_token': app.config['LAJIAPI_TOKEN'],
            'query': species_name
        }
        result = requests.get(
            '{}{}/{}'.format(app.config['LAJIAPI_URL'], 'taxa', 'search'),
            params=params
        ).json()

        match = result[0]
        if match['type'] != 'exactMatches':
            print('Mapped species {} to taxon {} ({}) but the match wasn\'t exact'.format(
                species_name, match['scientificName'], match['id'])
            )
        id = match['id']
        if id == 'MX.37166':
            id = 'MX.73566'
        elif match['taxonRank'] != 'MX.species':
            print('Mapped species {} to taxon {} () but its taxon rank isn\'t species'.format(
                species_name, match['scientificName'], match['id'])
            )

        params = {'access_token': app.config['LAJIAPI_TOKEN'], 'selectedFields': 'sensitive'}
        result = requests.get(
            '{}{}/{}'.format(app.config['LAJIAPI_URL'], 'taxa', id),
            params=params
        ).json()

        if 'sensitive' in result and result['sensitive'] is True:
            show_location = False

        species_cache[species_name] = (id, show_location)

    return species_cache[species_name]


def add_template(audio_id, taxon_id, show_location, position, file):
    template = LetterTemplate.query.filter(
        LetterTemplate.original_file_name == file
    ).first()

    if template is None:
        template = LetterTemplate(
            audio_id=audio_id,
            taxon_id=taxon_id,
            x1=position[0],
            x2=position[1],
            y1=position[2],
            y2=position[3],
            original_file_name=file,
            show_location=show_location
        )

        db.session.add(template)
        db.session.commit()

    return template


def add_candidates(reader, template_id, removed_audio_ids):
    audio_ids = []
    candidates = {}
    for row in reader:
        audio_id = int(row[0])
        if audio_id in removed_audio_ids:
            continue
        candidates[audio_id] = {
            'cross_correlation': float(row[1]),
            'x1': int(row[2]),
            'x2': int(row[3]),
            'y_diff': int(row[4])
        }
        audio_ids.append(audio_id)

    query = LetterCandidate.query.filter(
        LetterCandidate.template_id == template_id
    ).with_entities(LetterCandidate.audio_id)
    saved_audio_ids = pd.read_sql(query.statement, query.session.bind)['audio_id'].to_numpy()

    missing_ids = np.setdiff1d(np.array(audio_ids), saved_audio_ids)

    print('adding {} candidates'.format(len(missing_ids)))

    if len(missing_ids) > 0:
        objects = []
        for audio_id in missing_ids:
            c = candidates[audio_id]
            candidate = LetterCandidate(
                audio_id=int(audio_id),
                template_id=template_id,
                cross_correlation=c['cross_correlation'],
                x1=c['x1'],
                x2=c['x2'],
                y_diff=c['y_diff']
            )
            objects.append(candidate)

        db.session.bulk_save_objects(objects)
        db.session.commit()
