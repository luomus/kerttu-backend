from app import app, db
from app.kerttu_global.models.models import *
import requests


@app.cli.command('load_global_audio_data')
def load_global_audio_metadata():
    for audio_metadata in GlobalAudioMetadata.query.filter(
        GlobalAudioMetadata.specimen_url.contains('macaulaylibrary')
    ).all():
        folder_url = '{}/cornell{}'.format(
            app.config['IMAGE_LAJI_URL'],
            audio_metadata.asset_id,
        )
        response = requests.get(folder_url)
        if response.status_code == 404:
            continue
        response = response.json()

        for item in response:
            uri = item['uri']
            if uri[-4:] == '.mp3':
                audio = GlobalAudio.query.filter(
                    GlobalAudio.url.like(uri)
                ).first()

                if audio is not None:
                    continue

                audio = GlobalAudio(
                    url=uri,
                    spectrogram_url=uri[:-4] + '.jpg',
                    metadata_id=audio_metadata.id
                )

                db.session.add(audio)
                db.session.commit()
