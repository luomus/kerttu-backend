from app import app, db
from app.kerttu_global.models.models import *
import csv

DATA_FILE = 'datat/new_recordings_to_bsg_templates.csv'


@app.cli.command('load_global_xeno_canto_audio_metadata')
def load_global_xeno_canto_audio_metadata():
    with open(DATA_FILE) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)  # header

        new_objects = []

        for row in reader:
            audio = get_audio(row)
            if audio is not None:
                new_objects.append(audio)

            if len(new_objects) > 2000:
                db.session.bulk_save_objects(new_objects)
                db.session.commit()
                new_objects = []

        db.session.bulk_save_objects(new_objects)
        db.session.commit()


def get_audio(row):
    specimen_url = 'https:' + row[14]

    audio = GlobalAudioMetadata.query.filter(
        GlobalAudioMetadata.specimen_url.like(specimen_url)
    ).first()

    if audio is not None:
        return None

    species_code = row[1]
    species_id = GlobalSpecies.query.filter(
        GlobalSpecies.species_code.like(species_code)
    ).first().id

    recordist = row[7]

    year = ''
    month = ''
    day = ''
    if len(row[21]) > 0:
        date_parts = row[21].split('-')
        year = date_parts[0]
        month = date_parts[1]
        day = date_parts[2]

    country = row[8]
    location = row[9]
    asset_id = specimen_url.split('/')[-1]
    license_url = 'https:' + row[17]
    sound_type = row[13]

    return GlobalAudioMetadata(
        species_id=species_id,
        recordist=recordist,
        country=country,
        location=location,
        year=to_int(year),
        month=to_int(month),
        day=to_int(day),
        asset_id=asset_id,
        specimen_url=specimen_url,
        license_url=license_url,
        sound_type=sound_type
    )


def to_int(value):
    return int(value) if value != '' else None
