from app import db
from tests.kerttu_global.test_utils.base_test import BaseTestCase
from app.kerttu_global.models.models import *
from datetime import datetime


class IdentificationBaseTestCase(BaseTestCase):
    siteId1 = 1
    siteId2 = 2
    site1RecordingId = 1
    site2RecordingId = 2
    site2RecordingId2 = 3
    emptySiteId = 3
    restrictedSiteId = 4

    def setUp(self):
        super().setUp()
        self._add_site_data()

    def _add_site_data(self):
        site1 = GlobalSite(
            id=self.siteId1,
            name='site 1',
            latitude_exact=40,
            longitude_exact=70,
            latitude_coarse=40,
            longitude_coarse=70
        )
        site1.recordings.append(GlobalRecording(
            url='url.mp3',
            date_time=datetime.now(),
            x1=1,
            x2=7,
            duration=10
        ))

        site2 = GlobalSite(
            id=self.siteId2,
            name='site 2',
            latitude_exact=40,
            longitude_exact=70,
            latitude_coarse=40,
            longitude_coarse=70
        )
        site2.recordings.append(GlobalRecording(
            url='url.mp3',
            date_time=datetime.now(),
            x1=1,
            x2=7,
            duration=10
        ))
        site2.recordings.append(GlobalRecording(
            url='url.mp3',
            date_time=datetime.now(),
            x1=1,
            x2=7,
            duration=10
        ))

        empty_site = GlobalSite(
            id=self.emptySiteId,
            name='empty site',
            latitude_exact=40,
            longitude_exact=70,
            latitude_coarse=40,
            longitude_coarse=70
        )

        restricted_site = GlobalSite(
            id=self.restrictedSiteId,
            name='empty site',
            latitude_exact=40,
            longitude_exact=70,
            latitude_coarse=40,
            longitude_coarse=70,
            limit_access=True
        )
        restricted_site.recordings.append(GlobalRecording(
            url='url.mp3',
            date_time=datetime.now(),
            x1=1,
            x2=7,
            duration=10
        ))

        db.session.add(site1)
        db.session.add(site2)
        db.session.add(empty_site)
        db.session.add(restricted_site)
        db.session.commit()

    def add_recording_annotation(self, data):
        recording_id = data['recording_id'] if 'recording_id' in data else self.site1RecordingId
        user_id = data['user_id'] if 'user_id' in data else 'MA.1'
        has_non_bird_area = data['has_non_bird_area'] if 'has_non_bird_area' in data else False
        species_ids = data['species_ids'] if 'species_ids' in data else []
        nbr_of_boxes_per_species = data['nbr_of_boxes_per_species'] if 'nbr_of_boxes_per_species' in data else 0

        annotation = GlobalRecordingAnnotation(
            recording_id=recording_id,
            user_id=user_id,
            status=AnnotationStatusEnum.READY,
            created=datetime.now(),
            edited=datetime.now()
        )
        if has_non_bird_area:
            annotation.non_bird_area = GlobalArea(
                x1=0,
                x2=1,
                y1=0,
                y2=1
            )

        for species_id in species_ids:
            species_annotation = GlobalRecordingSpeciesAnnotation(
                species_id=species_id,
                occurrence=TaxonOccurrenceEnum.OCCURS
            )
            for j in range(nbr_of_boxes_per_species):
                species_annotation.boxes.append(GlobalRecordingSpeciesAnnotationBox(
                    area=GlobalArea(
                        x1=0,
                        x2=1,
                        y1=0,
                        y2=0
                    )
                ))
            annotation.species_annotations.append(species_annotation)

        db.session.add(annotation)
        db.session.commit()
