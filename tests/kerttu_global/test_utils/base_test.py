from app import db
from tests.test_utils.test_case import TestCase
from app.kerttu_global.models.models import *


class BaseTestCase(TestCase):
    order_count = 2
    family_count_per_order = 4
    species_count_per_family = 1
    subspecies_count_per_species = 1
    species_count = order_count * family_count_per_order * species_count_per_family

    def setUp(self):
        super().setUp()
        self._add_species_data()

    """
    Species data looks like this
    order 0
        family 0
            species 0 subspecies 1
        family 1
            species 2 subspecies 3
        ...
    order 1
        family 2
            ...
        ...
    """
    def _add_species_data(self):
        for i in range(self.order_count):
            order = GlobalOrder(
                id=i,
                order_order=i,
                scientific_name='order_{}'.format(i)
            )
            db.session.add(order)

            for j in range(self.family_count_per_order):
                family_id = i * self.family_count_per_order + j
                family = GlobalFamily(
                    id=family_id,
                    family_order=family_id,
                    scientific_name='family_{}'.format(family_id),
                    order=order
                )

                db.session.add(family)

                total_species_count_per_family = self.species_count_per_family + (
                    self.species_count_per_family * self.subspecies_count_per_species
                )
                species_id = family_id * total_species_count_per_family
                for k in range(self.species_count_per_family):
                    species_id = species_id
                    species = GlobalSpecies(
                        id=species_id,
                        taxon_order=species_id,
                        category=TaxonCategoryEnum.SPECIES,
                        species_code='{}'.format(species_id),
                        scientific_name='species_{}'.format(species_id),
                        common_name='species_{}'.format(species_id),
                        common_name_es='species_{}'.format(species_id),
                        order=order,
                        family=family,
                    )
                    db.session.add(species)
                    species_id += 1

                    for l in range(self.subspecies_count_per_species):
                        species = GlobalSpecies(
                            id=species_id,
                            taxon_order=species_id,
                            category=TaxonCategoryEnum.ISSF,
                            species_code='{}'.format(species_id),
                            scientific_name='species_{}'.format(species_id),
                            common_name='species_{}'.format(species_id),
                            common_name_es='species_{}'.format(species_id),
                            order=order,
                            family=family,
                            parent=species
                        )
                        db.session.add(species)
                        species_id += 1

        db.session.commit()
