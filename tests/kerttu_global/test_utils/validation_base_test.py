from app import db
from tests.kerttu_global.test_utils.base_test import BaseTestCase
from app.kerttu_global.models.models import *
from datetime import datetime


class ValidationBaseTestCase(BaseTestCase):
    user_id = 'MA.1'
    audio_count_per_species = 2
    candidate_count_per_audio = 2

    def setUp(self):
        super().setUp()
        self._add_validation_test_data()

    def _add_validation_test_data(self):
        species_ids = range(0, self.species_count + (self.species_count * self.subspecies_count_per_species))

        for species_id in species_ids:
            for i in range(self.audio_count_per_species):
                metadata_id = species_id * self.audio_count_per_species + i
                audio_metadata = GlobalAudioMetadata(
                    id=metadata_id,
                    species_id=species_id,
                    asset_id='{}'.format(metadata_id),
                    specimen_url='{}'.format(metadata_id)
                )
                db.session.add(audio_metadata)

                audio_id = metadata_id
                audio = GlobalAudio(
                    id=audio_id,
                    url='audio url',
                    spectrogram_url='spectogram url',
                    audio_metadata=audio_metadata
                )
                db.session.add(audio)

                for j in range(self.candidate_count_per_audio):
                    candidate = GlobalCandidate(
                        id=metadata_id * self.candidate_count_per_audio + j,
                        audio=audio,
                        x1=0,
                        x2=1,
                        y1=1000,
                        y2=3000,
                        candidate_number=j
                    )
                    db.session.add(candidate)

        db.session.commit()

    def add_version_for_species(self, species_id, user_id, validated_by=[]):
        audio = GlobalAudio.query.filter(
            GlobalAudio.audio_metadata.has(species_id=species_id)
        ).first()
        version = GlobalVersion(
            species_id=species_id,
            user_id=user_id,
            created=datetime.now()
        )

        for i in range(10):
            version.templates.append(
                GlobalTemplate(
                    audio_id=audio.id,
                    x1=0,
                    x2=1,
                    y1=1000,
                    y2=3000,
                    template_number=i + 1
                )
            )

        db.session.add(version)
        db.session.commit()

        for user in validated_by:
            validation = GlobalValidation(
                version_id=version.id,
                user_id=user,
                created=datetime.now()
            )
            db.session.add(validation)
        db.session.commit()
