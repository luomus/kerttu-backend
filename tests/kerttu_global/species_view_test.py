from tests.kerttu_global.test_utils.validation_base_test import ValidationBaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch
from app.kerttu_global.models.models import GlobalSpeciesLock


@patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
class SpeciesViewTest(ValidationBaseTestCase):
    def test_species_list_returns_correct_number_of_species(self):
        response = self.app.get('/global/species?pageSize=1000')
        assert response.status_code == 200

        correct_count = self.species_count
        count = response.json['total']
        assert count == correct_count

        results = response.json['results']
        assert len(results) == correct_count

    def test_species_list_order_filter_works(self):
        order_id = 0
        response = self.app.get('/global/species?pageSize=1000&order={}'.format(order_id))
        assert response.status_code == 200

        correct_count = self.family_count_per_order * self.species_count_per_family
        count = response.json['total']
        assert count == correct_count

        results = response.json['results']
        assert len(results) == correct_count

    def test_species_list_family_filter_works(self):
        family_id = 0
        response = self.app.get('/global/species?pageSize=1000&family={}'.format(family_id))
        assert response.status_code == 200

        correct_count = self.species_count_per_family
        count = response.json['total']
        assert count == correct_count

        results = response.json['results']
        assert len(results) == correct_count

    def test_species_returns_correct_species(self):
        species_id = 3
        response = self.app.get('/global/species/{}'.format(species_id))
        assert response.status_code == 200
        species = response.json
        assert species['id'] == species_id
        assert species['scientificName'] == 'species_{}'.format(species_id)

    def test_lock_species_creates_lock(self):
        species_id = 0
        response = self.app.post('/global/species/lock/{}'.format(species_id))
        assert response.status_code == 200

        lock = GlobalSpeciesLock.query.filter(
            GlobalSpeciesLock.user_id == self.user_id,
            GlobalSpeciesLock.species_id == species_id
        ).first()
        assert lock is not None

    def test_unlock_species_removes_lock(self):
        species_id = 0
        response = self.app.post('/global/species/lock/{}'.format(species_id))
        assert response.status_code == 200
        response = self.app.post('/global/species/unlock/{}'.format(species_id))
        assert response.status_code == 200

        lock = GlobalSpeciesLock.query.filter(
            GlobalSpeciesLock.user_id == self.user_id,
            GlobalSpeciesLock.species_id == species_id
        ).first()
        assert lock is None
