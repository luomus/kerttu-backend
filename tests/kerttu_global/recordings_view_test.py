from tests.kerttu_global.test_utils.validation_base_test import ValidationBaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch


@patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
class RecordingsViewTest(ValidationBaseTestCase):
    def test_recording_list_returns_success_status(self):
        response = self.app.get('/global/recordings/0')
        assert response.status_code == 200

    def test_recording_list_returns_correct_number_of_recordings(self):
        response = self.app.get('/global/recordings/0')
        data = response.json['results']
        assert len(data) == self.audio_count_per_species * (1 + self.subspecies_count_per_species)

    def test_recording_list_returns_correct_number_of_candidates(self):
        response = self.app.get('/global/recordings/0')
        data = response.json['results']
        for recording in data:
            assert 'candidates' in recording
            assert len(recording['candidates']) == self.candidate_count_per_audio

    def test_recording_list_recordings_belong_to_correct_species(self):
        allowed_species_ids = [0]
        for i in range(self.subspecies_count_per_species):
            allowed_species_ids.append(i + 1)

        response = self.app.get('/global/recordings/0')
        data = response.json['results']
        for recording in data:
            assert recording['audio']['species']['id'] in allowed_species_ids
