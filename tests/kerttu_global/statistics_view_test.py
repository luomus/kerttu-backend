import time
from tests.kerttu_global.test_utils.validation_base_test import ValidationBaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch


@patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
class SpeciesViewTest(ValidationBaseTestCase):
    def test_validation_stats_when_no_validations(self):
        response = self.app.get('/global/statistics/validations')
        assert response.status_code == 200

        stats = response.json['results']
        assert len(stats) == 1

        assert stats[0]['validationCount'] == 0 and stats[0]['count'] == self.species_count

    def test_validation_stats_when_validations_from_one_user(self):
        user_id = 'MA.1'
        self.add_version_for_species(0, user_id)
        self.add_version_for_species(2, user_id)
        response = self.app.get('/global/statistics/validations')
        assert response.status_code == 200

        stats = response.json['results']
        assert len(stats) == 2

        stats = sorted(stats, key=lambda x: x['validationCount'])
        correct_stats = [
            {'validationCount': 0, 'count': self.species_count - 2},
            {'validationCount': 1, 'count': 2}
        ]
        assert stats == correct_stats

    def test_validation_stats_when_validations_from_multiple_user(self):
        self.add_version_for_species(0, 'MA.1', ['MA.3'])
        self.add_version_for_species(2, 'MA.1', ['MA.2', 'MA.3'])
        self.add_version_for_species(4, 'MA.3', ['MA.1'])
        self.add_version_for_species(6, 'MA.1')
        time.sleep(1)
        self.add_version_for_species(2, 'MA.2')
        response = self.app.get('/global/statistics/validations')
        assert response.status_code == 200

        stats = response.json['results']
        assert len(stats) == 3

        stats = sorted(stats, key=lambda x: x['validationCount'])
        correct_stats = [
            {'validationCount': 0, 'count': self.species_count - 4},
            {'validationCount': 1, 'count': 2},
            {'validationCount': 2, 'count': 2}
        ]
        assert stats == correct_stats

    def test_user_stats_when_no_validations(self):
        response = self.app.get('/global/statistics/users')
        assert response.status_code == 200

        stats = response.json['results']
        assert len(stats) == 0

    def test_user_stats_when_validations(self):
        self.add_version_for_species(0, 'MA.1', ['MA.3'])
        self.add_version_for_species(2, 'MA.1', ['MA.3'])
        self.add_version_for_species(4, 'MA.3', ['MA.1'])
        self.add_version_for_species(6, 'MA.1', ['MA.4', 'MA.3'])
        time.sleep(1)
        self.add_version_for_species(2, 'MA.2', ['MA.1'])
        self.add_version_for_species(8, 'MA.5')
        response = self.app.get('/global/statistics/users')
        assert response.status_code == 200

        stats = response.json['results']
        assert len(stats) == 5

        stats = sorted(stats, key=lambda x: x['userId'])
        correct_stats = [
            {'userId': 'MA.1', 'speciesCreated': 3, 'speciesValidated': 1},
            {'userId': 'MA.2', 'speciesCreated': 0, 'speciesValidated': 1},
            {'userId': 'MA.3', 'speciesCreated': 1, 'speciesValidated': 3},
            {'userId': 'MA.4', 'speciesCreated': 0, 'speciesValidated': 1},
            {'userId': 'MA.5', 'speciesCreated': 1, 'speciesValidated': 0}
        ]
        assert stats == correct_stats
