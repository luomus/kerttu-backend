from base64 import b64encode
from unittest.mock import patch

from app import db
from app.kerttu_global.models.models import APIUser, GlobalRecording, GlobalSite
from tests.test_utils.mediaAPI_mock import mediaAPI_mock
from tests.test_utils.test_case import TestCase
from io import BytesIO
from datetime import datetime
import os

@patch('app.services.mediaAPI.upload_file', new=mediaAPI_mock.upload_file)
class ApiViewTest(TestCase):
    test_user = 'test_user'
    test_password = 'test_password'
    device_id = 'abc'

    def setUp(self):
        super().setUp()
        self._add_test_data()

    def test_uploading_without_authentication(self):
        response = self.app.post('/api/fileUpload', content_type='multipart/form-data', data=self._get_post_data())
        assert response.status_code == 401

    def test_uploading_with_invalid_authentication(self):
        response = self.app.post(
            '/api/fileUpload',
            content_type='multipart/form-data',
            data=self._get_post_data(),
            headers=self._get_auth_headers(self.test_user, 'abc')
        )
        assert response.status_code == 401

    def test_uploading_with_invalid_file(self):
        response = self.app.post(
            '/api/fileUpload',
            content_type='multipart/form-data',
            data=self._get_post_data('invalid.mp3'),
            headers=self._get_auth_headers()
        )
        assert response.status_code == 400
        data = response.json
        assert 'message' in data and data['message'] == 'Couldn\'t decode the file'

    def test_uploading_with_invalid_date(self):
        response = self.app.post(
            '/api/fileUpload',
            content_type='multipart/form-data',
            data=self._get_post_data('test.mp3', '2020-01'),
            headers=self._get_auth_headers()
        )
        assert response.status_code == 400
        data = response.json
        assert 'message' in data and data['message'] == 'Couldn\'t parse the date'

    def test_uploading_with_invalid_device_id(self):
        response = self.app.post(
            '/api/fileUpload',
            content_type='multipart/form-data',
            data=self._get_post_data('test.mp3', None, 'jglk'),
            headers=self._get_auth_headers()
        )
        assert response.status_code == 400
        data = response.json
        assert 'message' in data and data['message'] == 'Invalid device ID'

    def test_uploading_with_valid_file_and_date(self):
        date_string = '2023-05-01T16:19:27.000Z'

        response = self.app.post(
            '/api/fileUpload',
            content_type='multipart/form-data',
            data=self._get_post_data('test.mp3', date_string),
            headers=self._get_auth_headers()
        )
        assert response.status_code == 200

        recording = GlobalRecording.query.filter(
            GlobalRecording.uploaded_by == self.test_user
        ).first()
        assert recording is not None
        assert recording.date_time == datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S.%fZ')
        assert recording.group.name == 'test.mp3'

    def _get_post_data(self, file_name='test.mp3', date=None, device_id=None):
        with open(os.path.join(os.path.dirname(__file__), 'test_data', file_name), 'rb') as audio:
            audioBytesIO = BytesIO(audio.read())

        data = {
            'file': (audioBytesIO, 'test.mp3'),
            'deviceID': device_id if device_id is not None else self.device_id
        }

        if date is not None:
            data['date'] = date

        return data

    def _get_auth_headers(self, user=None, password=None):
        if user is None:
            user = self.test_user
        if password is None:
            password = self.test_password

        credentials = b64encode(str.encode('{}:{}'.format(user, password))).decode('utf-8')
        return {"Authorization": "Basic {}".format(credentials)}

    def _add_test_data(self):
        site = GlobalSite(
            id=1,
            name='site 1',
            latitude_exact=40,
            longitude_exact=70,
            latitude_coarse=40,
            longitude_coarse=70,
            device_id=self.device_id
        )
        db.session.add(site)

        user = APIUser(system_id=self.test_user, password=self.test_password)
        db.session.add(user)

        db.session.commit()
