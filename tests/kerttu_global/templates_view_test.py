import time
from app import app
from app.kerttu_global.models.models import GlobalVersion, GlobalValidation, GlobalTemplate, GlobalComment
from tests.kerttu_global.test_utils.validation_base_test import ValidationBaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch


@patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
class TemplatesViewTest(ValidationBaseTestCase):
    def test_template_list_when_no_version(self):
        response = self.app.get('/global/templates/0')
        assert response.status_code == 200

        versions = response.json['results']
        assert len(versions) == 0

    def test_template_list_when_versions(self):
        self.add_version_for_species(0, 'MA.1')
        time.sleep(1)
        self.add_version_for_species(0, 'MA.2')

        response = self.app.get('/global/templates/0')
        assert response.status_code == 200

        versions = response.json['results']
        assert len(versions) == 2
        assert versions[0]['userId'] == 'MA.1'
        assert len(versions[0]['templates']) == app.config['GLOBAL_REQUIRED_TEMPLATE_COUNT']
        assert versions[1]['userId'] == 'MA.2'
        assert len(versions[1]['templates']) == app.config['GLOBAL_REQUIRED_TEMPLATE_COUNT']

    def test_can_save_version(self):
        species_id = 0
        templates = app.config['GLOBAL_REQUIRED_TEMPLATE_COUNT'] * [
            {'audioId': 0, 'area': {'xRange': [0, 1], 'yRange': [100, 200]}}
        ]
        data = {'templates': templates}

        response = self.app.post('/global/templates/{}'.format(species_id), json=data)
        assert response.status_code == 200

        versions = GlobalVersion.query.filter(
            GlobalVersion.species_id == species_id
        )
        assert versions.count() == 1
        version = versions.first()
        assert version.user_id == self.user_id
        assert len(version.templates) == app.config['GLOBAL_REQUIRED_TEMPLATE_COUNT']

    def test_can_save_validation(self):
        species_id = 0
        version_user_id = 'MA.2'
        self.add_version_for_species(species_id, version_user_id)

        response = self.app.get('/global/templates/0')
        versions = response.json['results']
        templates = versions[0]['templates']
        data = {'templates': templates}

        response = self.app.post('/global/templates/{}'.format(species_id), json=data)
        assert response.status_code == 200

        versions = GlobalVersion.query.filter(
            GlobalVersion.species_id == species_id
        )
        assert versions.count() == 1
        version = versions.first()
        assert version.user_id == version_user_id

        validations = GlobalValidation.query.filter(
            GlobalValidation.version_id == version.id
        )
        assert validations.count() == 1
        validation = validations.first()
        assert validation.user_id == self.user_id

    def test_can_save_second_version(self):
        species_id = 0
        first_version_user_id = 'MA.2'
        self.add_version_for_species(species_id, first_version_user_id)

        response = self.app.get('/global/templates/0')
        versions = response.json['results']
        templates = versions[0]['templates']
        comments = [{'templateId': templates[4]['id'], 'type': 0, 'comment': 'comment'}]
        templates[4] = {
            'audioId': 0,
            'area': {'xRange': [4, 6], 'yRange': [1000, 1200]}
        }
        data = {'templates': templates, 'comments': comments}

        response = self.app.post('/global/templates/{}'.format(species_id), json=data)
        assert response.status_code == 200

        versions = GlobalVersion.query.filter(
            GlobalVersion.species_id == species_id
        )
        assert versions.count() == 2

        changed_template = GlobalTemplate.query.filter(
            GlobalTemplate.versions.any(user_id=self.user_id),
            GlobalTemplate.template_number == 5
        ).first()

        assert (changed_template.x1 == 4 and changed_template.x2 == 6 and
                changed_template.y1 == 1000 and changed_template.y2 == 1200)

        assert GlobalComment.query.count() == 1

    def test_cannot_save_second_version_without_comment(self):
        species_id = 0
        first_version_user_id = 'MA.2'
        self.add_version_for_species(species_id, first_version_user_id)

        response = self.app.get('/global/templates/0')
        versions = response.json['results']
        templates = versions[0]['templates']
        templates[4] = {
            'audioId': 0,
            'area': {'xRange': [4, 6], 'yRange': [1000, 1200]}
        }
        data = {'templates': templates, 'comments': []}

        response = self.app.post('/global/templates/{}'.format(species_id), json=data)
        assert response.status_code == 400
