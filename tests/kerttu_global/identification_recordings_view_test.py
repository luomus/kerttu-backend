from tests.kerttu_global.test_utils.identification_base_test import IdentificationBaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch
from app.kerttu_global.models.enums import TaxonOccurrenceEnum, AnnotationStatusEnum
from app.kerttu_global.models.models import GlobalRecordingAnnotation


@patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
class IdentificationRecordingsViewTest(IdentificationBaseTestCase):
    def test_returns_recording(self):
        response = self.app.get('/global/identification/recordings/new', query_string={'sites': self.siteId1})
        assert response.status_code == 200

        result = response.json
        assert 'recording' in result and result['recording'] is not None

    def test_returns_annotation(self):
        recording_id = self.site2RecordingId

        self.app.post('/global/identification/recordings/{}/annotation'.format(recording_id), json={
            'containsUnknownBirds': True
        })

        response = self.app.get('/global/identification/recordings/{}'.format(recording_id))
        assert response.status_code == 200
        result = response.json
        assert (
            'annotation' in result and
            result['annotation'] is not None and
            result['annotation']['containsUnknownBirds'] is True
        )

    def test_does_not_accept_invalid_annotation(self):
        recording_id = self.site2RecordingId

        response = self.app.post('/global/identification/recordings/{}/annotation'.format(recording_id), json={
            'hasBoxesForAllBirdSounds': True
        })
        assert response.status_code == 400

    def test_accepts_invalid_annotation_if_it_is_draft(self):
        recording_id = self.site2RecordingId

        response = self.app.post(
            '/global/identification/recordings/{}/annotation'.format(recording_id),
            query_string={'isDraft': 'true'},
            json={'hasBoxesForAllBirdSounds': True}
        )
        assert response.status_code == 200

        response = self.app.get('/global/identification/recordings/{}'.format(recording_id))
        assert response.status_code == 200
        annotation = response.json['annotation']
        assert annotation['hasBoxesForAllBirdSounds'] is True

    def test_can_skip_recording(self):
        recording_id = self.site2RecordingId
        response = self.app.post(
            '/global/identification/recordings/{}/annotation'.format(recording_id),
            query_string={'skipRecording': 'true'},
            json={}
        )
        assert response.status_code == 200

        annotation = GlobalRecordingAnnotation.query.filter(
            GlobalRecordingAnnotation.user_id == 'MA.1',
            GlobalRecordingAnnotation.recording_id == recording_id
        ).first()

        assert annotation.status == AnnotationStatusEnum.SKIPPED

    def test_can_change_annotation(self):
        recording_id = self.site2RecordingId

        self.app.post('/global/identification/recordings/{}/annotation'.format(recording_id), json={
            'isLowQuality': True,
            'speciesAnnotations': [{
                'speciesId': 1,
                'occurrence': TaxonOccurrenceEnum.OCCURS
            }]
        })

        response = self.app.get('/global/identification/recordings/{}'.format(recording_id))
        annotation = response.json['annotation']
        assert annotation['isLowQuality'] is True
        assert len(annotation['speciesAnnotations']) == 1 and annotation['speciesAnnotations'][0]['speciesId'] == 1

        self.app.post('/global/identification/recordings/{}/annotation'.format(recording_id), json={
            'containsHumanSpeech': True,
            'speciesAnnotations': [{
                'speciesId': 2,
                'occurrence': TaxonOccurrenceEnum.OCCURS
            }]
        })

        response = self.app.get('/global/identification/recordings/{}'.format(recording_id))
        annotation = response.json['annotation']
        assert annotation['isLowQuality'] is False
        assert annotation['containsHumanSpeech'] is True
        assert len(annotation['speciesAnnotations']) == 1 and annotation['speciesAnnotations'][0]['speciesId'] == 2
