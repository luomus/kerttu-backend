from tests.kerttu_global.test_utils.identification_base_test import IdentificationBaseTestCase
from app.kerttu_global.models.models import GlobalSitePermission
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch
from app import db


class IdentificationSitesViewTest(IdentificationBaseTestCase):
    def test_sites_list_returns_success_status(self):
        response = self.app.get('/global/identification/sites')
        assert response.status_code == 200

    def test_sites_list_returns_correct_sites(self):
        response = self.app.get('/global/identification/sites')
        data = response.json['results']
        assert len(data) == 2

        ids = [d['id'] for d in data]
        assert ids[0] == self.siteId1
        assert ids[1] == self.siteId2

    @patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
    def test_sites_list_returns_also_restricted_site_when_the_user_has_permission(self):
        user_permission = GlobalSitePermission(user_id='MA.1', site_id=self.restrictedSiteId)
        db.session.add(user_permission)
        db.session.commit()

        response = self.app.get('/global/identification/sites')
        data = response.json['results']
        assert len(data) == 3

        ids = [d['id'] for d in data]
        assert self.restrictedSiteId in ids
