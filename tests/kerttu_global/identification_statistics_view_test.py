from tests.kerttu_global.test_utils.identification_base_test import IdentificationBaseTestCase


class IdentificationSitesViewTest(IdentificationBaseTestCase):
    def test_statistics_returns_empty_list_when_no_data(self):
        for stat_type in ['sites', 'users']:
            response = self.app.get('/global/identification/statistics/{}'.format(stat_type))
            assert response.status_code == 200
            results = response.json['results']
            assert len(results) == 0

    def test_site_statistics_returns_correct_annotation_count_for_one_site(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1',
            'species_ids': [0],
            'nbr_of_boxes_per_species': 2
        })
        response = self.app.get('/global/identification/statistics/sites')
        data = response.json['results']
        assert len(data) == 1
        assert data[0]['siteId'] == self.siteId1 and data[0]['count'] == 1

        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.2'
        })
        response = self.app.get('/global/identification/statistics/sites')
        data = response.json['results']
        assert len(data) == 1
        assert data[0]['siteId'] == self.siteId1 and data[0]['count'] == 2

    def test_site_statistics_returns_correct_annotation_count_for_two_sites(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1',
            'has_non_bird_area': True,
            'species_ids': [0]
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.1',
            'species_ids': [0, 1]
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.2',
            'species_ids': [0],
            'nbr_of_boxes_per_species': 1
        })
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.2',
            'has_non_bird_area': True
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.3'
        })

        response = self.app.get('/global/identification/statistics/sites')
        data = response.json['results']
        assert len(data) == 2
        assert data[0]['siteId'] == self.siteId1 and data[0]['count'] == 2
        assert data[1]['siteId'] == self.siteId2 and data[1]['count'] == 3

    def test_user_statistics_returns_correct_counts_for_one_user(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1',
            'species_ids': [0],
            'nbr_of_boxes_per_species': 2
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.1',
            'has_non_bird_area': True,
            'species_ids': [0, 1, 2]
        })

        response = self.app.get('/global/identification/statistics/users')
        data = response.json['results']
        assert len(data) == 1
        user_data = data[0]
        assert user_data['userId'] == 'MA.1'
        assert user_data['annotationCount'] == 2
        assert user_data['speciesCount'] == 4
        assert user_data['distinctSpeciesCount'] == 3
        assert user_data['drawnBoxesCount'] == 3

    def test_user_statistics_returns_correct_drawn_boxes_count_when_no_boxes(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1',
            'has_non_bird_area': False
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.1',
            'has_non_bird_area': False
        })

        response = self.app.get('/global/identification/statistics/users')
        data = response.json['results']
        assert len(data) == 1
        user_data = data[0]
        assert user_data['userId'] == 'MA.1'
        assert user_data['drawnBoxesCount'] == 0

    def test_user_statistics_returns_correct_drawn_boxes_count_when_only_non_bird_boxes(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1',
            'has_non_bird_area': True,
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.1'
        })

        response = self.app.get('/global/identification/statistics/users')
        data = response.json['results']
        assert len(data) == 1
        user_data = data[0]
        assert user_data['userId'] == 'MA.1'
        assert user_data['drawnBoxesCount'] == 1

    def test_user_statistics_returns_correct_drawn_boxes_count_when_only_bird_boxes(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1',
            'species_ids': [0],
            'nbr_of_boxes_per_species': 2
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.1',
            'species_ids': [0, 1],
            'nbr_of_boxes_per_species': 2
        })

        response = self.app.get('/global/identification/statistics/users')
        data = response.json['results']
        assert len(data) == 1
        user_data = data[0]
        assert user_data['userId'] == 'MA.1'
        assert user_data['drawnBoxesCount'] == 6

    def test_user_statistics_returns_correct_distinct_species_count_when_no_species_annotations(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1'
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.1'
        })

        response = self.app.get('/global/identification/statistics/users')
        data = response.json['results']
        assert len(data) == 1
        user_data = data[0]
        assert user_data['userId'] == 'MA.1'
        assert user_data['distinctSpeciesCount'] == 0

    def test_species_statistics_returns_correct_count(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1',
            'species_ids': [0, 1]
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.1',
            'species_ids': [1, 2]
        })

        response = self.app.get('/global/identification/statistics/species')
        data = response.json['results']
        assert len(data) == 3
        species_data = data[0]
        assert species_data['id'] == 1
        assert species_data['count'] == 2

    def test_species_statistics_returns_correct_box_count(self):
        self.add_recording_annotation({
            'recording_id': self.site1RecordingId,
            'user_id': 'MA.1',
            'species_ids': [0],
            'nbr_of_boxes_per_species': 2
        })
        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.1',
            'species_ids': [0]
        })

        response = self.app.get('/global/identification/statistics/species')
        data = response.json['results']
        assert len(data) == 1
        species_data = data[0]
        assert species_data['id'] == 0
        assert species_data['drawnBoxesCount'] == 2

        self.add_recording_annotation({
            'recording_id': self.site2RecordingId,
            'user_id': 'MA.2',
            'species_ids': [0],
            'nbr_of_boxes_per_species': 1
        })

        response = self.app.get('/global/identification/statistics/species')
        data = response.json['results']
        assert len(data) == 1
        species_data = data[0]
        assert species_data['id'] == 0
        assert species_data['drawnBoxesCount'] == 3
        