from tests.test_utils.base_test import BaseTestCase
from app import db
from app.services.next import next_letter_template
from app.services.database import letter_service
from app.models.models import LetterTemplate
import pandas as pd
from pandas.testing import assert_frame_equal


class NextLetterTemplateTest(BaseTestCase):
    def test_returns_correct_target_taxon_when_no_expertice(self):
        taxon = next_letter_template._get_target_taxon('MA.1', [])
        assert taxon is None

    def test_returns_correct_target_taxon(self):
        user_id = 'MA.1'
        letter_service.set_letter_template_finished(user_id, 1)
        taxon = next_letter_template._get_target_taxon(user_id, ['MX.1', 'MX.2'])
        assert taxon == 'MX.2'
        letter_service.set_letter_template_skipped(user_id, 4)
        taxon = next_letter_template._get_target_taxon(user_id, ['MX.1', 'MX.2'])
        assert taxon == 'MX.1'
        letter_service.set_letter_template_finished(user_id, 2)
        letter_service.set_letter_template_finished(user_id, 3)
        taxon = next_letter_template._get_target_taxon(user_id, ['MX.1', 'MX.2'])
        assert taxon is None

    def test_returns_correct_template1(self):
        letter_service.set_letter_template_finished('MA.2', 1)
        letter_service.set_letter_template_finished('MA.3', 1)
        letter_service.set_letter_template_finished('MA.2', 2)
        letter_service.set_letter_template_finished('MA.3', 2)
        template = next_letter_template.get_letter_template('MA.1', ['MX.1'])
        assert template.id == 3

    def test_returns_correct_template2(self):
        letter_service.set_letter_template_finished('MA.2', 1)
        letter_service.set_letter_template_finished('MA.2', 2)
        letter_service.set_letter_template_finished('MA.3', 2)
        template = next_letter_template.get_letter_template('MA.1', ['MX.1'])
        assert template.id == 1

    def test_returns_correct_template3(self):
        letter_service.set_letter_template_skipped('MA.1', 1)
        letter_service.set_letter_template_finished('MA.2', 1)
        letter_service.set_letter_template_finished('MA.2', 2)
        template = next_letter_template.get_letter_template('MA.1', ['MX.1'])
        assert template.id == 2

    def test_returns_correct_template4(self):
        letter_service.set_letter_template_finished('MA.2', 1)
        letter_service.set_letter_template_skipped('MA.3', 1)
        letter_service.set_letter_template_finished('MA.2', 2)
        letter_service.set_letter_template_finished('MA.3', 2)
        template = next_letter_template.get_letter_template('MA.1', ['MX.1'])
        assert template.id == 1

    def test_returns_correct_template5(self):
        letter_service.set_letter_template_finished('MA.1', 1)
        letter_service.set_letter_template_finished('MA.1', 2)
        letter_service.set_letter_template_finished('MA.1', 3)
        letter_service.set_letter_template_finished('MA.2', 4)
        template = next_letter_template.get_letter_template('MA.1', ['MX.1', 'MX.2'])
        assert template.id == 4

    def test_returns_some_template_when_no_annotations(self):
        template = next_letter_template.get_letter_template('MA.1', ['MX.1'])
        assert template is not None

    def test_returns_none_when_no_templates(self):
        letter_service.set_letter_template_skipped('MA.1', 1)
        letter_service.set_letter_template_finished('MA.1', 2)
        letter_service.set_letter_template_finished('MA.1', 3)
        template = next_letter_template.get_letter_template('MA.1', ['MX.1'])
        assert template is None

    def test_returns_none_when_only_removed_templates(self):
        for t in LetterTemplate.query.all():
            t.removed = True
        db.session.commit()
        template = next_letter_template.get_letter_template('MA.1', ['MX.1'])
        assert template is None

    def test_returns_correct_template_annotation_counts(self):
        letter_service.set_letter_template_finished('MA.1', 1)
        letter_service.set_letter_template_finished('MA.1', 3)
        letter_service.set_letter_template_finished('MA.2', 1)
        letter_service.set_letter_template_finished('MA.2', 3)
        letter_service.set_letter_template_finished('MA.2', 4)

        correct_counts = pd.DataFrame({
            'taxon_id': ['MX.1', 'MX.2'], 'count': [4, 1], 'by_user_count': [2, 0]
        })
        counts = next_letter_template._get_template_annotation_counts('MA.1', ['MX.1', 'MX.2'])
        assert_frame_equal(counts, correct_counts, check_like=True)

    def test_returns_correct_template_annotation_counts_when_no_data(self):
        correct_counts = pd.DataFrame({
            'taxon_id': ['MX.1', 'MX.2'], 'count': [0, 0], 'by_user_count': [0, 0]
        })
        counts = next_letter_template._get_template_annotation_counts('MA.1', ['MX.1', 'MX.2'])
        assert_frame_equal(counts, correct_counts, check_like=True)
