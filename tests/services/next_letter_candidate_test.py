from tests.test_utils.base_test import BaseTestCase
from unittest.mock import patch
import numpy as np
import pandas as pd
from app.services.database import letter_service
from app.services.next import next_letter_candidate
from app.models.enums import LetterAnnotationEnum


class NextLetterCandidateTest(BaseTestCase):
    @patch('numpy.random.uniform')
    def test_target_correlation_when_few_annotations(self, random_mock):
        random_mock.return_value = 0

        user_data = pd.DataFrame({'id': [], 'annotation': [], 'cross_correlation': []})

        corr = next_letter_candidate._get_target_correlation(user_data)
        np.testing.assert_almost_equal(corr, 1)

        user_data = _add_row_to_df(user_data, {'id': 1, 'annotation': LetterAnnotationEnum.YES, 'cross_correlation': 1})
        corr = next_letter_candidate._get_target_correlation(user_data)
        np.testing.assert_almost_equal(corr, 0.9)

        for i in range(2, 10):
            user_data = _add_row_to_df(
                user_data,
                {'id': i, 'annotation': LetterAnnotationEnum.YES, 'cross_correlation': 1}
            )

        corr = next_letter_candidate._get_target_correlation(user_data)
        np.testing.assert_almost_equal(corr, 0.1)

        user_data = _add_row_to_df(
            user_data,
            {'id': 10, 'annotation': LetterAnnotationEnum.YES, 'cross_correlation': 1}
        )

        corr = next_letter_candidate._get_target_correlation(user_data)
        np.testing.assert_almost_equal(corr, 0)

    @patch('numpy.random.uniform')
    def test_target_correlation_when_more_annotations(self, random_mock):
        random_mock.return_value = 0.5

        candidates = np.arange(1, 21)
        annotations = np.concatenate([np.full(14, LetterAnnotationEnum.NO), np.full(6, LetterAnnotationEnum.YES)])
        cross_correlations = np.linspace(0.05, 1, 20)

        user_data = pd.DataFrame({'id': candidates, 'annotation': annotations, 'cross_correlation': cross_correlations})

        corr = next_letter_candidate._get_target_correlation(user_data)
        assert 0.7 < corr < 0.75

    @patch('numpy.random.uniform')
    def test_target_correlation_is_not_zero_when_good_fit(self, random_mock):
        random_mock.return_value = 0.2

        annotations = np.array(
            [0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0,
             0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0])
        cross_correlations = np.array(
            [0.241735, 0.714662, 0.611208, 0.633942, 0.606171, 0.235513, 0.316373, 0.618914, 0.237043, 0.608525,
             0.241412, 0.230727, 0.236301, 0.240194, 0.60122, 0.237026, 0.6253, 0.233998, 0.236685, 0.211526, 0.601399,
             0.214558, 0.213208, 0.601769, 0.233366, 0.712861, 0.230265, 0.236991, 0.21897, 0.230896, 0.628833,
             0.602883, 0.232701, 0.612552, 0.221044, 0.377258, 0.234032, 0.682988, 0.622838, 0.241664, 0.626877,
             0.609005, 0.626708, 0.718593, 0.234834, 0.240505, 0.639681, 0.672413, 0.239106, 0.215413, 0.226471,
             0.60117, 0.224576, 0.606625, 0.240126])
        candidates = np.arange(1, len(annotations) + 1)

        user_data = pd.DataFrame({'id': candidates, 'annotation': annotations, 'cross_correlation': cross_correlations})

        corr = next_letter_candidate._get_target_correlation(user_data)
        assert np.isclose(corr, 0.6)

    @patch('numpy.random.uniform')
    def test_returns_correct_candidate_when_one_user(self, random_mock):
        random_mock.return_value = 0

        user_id = 'MA.1'
        template_id = 1

        candidate = next_letter_candidate.get_letter_candidate(user_id, template_id)
        assert candidate.id == 20

        candidate = next_letter_candidate.get_letter_candidate(user_id, template_id)
        assert candidate.id == 20

        letter_service.set_letter_annotation(user_id, 20, LetterAnnotationEnum.YES)
        candidate = next_letter_candidate.get_letter_candidate(user_id, template_id)
        assert candidate.id == 18

    def test_propotions_by_category_is_correct(self):
        user_id = 'MA.1'
        user_ids = ['MA.2', 'MA.3']
        d = {
            'total_user_count': [1, 1, 1, 2, 2, 2, 3],
            'MA.1': [1, 1, 1, 1, 1, 1, 1],
            'MA.2': [0, 0, 0, 1, 1, 0, 1],
            'MA.3': [0, 0, 0, 0, 0, 1, 1]
        }
        data = pd.DataFrame(data=d)

        correct_counts = np.array([2, 1, 3, 1, 0])
        correct_props = correct_counts / (2 + 1 + 3 + 1)

        props = next_letter_candidate._get_proportions_by_category(user_id, user_ids, data)
        np.testing.assert_almost_equal(props, correct_props)

    def test_target_propotions_by_category_is_correct(self):
        user_ids = ['MA.2', 'MA.3']
        correct_props = np.array([0.123629982, 0.123629982, 0.502740035, 0.25, 0])

        props = next_letter_candidate._get_target_proportions_by_category(user_ids)
        np.testing.assert_almost_equal(props, correct_props)

    def test_next_category_is_correct(self):
        user_ids = ['MA.2', 'MA.3', 'MA.4']
        d = {
            'total_user_count': [1, 1, 2, 3],
            'MA.1': [0, 0, 0, 0],
            'MA.2': [1, 0, 1, 1],
            'MA.3': [0, 1, 1, 1],
            'MA.4': [0, 0, 0, 1]
        }
        data = pd.DataFrame(data=d)
        next_letter_candidate._set_next_category_to_data(user_ids, data)

        correct = np.array([0, 1, 5, 4])
        np.testing.assert_almost_equal(data['next_category'].to_numpy(), correct)

    @patch('numpy.random.uniform')
    def test_returns_correct_candidate_when_multiple_users(self, random_mock):
        random_mock.return_value = 0

        for i in range(1, 21):
            if i != 19:
                letter_service.set_letter_annotation('MA.2', i, LetterAnnotationEnum.YES)
                letter_service.set_letter_annotation('MA.3', i, LetterAnnotationEnum.YES)
        letter_service.set_letter_template_finished('MA.2', 1)
        letter_service.set_letter_template_finished('MA.3', 1)

        candidate = next_letter_candidate.get_letter_candidate('MA.1', 1)
        assert candidate.id == 19

        letter_service.set_letter_annotation('MA.1', 19, LetterAnnotationEnum.YES)
        candidate = next_letter_candidate.get_letter_candidate('MA.1', 1)
        assert candidate.id == 18

    @patch('numpy.random.uniform')
    def test_does_not_return_candidate_the_user_has_annotated(self, random_mock):
        random_mock.return_value = 0

        for i in range(1, 21):
            if i != 19:
                letter_service.set_letter_annotation('MA.2', i, LetterAnnotationEnum.YES)
                letter_service.set_letter_annotation('MA.3', i, LetterAnnotationEnum.YES)
        letter_service.set_letter_template_finished('MA.2', 1)
        letter_service.set_letter_template_finished('MA.3', 1)

        letter_service.set_letter_annotation('MA.1', 20, LetterAnnotationEnum.UNSURE)
        letter_service.set_letter_annotation('MA.1', 19, LetterAnnotationEnum.UNSURE)
        candidate = next_letter_candidate.get_letter_candidate('MA.1', 1)
        assert candidate.id == 18


def _add_row_to_df(df, row):
    return pd.concat([df, pd.DataFrame(row, index=[0])], ignore_index=True)
