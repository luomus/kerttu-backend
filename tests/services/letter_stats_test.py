from tests.test_utils.base_test import BaseTestCase
from app.services.statistics import letter_stats as stats
from app.services.database import letter_service
from app.models.enums import LetterAnnotationEnum
import numpy as np


class LetterStatsTest(BaseTestCase):

    def test_user_annotation_count(self):
        user_id = 'MA.1'
        _put_example_data(user_id)
        assert stats.user_annotation_count(user_id, 1) == 3

    def test_user_annotation_data(self):
        user_id = 'MA.1'
        _put_example_data(user_id)

        data = stats.user_annotation_data(user_id, 1, True).sort_values(by=['id'])
        np.testing.assert_equal(data['id'], np.array([1, 2, 3, 4]))
        np.testing.assert_equal(data['annotation'], np.array(
            [LetterAnnotationEnum.YES, LetterAnnotationEnum.YES, LetterAnnotationEnum.UNSURE, LetterAnnotationEnum.NO])
        )

    def test_annotation_data_when_one_user(self):
        user_id = 'MA.1'
        _put_example_data(user_id)

        users, data = stats.annotation_data_for_template(1)
        data = data.sort_values(by=['id'])
        assert len(users) == 1 and users[0] == user_id
        np.testing.assert_equal(data['id'], np.array([1, 2, 4]))
        np.testing.assert_equal(data[user_id], np.ones(3))
        np.testing.assert_equal(data['total_user_count'], np.ones(3))

    def test_annotation_data_when_one_many_users(self):
        user1 = 'MA.1'
        _put_example_data(user1)
        user2 = 'MA.2'
        letter_service.set_letter_annotation(user2, 2, LetterAnnotationEnum.YES)
        letter_service.set_letter_annotation(user2, 3, LetterAnnotationEnum.NO)
        letter_service.set_letter_template_finished(user2, 1)
        user3 = 'MA.3'
        letter_service.set_letter_annotation(user3, 1, LetterAnnotationEnum.NO)
        letter_service.set_letter_template_finished(user3, 1)

        users, data = stats.annotation_data_for_template(1)
        data = data.sort_values(by=['id'])
        assert len(users) == 3 and user1 in users and user2 in users and user3 in users
        np.testing.assert_equal(data['id'], np.array([1, 2, 3, 4]))
        np.testing.assert_equal(data[user1], np.array([1, 1, 0, 1]))
        np.testing.assert_equal(data[user2], np.array([0, 1, 1, 0]))
        np.testing.assert_equal(data[user3], np.array([1, 0, 0, 0]))
        np.testing.assert_equal(data['total_user_count'], np.array([2, 2, 1, 1]))


def _put_example_data(user_id):
    letter_service.set_letter_annotation(user_id, 1, LetterAnnotationEnum.YES)
    letter_service.set_letter_annotation(user_id, 2, LetterAnnotationEnum.YES)
    letter_service.set_letter_annotation(user_id, 3, LetterAnnotationEnum.UNSURE)
    letter_service.set_letter_annotation(user_id, 4, LetterAnnotationEnum.NO)
    letter_service.set_letter_template_finished(user_id, 1)
