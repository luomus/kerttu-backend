from tests.test_utils.base_test import BaseTestCase
from app.services.lajistore import get_visible_users, get_expertise_data


class LajistoreTest(BaseTestCase):

    def test_get_visible_users_returns_something(self):
        user_ids = ['MA.309']
        visible_users = get_visible_users(user_ids)
        assert visible_users == [] or visible_users == user_ids

    def test_get_expertise_data_returns_something(self):
        user_ids = ['MA.309']
        data = get_expertise_data(user_ids)
        assert len(data) == 1
