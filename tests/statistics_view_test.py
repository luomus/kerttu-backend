from tests.test_utils.base_test import BaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from tests.test_utils.lajistore_mock import lajistore_mock
from app.services.database import letter_service
from unittest.mock import patch
import unittest


@unittest.skip('view is not in use')
@patch('app.services.lajistore.get_visible_users', new=lajistore_mock.get_visible_users)
class StatisticsViewTest(BaseTestCase):
    def test_returns_correct_users_stats(self):
        letter_service.set_letter_template_finished('MA.1', 1)
        letter_service.set_letter_template_finished('MA.1', 2)
        letter_service.set_letter_template_finished('MA.1', 4)

        response = self.app.get('/statistics/users')
        assert response.status_code == 200
        data = response.json

        assert len(data) == 1
        assert data[0]['userId'] is None
        assert data[0]['letterAnnotationCount'] == 300
        assert data[0]['recordingAnnotationCount'] == 0
        assert data[0]['totalAnnotationCount'] == 300

    @patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
    @patch('app.services.lajiAPI.get_user_taxon_expertise', new=lajiAPI_mock.get_user_taxon_expertise)
    def test_users_stats_shows_user_id_of_the_requester(self):
        letter_service.set_letter_template_finished('MA.1', 1)

        response = self.app.get('/statistics/users')
        data = response.json

        assert data[0]['userId'] == 'MA.1'

    def test_users_stats_shows_user_ids_of_visible_users(self):
        letter_service.set_letter_template_finished('MA.1', 1)
        letter_service.set_letter_template_finished('MA.2', 1)
        letter_service.set_letter_template_finished('MA.3', 1)
        letter_service.set_letter_template_finished('MA.4', 1)

        response = self.app.get('/statistics/users')
        data = response.json

        user_ids = [d['userId'] for d in data if d['userId'] is not None]

        assert len(user_ids) == 2
        assert 'MA.2' in user_ids
        assert 'MA.4' in user_ids
