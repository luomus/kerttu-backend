from tests.test_utils.base_test import BaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch
import unittest
from app.services.database import letter_service


@unittest.skip('view is not in use')
@patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
@patch('app.services.lajiAPI.get_user_taxon_expertise', new=lajiAPI_mock.get_user_taxon_expertise)
class LetterViewTest(BaseTestCase):

    def test_returns_letter_template_and_candidate(self):
        response = self.app.get('/letter/template')
        assert response.status_code == 200

        data = response.json
        assert 'template' in data
        assert data['template'] is not None

        template_id = data['template']['id']

        response = self.app.get('/letter/candidate/{}'.format(template_id))
        assert response.status_code == 200

        data = response.json
        assert 'candidate' in data
        assert data['candidate'] is not None

    def test_can_submit_annotation(self):
        template_id = self._get_template_id('template')
        candidate_id = self._get_candidate_id('candidate/{}'.format(template_id))

        data = {'annotation': 1}
        response = self.app.put('/letter/annotation/{}/{}'.format(template_id, candidate_id), json=data)
        assert response.status_code == 200

        annotation = letter_service.get_letter_annotation('MA.1', candidate_id)
        assert annotation == 1

    def test_can_go_to_previous_candidate(self):
        template_id = self._get_template_id('template')
        candidate_id = self._get_candidate_id('candidate/{}'.format(template_id))
        next_candidate_id = self._get_candidate_id('nextCandidate/{}/{}'.format(template_id, candidate_id))

        self.app.put('/letter/annotation/{}/{}'.format(template_id, candidate_id), json={'annotation': 1})
        assert self._get_candidate_id('candidate/{}'.format(template_id)) == next_candidate_id

        assert self._get_candidate_id('previousCandidate/{}/{}'.format(template_id, next_candidate_id)) == candidate_id
        assert self._get_candidate_id('candidate/{}'.format(template_id)) == candidate_id

    def test_can_skip_template(self):
        template_id = self._get_template_id('template')

        data = {'templateId': template_id}
        response = self.app.put('/letter/skipTemplate', json=data)
        assert response.status_code == 200

        assert self._get_template_id('template') != template_id

    def _get_template_id(self, url):
        response = self.app.get('/letter/{}'.format(url))
        return response.json['template']['id']

    def _get_candidate_id(self, url):
        response = self.app.get('/letter/{}'.format(url))
        return response.json['candidate']['id']
