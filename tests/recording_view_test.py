from tests.test_utils.base_test import BaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch
import unittest


@unittest.skip('view is not in use')
@patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
@patch('app.services.lajiAPI.get_user_taxon_expertise', new=lajiAPI_mock.get_user_taxon_expertise)
class RecordingViewTest(BaseTestCase):
    data1 = {
        'containsNoiseCausedByHumanActivity': True,
        'taxonAnnotations': {
            'main': [{
                'taxonId': 'MX.1',
                'annotation': 2,
                'bird': True
            }]
        }
    }
    data2 = {
        'containsNoiseCausedByHumanActivity': False,
        'taxonAnnotations': {
            'main': [
                {
                    'taxonId': 'MX.2',
                    'annotation': 1,
                    'bird': True
                },
                {
                    'taxonId': 'MX.1',
                    'annotation': 2,
                    'bird': True
                }
            ]
        }
    }

    def test_returns_recording_when_there_are_letter_annotations(self):
        self.add_example_letter_annotation_data()

        response = self.app.get('/recording')
        assert response.status_code == 200

        data = response.json
        assert data['recording'] is not None

    def test_does_not_return_recording_no_letter_annotations(self):
        response = self.app.get('/recording')
        assert response.status_code == 200

        data = response.json
        assert data['recording'] is None

    def test_can_save_annotation(self):
        self.add_example_letter_annotation_data()
        recording_id = self.app.get('/recording').json['recording']['id']

        response = self.app.post('/recording/annotation/{}'.format(recording_id), json=self.data1)
        assert response.status_code == 200

        response = self.app.get('/recording')
        assert response.status_code == 200
        data = response.json['annotation']
        assert data is not None
        assert data['containsNoiseCausedByHumanActivity'] is True
        assert len(data['taxonAnnotations']['main']) == 1
        assert data['taxonAnnotations']['main'][0]['taxonId'] == 'MX.1'
        assert data['taxonAnnotations']['main'][0]['annotation'] == 2

    def test_can_remove_taxon_from_taxon_annotations(self):
        self.add_example_letter_annotation_data()
        recording_id = self.app.get('/recording').json['recording']['id']
        self.app.post('/recording/annotation/{}'.format(recording_id), json=self.data2)
        self.app.post('/recording/annotation/{}'.format(recording_id), json=self.data1)

        response = self.app.get('/recording')
        data = response.json['annotation']

        assert data['containsNoiseCausedByHumanActivity'] is True
        assert len(data['taxonAnnotations']['main']) == 1
        assert data['taxonAnnotations']['main'][0]['taxonId'] == 'MX.1'

    def test_can_add_taxon_to_taxon_annotations(self):
        self.add_example_letter_annotation_data()
        recording_id = self.app.get('/recording').json['recording']['id']
        self.app.post('/recording/annotation/{}'.format(recording_id), json=self.data1)
        self.app.post('/recording/annotation/{}'.format(recording_id), json=self.data2)

        response = self.app.get('/recording')
        data = response.json['annotation']

        assert data['containsNoiseCausedByHumanActivity'] is False
        assert len(data['taxonAnnotations']['main']) == 2

    def test_can_go_to_next_recording(self):
        self.add_example_letter_annotation_data()
        recording_id = self.app.get('/recording').json['recording']['id']
        self.app.post('/recording/annotation/{}'.format(recording_id), json=self.data2)

        response = self.app.get('/recording/next/{}'.format(recording_id))
        assert response.status_code == 200
        assert response.json['recording']['id'] != recording_id

    def test_can_go_to_previous_recording(self):
        self.add_example_letter_annotation_data()
        recording_id = self.app.get('/recording').json['recording']['id']
        self.app.post('/recording/annotation/{}'.format(recording_id), json=self.data2)
        next_recording_id = self.app.get('/recording/next/{}'.format(recording_id)).json['recording']['id']

        response = self.app.get('/recording/previous/{}'.format(next_recording_id))
        assert response.status_code == 200
        assert response.json['recording']['id'] == recording_id
