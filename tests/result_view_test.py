from tests.test_utils.base_test import BaseTestCase
from tests.test_utils.lajiAPI_mock import lajiAPI_mock
from unittest.mock import patch
from base64 import b64encode
from io import BytesIO
import pandas as pd
import unittest


@unittest.skip('view is not in use')
@patch('app.services.lajiAPI.get_user_id', new=lajiAPI_mock.get_user_id)
@patch('app.services.lajiAPI.get_user_taxon_expertise', new=lajiAPI_mock.get_user_taxon_expertise)
class ResultViewTest(BaseTestCase):

    def test_returns_correct_letter_results(self):
        self.add_example_letter_annotation_data()
        response = self.app.get('/result/letter', headers=self._get_auth_headers())
        assert response.status_code == 200

        correct_result = pd.DataFrame({
            'original_file_name': [1, 1], 'taxon_id': ['MX.1', 'MX.1'], 'audio_id': [1, 2], 'persons_yes': ['MA.1', 'MA.1']
        })
        df = pd.read_csv(BytesIO(response.data), sep='\t')
        assert df.equals(correct_result)

    def test_returns_recording_results(self):
        response = self.app.get('/result/recording', headers=self._get_auth_headers())
        assert response.status_code == 200
        assert response.mimetype == 'application/zip'

    def _get_auth_headers(self):
        credentials = b64encode(b'test_user:test_password').decode('utf-8')
        return {"Authorization": "Basic {}".format(credentials)}
