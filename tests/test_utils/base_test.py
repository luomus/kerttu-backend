from app import db
from app.models.models import User, Audio, LetterTemplate, LetterCandidate
from app.services.database import letter_service
from app.models.enums import LetterAnnotationEnum
import numpy as np
from tests.test_utils.test_case import TestCase


class BaseTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self._add_test_data()

    def _add_test_data(self):
        for i in range(4):
            user = User(id='MA.{}'.format(i + 1))
            db.session.add(user)

        for j in range(20):
            audio = Audio(id=j+1, file_name='audio_{}'.format(j+1))
            db.session.add(audio)

        taxon_ids = ['MX.1', 'MX.1', 'MX.1', 'MX.2']
        for i in range(4):
            template = LetterTemplate(
                id=i + 1, audio_id=1, taxon_id=taxon_ids[i],
                x1=1, x2=1, y1=1, y2=1, original_file_name='1'
            )
            db.session.add(template)

        db.session.commit()

        cross_correlations = np.linspace(0.05, 1, 20)
        for i in range(4):
            for j in range(20):
                candidate = LetterCandidate(
                    id=i * 20 + j + 1, audio_id=j + 1, template_id=i + 1,
                    cross_correlation=cross_correlations[j],
                    x1=1, x2=1, y_diff=0
                )
                db.session.add(candidate)

        db.session.commit()

    def add_example_letter_annotation_data(self):
        letter_service.set_letter_annotation('MA.1', 1, LetterAnnotationEnum.YES)
        letter_service.set_letter_annotation('MA.1', 2, LetterAnnotationEnum.YES)
        letter_service.set_letter_template_finished('MA.1', 1)
