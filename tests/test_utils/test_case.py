from app import app, db
import unittest


class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
