from unittest.mock import Mock

lajiAPI_mock = Mock()
lajiAPI_mock.get_user_id.return_value = 'MA.1'
lajiAPI_mock.get_user_taxon_expertise.return_value = ['MX.1', 'MX.2', 'MX.3']
