from unittest.mock import Mock

mediaAPI_mock = Mock()
mediaAPI_mock.upload_file.return_value = {'urls': {'mp3': 'test.image.fi/test'}, 'secretKey': 'abc'}
