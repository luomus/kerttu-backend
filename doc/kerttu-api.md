# Kerttu api

## /letter
Letter api is for Kerttu letter annotation. In letter annotation, user gives annotations to letters which are small sound segments. User gets a letter template and a candidate and annotates whether they are from the same species or not.

All subroutes take "personToken" as a parameter. It is used to determine the user and their taxon expertise. The subroutes returns either TemplateResponse, CandidateResponse or AnnotationResponse.

```
TemplateResponse { 
  "template": Template,
  "statusInfo": StatusInfo 
}

CandidateResponse { 
  "candidate": Candidate,
  "statusInfo": StatusInfo 
}

AnnotationResponse { 
  "annotation": whether the candidate has a sound from the same species than the template (annotationEnum[1 = yes, 0 = no, -1 = unsure]),
  "statusInfo": StatusInfo 
}

Template {
  "id": template's id (number),
  "taxonId": id of the taxon which sound is represented in the template (string),
  "recording": url of the recording (string),
  "name": template's name (string),
  "xRange": x coordinates of the template in the recording, in seconds (array[string]),
  "yRange": y coordinates of the template in the recording, in hertz (array[string])
}

Candidate {
  "id": candidate's id (number),
  "recording": url of the recording (string),
  "xRange": x coordinates of the candidate in the recording, in seconds (array[string]),
  "yRange": y coordinates of the candidate in the recording, in hertz (array[string]),
  "crossCorrelation": cross correlation between the template and the candidate (number)
}

StatusInfo {
  "userAnnotationCount": number of annotations for the template by the user (number),
  "targetAnnotationCount": target number of annotations for the template (number),
  "hasPreviousCandidate": whether user can go back to previous candidate (boolean)
}
```

### /template GET
Returns user's current letter template.
#### Returns
```
TemplateResponse
```

### /skipTemplate PUT
Sets user's current template as skipped. Returns a new template for the user.
#### Request data
```
{
  "templateId": current template id
}
```
#### Returns
```
TemplateResponse
```

### /candidate/<template_id> GET
Returns user's current letter candidate.
#### Returns
```
CandidateResponse
```

### /nextCandidate/<template_id>/<candidate_id> GET
Returns user's next letter candidate. It will be user's next candidate after they have given annotation to the current one.
#### Returns
```
CandidateResponse
```

### /previousCandidate/<template_id>/<candidate_id> GET
Returns user's previous candidate and sets it as current.
#### Returns
```
CandidateResponse
```

### /annotation/<template_id>/<candidate_id> PUT
Sets annotation for the current candidate.
#### Request data
```
{
  "annotation": whether the candidate has a sound from the same species than the template (annotationEnum[1 = yes, 0 = no, -1 = unsure])
}
```
#### Returns
```
AnnotationResponse
```

## /recording
Recording api is for Kerttu recording annotation where user annotates the quality of given recording and what birds the recording contains.

All subroutes take "personToken" as a parameter. It is used to determine the user and their taxon expertise. Most subroutes returns RecordingResponse as a response.

```
RecordingResponse { 
  "recording": Recording,
  "annotation": RecordingAnnotation,
  "statusInfo": RecordingStatusInfo 
}

Recording {
  "id": recording's id (number),
  "recording": url of the recording (string),
  "xRange": x coordinates of the extracted recording in the whole recording, in seconds (array[string]),
}

RecordingAnnotattion {
  "isLowQuality": whether the recording is low quality (boolean),
  "containsNoiseCausedByHumanActivity": whether the recording contains noise caused by human activity (boolean),
  "containsHumanSpeech": whether the recording contains human speech (boolean),
  "containsUnknownBirds": whether the recording constains unknown birds for the user (boolean),
  "doesNotContainBirds": whether the recording does not contain birds (boolean),
  "taxonAnnotations": {
    "main": array[TaxonAnnotation],
    "other": array[TaxonAnnotation]
  }
}

TaxonAnnotation {
  "taxonId": id of the taxon (string),
  "annotation": annotation for the taxon in the recording (taxonAnnotationEnum[1 = occurs, 0 = does not occur, -1 = possible occurs]),
  "bird": whether the taxon is bird or not (boolean)
}

RecordingStatusInfo {
  "hasPreviousRecording": whether user can go back to previous recording (boolean)
}
```

### / GET
Returns user's current recording.

#### Returns
```
RecordingResponse
```

### /next/<recording_id> GET
Sets user's current recording as finished and returns a next recording. 

#### Returns
```
RecordingResponse
```

### /previous/<recording_id> GET
Sets user's previous recording as current and returns it.

#### Returns
```
RecordingResponse
```

### /annotation/<recording_id> POST
Updates user's annotation for the recording.

#### RequestData
```
RecordingAnnotation
```
