#!/bin/bash

FILES="wav/*.wav"

mkdir -p mp3

for f in $FILES;
do
    mp3="mp3/$(basename $f .wav).mp3"
    if [ -f "$mp3" ]; then
     echo "${mp3} ALREADY EXISTS.. skipping"
    else
     echo "${mp3}";
     sox "${f}" -C 64.01 "${mp3}" rate 22050;
  fi
done
