#!/bin/bash

FILES="mp3CornellNonPasserines_seg/*.mp3"

for f in $FILES;
do
  f_name="$(basename $f)"
  asset_id=$(echo "$f_name" | cut -d "_" -f1)
  path1=$(echo "$asset_id" | sed "s/.\{1\}/&\//g")
  path=/mnt/DATA/luomus/image_library/c/o/r/n/e/l/l/"$path1"cornell"$asset_id"
  echo $path

  if [[ ! -d "$path" ]]
  then
    mkdir -p "$path"
    echo "Creating folders "$path
  fi
  mv $f $path/$f_name
done
