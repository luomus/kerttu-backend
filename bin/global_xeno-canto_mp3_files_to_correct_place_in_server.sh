#!/bin/bash

FILES="BSG_update_xeno-canto/*.mp3"

for f in $FILES;
do
  f_name="$(basename $f)"
  asset_id=$(echo "$f_name" | cut -d "." -f1)
  path1=$(echo "$asset_id" | sed "s/.\{1\}/&\//g")
  path=/mnt/DATA/luomus/image_library/x/e/n/o/-/c/a/n/t/o/"$path1"xeno-canto"$asset_id"
  echo $path

  if [[ ! -d "$path" ]]
  then
    mkdir -p "$path"
    echo "Creating folders "$path
  fi
  mv $f $path/$f_name
done
