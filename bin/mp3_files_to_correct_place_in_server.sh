#!/bin/bash

FILES="*.tar"
WD=$PWD

for f in $FILES;
do
  f_name="$(basename $f)"
  device_id="$(basename $f .tar)"
  path1=$(echo "$device_id" | sed "s/.\{1\}/&\//g")
  path=/mnt/DATA/luomus/image_library/"$path1""$device_id"

  if [[ ! -d "$path" ]]
  then
    mkdir -p "$path"
    echo "Creating folders "$path
  fi
  mv $f $path/$f_name
  cd $path
  tar -xf "$f_name" --strip-components=1
  rm "$f_name"
  cd $WD
done