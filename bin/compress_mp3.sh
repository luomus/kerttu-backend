#!/bin/bash

FILES="mp3/*.mp3"

# group files to folders
for f in $FILES;
do
  f_name="$(basename $f)"
  device_id="$( cut -d '_' -f 1 <<< "$f_name" )"
  mkdir -p mp3_tar/$device_id
  cp $f mp3_tar/$device_id
done

# tar files
cd mp3_tar
for dir in */;
do
    dir=${dir%*/}      # remove the trailing "/"
    tar -cvf "$dir".tar $dir
    rm -r $dir
done
cd ..

cp mp3_files_to_correct_place_in_server.sh mp3_tar/mp3_files_to_correct_place_in_server.sh