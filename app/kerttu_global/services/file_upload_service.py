from io import BytesIO

from pydub import AudioSegment
from pydub.utils import make_chunks

from pathlib import Path

from app.services import mediaAPI


def upload_file(file, file_name):
    file_parts = _split_file_into_chunks(file)
    return _upload_file_chunks(file_parts, file_name)


def _split_file_into_chunks(file):
    results = []

    data = AudioSegment.from_file(file)

    chuck_duration = 20 * 1000 # 20 seconds
    chunks = make_chunks(data, chuck_duration)

    if len(chunks) > 1 and len(chunks[-1]) < 5 * 1000: # join the last chunk with the second last if it's too short
        chunks[-2] = chunks[-2] + chunks[-1]
        chunks.pop()

    for chunk in chunks:
        duration = len(chunk) / 1000
        f = chunk.export(BytesIO(), format='mp3')
        results.append({'file': f, 'duration': duration})

    return results


def _upload_file_chunks(file_parts, file_name):
    recording_data = []
    start_time = 0

    for index, f in enumerate(file_parts):
        result = mediaAPI.upload_file(f['file'], '{}_{}.mp3'.format(Path(file_name).stem, start_time))

        url = result['urls']['mp3'] + '?secret=' + result['secretKey']

        recording_data.append({
          'url': url,
          'duration': f['duration']
        })

        start_time += int(f['duration'])
        
    return recording_data
