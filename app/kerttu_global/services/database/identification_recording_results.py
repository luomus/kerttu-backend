from app.kerttu_global.models.models import GlobalRecording, GlobalRecordingAnnotation, GlobalArea,\
    GlobalRecordingSpeciesAnnotation, GlobalRecordingSpeciesAnnotationBox, GlobalSpecies
from app.kerttu_global.models.enums import AnnotationStatusEnum
from app.utils.database_utils import query_to_dataframe


def recording_results():
    query = GlobalRecording.query.filter(
        GlobalRecording.annotations.any(
            GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
        )
    ).with_entities(
        GlobalRecording.id.label('recording_id'),
        GlobalRecording.site_id,
        GlobalRecording.url
    )

    df = query_to_dataframe(query)
    df['file_name'] = df['url'].str.split('/').str[-1]

    return df


def annotation_results():
    query = GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    ).join(
        GlobalRecordingAnnotation.recording
    ).join(
        GlobalRecordingAnnotation.non_bird_area, isouter=True
    ).with_entities(
        GlobalRecordingAnnotation.id.label('annotation_id'),
        GlobalRecording.id,
        GlobalRecordingAnnotation.user_id,
        GlobalRecordingAnnotation.is_low_quality,
        GlobalRecordingAnnotation.contains_human_speech,
        GlobalRecordingAnnotation.contains_unknown_birds,
        GlobalRecordingAnnotation.does_not_contain_birds,
        GlobalRecordingAnnotation.contains_birds_not_on_list,
        GlobalRecordingAnnotation.birds_not_on_list,
        GlobalRecordingAnnotation.has_boxes_for_all_bird_sounds,
        GlobalArea.x1.label('non_bird_area_x1'),
        GlobalArea.x2.label('non_bird_area_x2'),
        GlobalArea.y1.label('non_bird_area_y1'),
        GlobalArea.y2.label('non_bird_area_y2'),
        GlobalRecordingAnnotation.created,
        GlobalRecordingAnnotation.edited
    )

    df = query_to_dataframe(query)
    df = df.rename(columns={'id': 'recording_id'})

    return df


def species_annotation_results():
    query = GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    ).join(
        GlobalRecordingAnnotation.recording
    ).join(
        GlobalRecordingAnnotation.species_annotations
    ).join(
        GlobalRecordingSpeciesAnnotation.species
    ).with_entities(
        GlobalRecordingSpeciesAnnotation.id.label('species_annotation_id'),
        GlobalRecording.id,
        GlobalRecordingAnnotation.id.label('annotation_id'),
        GlobalRecordingAnnotation.user_id,
        GlobalSpecies.species_code,
        GlobalSpecies.scientific_name,
        GlobalSpecies.common_name,
        GlobalRecordingSpeciesAnnotation.occurrence
    )

    df = query_to_dataframe(query)
    df = df.rename(columns={'id': 'recording_id'})

    return df


def species_annotation_boxes_results():
    query = GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    ).join(
        GlobalRecordingAnnotation.species_annotations
    ).join(
        GlobalRecordingSpeciesAnnotation.boxes
    ).join(
        GlobalRecordingSpeciesAnnotationBox.area
    ).with_entities(
        GlobalRecordingAnnotation.user_id,
        GlobalRecordingSpeciesAnnotation.id.label('species_annotation_id'),
        GlobalArea.x1.label('area_x1'),
        GlobalArea.x2.label('area_x2'),
        GlobalArea.y1.label('area_y1'),
        GlobalArea.y2.label('area_y2'),
        GlobalRecordingSpeciesAnnotationBox.overlaps_with_other_species,
        GlobalRecordingSpeciesAnnotationBox.sound_type
    )

    df = query_to_dataframe(query)

    return df


def users_with_annotations():
    users = GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    ).with_entities(
        GlobalRecordingAnnotation.user_id
    ).distinct().all()

    return [user_id for (user_id,) in users]
