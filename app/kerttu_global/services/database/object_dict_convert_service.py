from app.kerttu_global.services.database.species_name_service import get_common_name
from app.kerttu_global.models.enums import TaxonCategoryEnum
from app.kerttu_global.models.models import GlobalRecordingAnnotation, GlobalRecordingSpeciesAnnotation, GlobalArea, \
    GlobalRecordingSpeciesAnnotationBox, GlobalRecordingSpeciesAnnotationBoxGroup


def species_to_dict(species, lang, include_extra_info=False):
    result = {
        'id': species.id,
        'scientificName': species.scientific_name,
        'commonName': get_common_name(species, lang)
    }

    if include_extra_info:
        result['isSpecies'] = species.category == TaxonCategoryEnum.SPECIES
        result['taxonOrder'] = species.taxon_order

    return result


def continent_to_dict(continent):
    return {
        'id': continent.id,
        'name': continent.name
    }


def order_to_dict(order):
    return {
        'id': order.id,
        'scientificName': order.scientific_name
    }


def family_to_dict(family):
    return {
        'id': family.id,
        'order': family.order_id,
        'scientificName': family.scientific_name
    }


def validation_recording_to_dict(audio, audio_metadata, lang):
    return {
        'audio': validation_audio_to_dict(audio, audio_metadata, lang),
        'candidates': [
            validation_candidate_to_dict(candidate) for candidate in sorted(audio.candidates, key=lambda x: x.x1)
        ]
    }


def validation_audio_to_dict(audio, audio_metadata, lang):
    return {
        'id': audio.id,
        'url': audio.url,
        'spectrogramUrl': audio.spectrogram_url,
        'species': species_to_dict(audio_metadata.species, lang, True),
        'recordist': audio_metadata.recordist,
        'country': audio_metadata.country,
        'state': audio_metadata.state,
        'location': audio_metadata.location,
        'year': audio_metadata.year,
        'month': audio_metadata.month,
        'day': audio_metadata.day,
        'assetId': audio_metadata.asset_id,
        'specimenUrl': audio_metadata.specimen_url,
        'checklistId': audio_metadata.ebird_checklist_id,
        'checklistUrl': audio_metadata.ebird_checklist_url,
        'licenseUrl': audio_metadata.license_url,
        'soundType': audio_metadata.sound_type
    }


def validation_candidate_to_dict(candidate):
    return {
        'xRange': [candidate.x1, candidate.x2],
        'yRange': [candidate.y1, candidate.y2]
    }


def identification_site_to_dict(site):
    return {
        'id': site.id,
        'name': site.name,
        'country': site.country,
        'geometry': {
            'type': 'Point',
            'coordinates': [site.longitude_coarse, site.latitude_coarse]
        }
    }


def identification_recording_to_dict(recording, lang):
    if recording is not None:
        result = {
            'id': recording.id,
            'url': recording.url,
            'dateTime': str(recording.date_time) if recording.date_time is not None else None,
            'xRange': [recording.x1, recording.x2],
            'duration': recording.duration,
            'locality': recording.locality,
            'taxonType': int(recording.taxon_type),
            'site': {
                'name': recording.site.name,
                'country': recording.site.country
            }
        }

        if recording.target_species is not None:
            result['targetSpecies'] = species_to_dict(recording.target_species, lang)

        return result


def identification_recording_annotation_to_dict(annotation):
    if annotation is not None:
        non_bird_area = None
        if annotation.non_bird_area is not None:
            non_bird_area = {
                'xRange': [annotation.non_bird_area.x1, annotation.non_bird_area.x2],
                'yRange': [annotation.non_bird_area.y1, annotation.non_bird_area.y2]
            }

        species_annotations = []

        for species_annotation_obj in annotation.species_annotations:
            species_annotation = {
                'speciesId': species_annotation_obj.species_id,
                'occurrence': species_annotation_obj.occurrence
            }

            boxes = []
            group_index = {}

            for box_obj in species_annotation_obj.boxes:
                if box_obj.group is not None:
                    if box_obj.group_id not in group_index:
                        boxes.append({'boxes': []})
                        group_index[box_obj.group_id] = len(boxes) - 1

                    idx = group_index[box_obj.group_id]
                    boxes[idx]['boxes'].append(identification_recording_annotation_box_to_dict(box_obj))
                else:
                    boxes.append(identification_recording_annotation_box_to_dict(box_obj))

            species_annotation['boxes'] = boxes
            species_annotations.append(species_annotation)

        return {
            'created': format_date(annotation.created),
            'edited': format_date(annotation.edited),
            'isLowQuality': annotation.is_low_quality,
            'containsHumanSpeech': annotation.contains_human_speech,
            'containsUnknownBirds': annotation.contains_unknown_birds,
            'doesNotContainBirds': annotation.does_not_contain_birds,
            'containsBirdsNotOnList': annotation.contains_birds_not_on_list,
            'birdsNotOnList': annotation.birds_not_on_list,
            'hasBoxesForAllBirdSounds': annotation.has_boxes_for_all_bird_sounds,
            'nonBirdArea': non_bird_area,
            'speciesAnnotations': species_annotations
        }


def identification_recording_annotation_box_to_dict(box_obj):
    box = {
        'area': {
            'xRange': [box_obj.area.x1, box_obj.area.x2],
            'yRange': [box_obj.area.y1, box_obj.area.y2]
        },
        'overlapsWithOtherSpecies': box_obj.overlaps_with_other_species
    }

    if box_obj.sound_type is not None:
        box['soundType'] = box_obj.sound_type

    return box


def identification_recording_annotation_from_dict(user_id, recording_id, data):
    annotation_obj = GlobalRecordingAnnotation(
        user_id=user_id,
        recording_id=recording_id,
        species_annotations=[]
    )

    annotation_obj.is_low_quality = data.get('isLowQuality', False)
    annotation_obj.contains_human_speech = data.get('containsHumanSpeech', False)
    annotation_obj.contains_unknown_birds = data.get('containsUnknownBirds', False)
    annotation_obj.does_not_contain_birds = data.get('doesNotContainBirds', False)
    annotation_obj.contains_birds_not_on_list = data.get('containsBirdsNotOnList', False)
    if annotation_obj.contains_birds_not_on_list:
        annotation_obj.birds_not_on_list = data.get('birdsNotOnList', None)
    else:
        annotation_obj.birds_not_on_list = None
    annotation_obj.has_boxes_for_all_bird_sounds = data.get('hasBoxesForAllBirdSounds', False)

    non_bird_area = data.get('nonBirdArea', None)
    if non_bird_area is not None:
        annotation_obj.non_bird_area = GlobalArea(
            x1=non_bird_area['xRange'][0],
            x2=non_bird_area['xRange'][1],
            y1=non_bird_area['yRange'][0],
            y2=non_bird_area['yRange'][1]
        )
    else:
        annotation_obj.non_bird_area = None

    species_annotations = data.get('speciesAnnotations', [])
    species_ids = []

    for species_annotation in species_annotations:
        species_id = species_annotation['speciesId']
        if species_id in species_ids:
            continue

        species_annotation_obj = GlobalRecordingSpeciesAnnotation(
            species_id=species_id,
            occurrence=species_annotation['occurrence']
        )

        boxes = species_annotation['boxes'] if 'boxes' in species_annotation else []
        for box in boxes:
            if 'boxes' in box:
                box_group = GlobalRecordingSpeciesAnnotationBoxGroup()
                for b in box['boxes']:
                    box_obj = identification_recording_annotation_box_from_dict(b)
                    if box_obj is not None:
                        box_obj.group = box_group
                        species_annotation_obj.boxes.append(box_obj)
            else:
                box_obj = identification_recording_annotation_box_from_dict(box)
                if box_obj is not None:
                    species_annotation_obj.boxes.append(box_obj)

        annotation_obj.species_annotations.append(species_annotation_obj)
        species_ids.append(species_id)

    return annotation_obj


def identification_recording_annotation_box_from_dict(box):
    if 'area' in box and 'xRange' in box['area'] and 'yRange' in box['area']:
        box_obj = GlobalRecordingSpeciesAnnotationBox(
            area=GlobalArea(
                x1=box['area']['xRange'][0],
                x2=box['area']['xRange'][1],
                y1=box['area']['yRange'][0],
                y2=box['area']['yRange'][1]
            )
        )
        if 'overlapsWithOtherSpecies' in box and box['overlapsWithOtherSpecies']:
            box_obj.overlaps_with_other_species = True
        if 'soundType' in box:
            box_obj.sound_type = box['soundType']
        return box_obj


def format_date(date):
    return date.astimezone().strftime('%Y-%m-%dT%H:%M:%S%z')
