from app.kerttu_global.models.enums import TaxonCategoryEnum
from app.kerttu_global.models.models import GlobalSpecies, GlobalVersion, GlobalValidation, GlobalAudioMetadata,\
    GlobalSpeciesLock, GlobalTemplate, GlobalAudio
from app.kerttu_global.services.database.species_name_service import get_common_name_field
import re
from sqlalchemy.sql import func, case
from sqlalchemy import and_, or_, nullsfirst, nullslast, union_all, literal
from datetime import datetime, timedelta
from app import app, db


def get_species_list_query(
        user_id,
        taxon_type,
        continent,
        order,
        family,
        include_species_without_audio,
        only_unvalidated,
        search_query,
        order_by_string,
        lang='en'
):
    query = _get_main_species_query(taxon_type, continent, order, family, include_species_without_audio)
    query = _filter_by_search_query(query, search_query, lang)

    query, subquery = _version_count_to_species_query(query)
    query, subquery2 = _validation_count_to_species_query(query, only_unvalidated)
    query, subquery3 = _user_has_validated_to_species_query(query, user_id)
    query, subquery4, has_modifications = _has_modifications_to_species_query(query, user_id)
    query, subquery5 = _missing_templates_to_species_query(query)
    query, subquery6 = _locked_to_species_query(query, user_id)
    subqueries = [subquery, subquery2, subquery3, subquery4, subquery5, subquery6]

    query = _order_species_query(query, subqueries, order_by_string, search_query, lang)

    selected_fields = [
        GlobalSpecies.id,
        GlobalSpecies.scientific_name,
        get_common_name_field(lang),
        subquery.c.version_count,
        subquery2.c.validation_count,
        subquery3.c.user_has_validated,
        subquery4.c.has_modifications,
        subquery5.c.missing_templates,
        subquery6.c.locked
    ]

    if include_species_without_audio:
        query, subquery7 = _has_audio_to_species_query(query)
        selected_fields.append(subquery7.c.has_audio)

    query = query.with_entities(*selected_fields)
    return query, has_modifications


def get_species_with_validation_count_query(species_id, lang='en'):
    query = GlobalSpecies.query.filter(
        GlobalSpecies.id == species_id
    )
    query, subquery = _validation_count_to_species_query(query, False, species_id)
    query, subquery2 = _has_audio_to_species_query(query, species_id)
    query = query.with_entities(
        GlobalSpecies.id,
        GlobalSpecies.scientific_name,
        get_common_name_field(lang),
        subquery.c.validation_count,
        subquery2.c.has_audio
    )
    return query


def get_species_validation_counts_query(continent, order, family):
    query = _get_main_species_query(None, continent, order, family)
    query, subquery = _validation_count_to_species_query(query, False)

    query = query.with_entities(
        subquery.c.validation_count.label('validation_count'),
        func.count(GlobalSpecies.id).label('count')
    ).group_by(
        subquery.c.validation_count
    )

    return query


def get_user_counts_query(continent, order, family):
    query = GlobalVersion.query

    filters = _get_region_and_taxonomy_filters(continent, order, family)
    if filters is not None:
        query = query.filter(GlobalVersion.species.has(filters))

    query1 = _filter_only_first_version(query).with_entities(
        GlobalVersion.user_id.label('user_id'),
        GlobalVersion.species_id.label('species_id'),
        literal(1).label('first_version_count'),
        literal(0).label('version_count'),
        literal(0).label('validation_count'),
    ).group_by(GlobalVersion.user_id, GlobalVersion.species_id)

    query2 = query.with_entities(
        GlobalVersion.user_id.label('user_id'),
        GlobalVersion.species_id.label('species_id'),
        literal(0).label('first_version_count'),
        literal(1).label('version_count'),
        literal(0).label('validation_count')
    ).group_by(GlobalVersion.user_id, GlobalVersion.species_id)

    query3 = query.join(
        GlobalVersion.validations
    ).with_entities(
        GlobalValidation.user_id.label('user_id'),
        GlobalVersion.species_id.label('species_id'),
        literal(0).label('first_version_count'),
        literal(0).label('version_count'),
        literal(1).label('validation_count')
    ).group_by(GlobalValidation.user_id, GlobalVersion.species_id)

    q = union_all(query1, query2, query3).alias('combined')
    q = db.session.query(q).with_entities(
        q.c.user_id,
        q.c.species_id,
        func.sum(q.c.first_version_count).label('first_version_count'),
        func.sum(q.c.version_count).label('version_count'),
        func.sum(q.c.validation_count).label('validation_count')
    ).group_by(q.c.user_id, q.c.species_id).subquery()

    query = db.session.query(q).with_entities(
        q.c.user_id,
        func.sum(q.c.first_version_count).label('species_created_count'),
        func.sum(
            case([(
                and_(q.c.first_version_count == 0, or_(q.c.version_count == 1, q.c.validation_count == 1)),
                literal(1)
            )], else_=literal(0))
        ).label('species_validated_count')
    ).group_by(q.c.user_id)

    return query


def get_template_results_query(continent, order, family, min_validations):
    query = _filter_only_last_version(GlobalVersion.query)

    filters = _get_region_and_taxonomy_filters(continent, order, family)
    if filters is not None:
        query = query.filter(GlobalVersion.species.has(filters))

    if min_validations > 0:
        subquery = query.filter(
            GlobalVersion.missing_templates == False
        ).join(
            GlobalVersion.validations, isouter=True
        ).with_entities(
            GlobalVersion.species_id,
            (
                func.count(func.distinct(GlobalVersion.id)) + func.count(func.distinct(GlobalValidation.id))
            ).label('validation_count')
        ).group_by(GlobalVersion.species_id).subquery()

        query = query.join(
            subquery,
            (GlobalVersion.species_id == subquery.c.species_id)
        ).filter(
            subquery.c.validation_count >= min_validations
        )

    query = query.join(
        GlobalVersion.species
    ).join(
        GlobalVersion.templates, isouter=True
    ).join(
        GlobalTemplate.audio
    ).order_by(
        GlobalSpecies.taxon_order,
        GlobalTemplate.template_number
    ).with_entities(
        GlobalSpecies.species_code,
        GlobalSpecies.common_name,
        GlobalSpecies.scientific_name,
        GlobalAudio.url,
        GlobalTemplate.x1,
        GlobalTemplate.x2,
        GlobalTemplate.y1,
        GlobalTemplate.y2
    )

    return query


def _get_main_species_query(
    taxon_type=None, continent=None, order=None, family=None, include_species_without_audio=False
):
    query = GlobalSpecies.query.filter(
        GlobalSpecies.category == TaxonCategoryEnum.SPECIES
    )

    if taxon_type is not None:
        query = query.filter(
            GlobalSpecies.taxon_type == taxon_type
        )
    
    if not include_species_without_audio:
        query = query.filter(
            or_(
                GlobalSpecies.audio_metadata.any(
                    GlobalAudioMetadata.audio.any()
                ),
                GlobalSpecies.children.any(
                    GlobalSpecies.audio_metadata.any(
                        GlobalAudioMetadata.audio.any()
                    )
                )
            )
        )

    filters = _get_region_and_taxonomy_filters(continent, order, family)
    if filters is not None:
        query = query.filter(filters)

    return query


def _get_region_and_taxonomy_filters(continent=None, order=None, family=None):
    filters = None

    if continent is not None:
        filters = _add_to_filters(filters, GlobalSpecies.continents.any(id=continent))
    if order is not None:
        filters = _add_to_filters(filters, GlobalSpecies.order_id == order)
    if family is not None:
        filters = _add_to_filters(filters, GlobalSpecies.family_id == family)

    return filters


def _add_to_filters(filters, new_filter):
    if filters is None:
        return new_filter
    return and_(filters, new_filter)


def _filter_by_search_query(query, search_query, lang):
    if search_query is not None and len(search_query) > 0:
        query = query.filter(
            or_(
                get_common_name_field(lang).ilike('%{}%'.format(search_query)),
                GlobalSpecies.scientific_name.ilike('%{}%'.format(search_query))
            )
        )

    return query


def _version_count_to_species_query(query):
    subquery = GlobalVersion.query.with_entities(
        GlobalVersion.species_id,
        func.count(GlobalVersion.id).label('version_count')
    ).group_by(GlobalVersion.species_id).subquery()

    query = query.join(
        subquery,
        (GlobalSpecies.id == subquery.c.species_id),
        isouter=True
    )

    return query, subquery


def _validation_count_to_species_query(query, only_unvalidated=False, species_id_filter=None):
    complete_version_query = GlobalVersion.query.filter(
        GlobalVersion.missing_templates == False
    )
    if species_id_filter is not None:
        complete_version_query = complete_version_query.filter(
            GlobalVersion.species_id == species_id_filter
        )

    subquery = _filter_only_last_version(complete_version_query).join(
        GlobalVersion.validations, isouter=True
    ).with_entities(
        GlobalVersion.species_id,
        (
            func.count(func.distinct(GlobalVersion.id)) + func.count(func.distinct(GlobalValidation.id))
        ).label('validation_count')
    ).group_by(GlobalVersion.species_id).subquery()

    query = _join_species_query_with_subquery(query, subquery)

    if only_unvalidated is True:
        query = query.filter(
            subquery.c.validation_count == None
        )

    return query, subquery


def _validation_count_to_version_query(query, min_validations=None):
    subquery = query.filter(
        GlobalVersion.missing_templates == False
    ).join(
        GlobalVersion.validations, isouter=True
    ).with_entities(
        GlobalVersion.species_id,
        (
            func.count(func.distinct(GlobalVersion.id)) + func.count(func.distinct(GlobalValidation.id))
        ).label('validation_count')
    ).group_by(GlobalVersion.species_id).subquery()

    query = query.join(
        subquery,
        (GlobalVersion.species_id == subquery.c.species_id)
    ).filter(
        subquery.c.validation_count > min_validations
    )
    return query, subquery


def _user_has_validated_to_species_query(query, user_id):
    subquery = GlobalVersion.query.filter(
        or_(
            GlobalVersion.user_id == user_id,
            GlobalVersion.validations.any(
                GlobalValidation.user_id == user_id
            )
        )
    ).with_entities(
        GlobalVersion.species_id,
        literal(1).label('user_has_validated')
    ).group_by(GlobalVersion.species_id).subquery()

    query = _join_species_query_with_subquery(query, subquery)

    return query, subquery


def _has_modifications_to_species_query(query, user_id):
    user_latest_subquery = GlobalVersion.query.filter(
        or_(
            GlobalVersion.user_id == user_id,
            GlobalVersion.validations.any(
                GlobalValidation.user_id == user_id
            )
        )
    ).with_entities(
        GlobalVersion.species_id,
        func.max(GlobalVersion.created).label('max_date')
    ).group_by(GlobalVersion.species_id).subquery()

    has_modifications_query = GlobalVersion.query.join(
        user_latest_subquery,
        (GlobalVersion.species_id == user_latest_subquery.c.species_id)
    ).filter(
        GlobalVersion.created > user_latest_subquery.c.max_date
    ).with_entities(
        GlobalVersion.species_id,
        literal(1).label('has_modifications')
    ).group_by(GlobalVersion.species_id)

    has_modifications = has_modifications_query.count() > 0
    subquery = has_modifications_query.subquery()
    query = _join_species_query_with_subquery(query, subquery)

    return query, subquery, has_modifications


def _locked_to_species_query(query, user_id):
    subquery = GlobalSpeciesLock.query.filter(
        GlobalSpeciesLock.user_id != user_id,
        GlobalSpeciesLock.created >= datetime.now() - timedelta(days=app.config['GLOBAL_SPECIES_LOCK_EXPIRES_IN_DAYS'])
    ).with_entities(
        GlobalSpeciesLock.species_id,
        literal(1).label('locked')
    ).group_by(GlobalSpeciesLock.species_id).subquery()

    query = _join_species_query_with_subquery(query, subquery)

    return query, subquery


def _has_audio_to_species_query(query, species_id_filter=None):
    species_query = GlobalSpecies.query

    if species_id_filter is not None:
        species_query = species_query.filter(
            GlobalSpecies.id == species_id_filter
        )

    subquery = species_query.filter(
        or_(
            GlobalSpecies.audio_metadata.any(
                GlobalAudioMetadata.audio.any()
            ),
            GlobalSpecies.children.any(
                GlobalSpecies.audio_metadata.any(
                    GlobalAudioMetadata.audio.any()
                )
            )
        )
    ).with_entities(
        GlobalSpecies.id.label('species_id'),
        literal(1).label('has_audio')
    ).group_by(GlobalSpecies.id).subquery()

    query = _join_species_query_with_subquery(query, subquery)

    return query, subquery


def _missing_templates_to_species_query(query):
    subquery = _filter_only_last_version(GlobalVersion.query).filter(
        GlobalVersion.missing_templates == True
    ).with_entities(
        GlobalVersion.species_id,
        literal(1).label('missing_templates')
    ).group_by(GlobalVersion.species_id).subquery()

    query = _join_species_query_with_subquery(query, subquery)

    return query, subquery


def _order_species_query(query, subqueries, order_by_string, search_query, lang):
    order_by = _parse_species_order_by(order_by_string, subqueries, search_query, lang)
    order_by.append(GlobalSpecies.taxon_order)

    return query.order_by(*order_by)


def _parse_species_order_by(value, subqueries, search_query, lang):
    common_name_field = get_common_name_field(lang)
    order_by = []

    for order_string in value.split(','):
        match = re.match(
            r'^(scientificName|commonName|versionCount|validationCount|userHasValidated|notifications|searchQuery) ' 
            '(ASC|DESC)$',
            order_string
        )
        if match is not None:
            prop = match.group(1)
            direction = match.group(2)

            orders = []
            if prop == 'scientificName':
                orders.append(GlobalSpecies.scientific_name)
            elif prop == 'commonName':
                orders.append(common_name_field)
            elif prop == 'versionCount':
                orders.append(subqueries[0].c.version_count)
            elif prop == 'validationCount':
                orders.append(subqueries[1].c.validation_count)
            elif prop == 'userHasValidated':
                orders.append(subqueries[2].c.user_has_validated)
            elif prop == 'notifications':
                orders.append(subqueries[3].c.has_modifications)
                orders.append(subqueries[4].c.missing_templates)
                orders.append(subqueries[5].c.locked)
            elif prop == 'searchQuery':
                orders.append(case([(
                    common_name_field.ilike('{}%'.format(search_query)),
                    literal(1)
                ), (
                    GlobalSpecies.scientific_name.ilike('{}%'.format(search_query)),
                    literal(2)
                ), (
                    common_name_field.ilike('%{}'.format(search_query)),
                    literal(5)
                ), (
                    GlobalSpecies.scientific_name.ilike('%{}'.format(search_query)),
                    literal(6)
                ), (
                    common_name_field.ilike('%{}%'.format(search_query)),
                    literal(3)
                )], else_=literal(4)))

            if direction == 'DESC':
                orders = [nullslast(order.desc()) for order in orders]
            else:
                orders = [nullsfirst(order.asc()) for order in orders]

            order_by += orders

    return order_by


def _join_species_query_with_subquery(query, subquery, is_outer=True):
    query = query.join(
        subquery,
        (GlobalSpecies.id == subquery.c.species_id),
        isouter=is_outer
    )
    return query


def _filter_only_last_version(query):
    return _filter_only_last_or_first_version(query, True)


def _filter_only_first_version(query):
    return _filter_only_last_or_first_version(query, False)


def _filter_only_last_or_first_version(query, last_version):
    target_func = func.max if last_version else func.min

    subquery = query.with_entities(
        GlobalVersion.species_id,
        target_func(GlobalVersion.created).label('target_date')
    ).group_by(GlobalVersion.species_id).subquery()

    query = query.join(
        subquery,
        (GlobalVersion.species_id == subquery.c.species_id)
    ).filter(
        GlobalVersion.created == subquery.c.target_date
    )

    return query
