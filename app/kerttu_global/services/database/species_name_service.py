from app.kerttu_global.models.models import GlobalSpecies


def get_common_name(species, lang):
    if lang in ['es', 'fr'] and getattr(species, 'common_name_{}'.format(lang)) is not None:
        return getattr(species, 'common_name_{}'.format(lang))
    else:
        return species.common_name


def get_common_name_field(lang):
    if lang == 'es':
        return GlobalSpecies.common_name_es
    elif lang == 'fr':
        return GlobalSpecies.common_name_fr
    else:
        return GlobalSpecies.common_name
