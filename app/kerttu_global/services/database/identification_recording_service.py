from app import db, app
from app.kerttu_global.models.enums import AnnotationStatusEnum, TaxonTypeEnum
from app.kerttu_global.models.models import GlobalRecording, GlobalRecordingAnnotation,\
    GlobalSitePermission, GlobalSite, GlobalRecordingGroup, GlobalSpecies
from app.utils.database_utils import pick_random_from_query
from sqlalchemy.sql import func, or_, and_
from datetime import datetime


################################################################################
# Methods related to sites

def get_site_for_device(device_id):
    return GlobalSite.query.filter(
        GlobalSite.device_id == device_id
    ).first()


################################################################################
# Methods related to recordings

def add_recording_data(recording_data, site_id, system_id, original_file_name,  date, taxon_type=TaxonTypeEnum.BIRD):
    new_recordings = []
    group = GlobalRecordingGroup(name=original_file_name)

    for index, data in enumerate(recording_data):
        new_recordings.append(
            GlobalRecording(
                url=data['url'],
                site_id=site_id,
                x1=0,
                x2=data['duration'],
                duration=data['duration'],
                date_time=date,
                taxon_type=taxon_type,
                group=group,
                group_order_number=index + 1 if group is not None else None,
                uploaded_by=system_id
            )
        )

    db.session.add_all(new_recordings)
    db.session.commit()


def get_recording(recording_id):
    return GlobalRecording.query.get(recording_id)


def get_recording_for_user(user_id, sites, user_limit, previous_recording_id=None, id_blacklist=[], file_name_filter=None):
    previous_recording = get_recording(previous_recording_id)

    next_recording_part = _get_next_recording_part_for_user(user_id, sites, previous_recording, id_blacklist, file_name_filter)
    if next_recording_part is not None:
        return next_recording_part

    unfinished_recording = _get_unfinished_recording_for_user(user_id, sites, id_blacklist, file_name_filter)
    if unfinished_recording is not None:
        return unfinished_recording

    species_id = _get_next_species_for_user(user_id, sites, id_blacklist, file_name_filter)
    recording = _get_random_recording_for_user(user_id, sites, user_limit, id_blacklist, file_name_filter, species_id)

    first_recording_part = _get_next_recording_part_for_user(user_id, sites, recording, id_blacklist, file_name_filter)
    if first_recording_part is not None:
        return first_recording_part

    return recording


def _get_next_species_for_user(user_id, sites, id_blacklist, file_name_filter):
    result = _get_recordings_for_user_query(user_id, sites, id_blacklist, file_name_filter).join(
        GlobalRecording.target_species
    ).order_by(
        GlobalSpecies.taxon_order,
        GlobalSpecies.scientific_name
    ).with_entities(
        GlobalSpecies.id
    ).first()

    if result is not None:
        return result[0]


def _get_random_recording_for_user(user_id, sites, user_limit, id_blacklist, file_name_filter, species_id):
    query = _get_recordings_for_user_query(user_id, sites, id_blacklist, file_name_filter).filter(
        GlobalRecording.target_species_id == species_id
    ).join(
        GlobalRecording.annotations, isouter=True
    ).group_by(
        GlobalRecording
    ).having(
        func.count(func.distinct(GlobalRecordingAnnotation.user_id)) < user_limit
    )

    return pick_random_from_query(query)


def _get_next_recording_part_for_user(user_id, sites, recording, id_blacklist, file_name_filter):
    if recording is None or recording.group is None:
        return None

    return _get_recordings_for_user_query(user_id, sites, id_blacklist, file_name_filter).filter(
        GlobalRecording.group == recording.group,
    ).order_by(
        GlobalRecording.group_order_number
    ).first()


def _get_unfinished_recording_for_user(user_id, sites, id_blacklist, file_name_filter):
    return _get_recordings_for_user_query(user_id, sites, id_blacklist, file_name_filter).filter(
        GlobalRecording.annotations.any(
            (GlobalRecordingAnnotation.user_id == user_id) &
            (GlobalRecordingAnnotation.status == AnnotationStatusEnum.NOT_READY)
        )
    ).first()


################################################################################
# Methods related to recording annotations

def get_recording_annotation(user_id, recording_id):
    return GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.user_id == user_id,
        GlobalRecordingAnnotation.recording_id == recording_id
    ).first()


def save_recording_annotation(recording_annotation):
    curr_date = datetime.now()
    recording_annotation.edited = curr_date

    previous_annotation = get_recording_annotation(
        recording_annotation.user_id,
        recording_annotation.recording_id
    )
    if previous_annotation is not None:
        _update_object(previous_annotation, recording_annotation)
    else:
        recording_annotation.created = curr_date
        db.session.add(recording_annotation)

    db.session.commit()


def set_recording_annotation_status(user_id, recording_id, status):
    annotation = get_recording_annotation(user_id, recording_id)

    if annotation is None:
        save_recording_annotation(GlobalRecordingAnnotation(user_id=user_id, recording_id=recording_id, status=status))
    else:
        annotation.status = status
        db.session.commit()


def check_recording_annotation_is_valid(annotation):
    if annotation is None or not (
            annotation.is_low_quality or
            annotation.does_not_contain_birds or
            annotation.contains_unknown_birds or
            annotation.contains_birds_not_on_list or
            len(annotation.species_annotations) > 0
    ):
        return False
    return True

################################################################################
# Helper functions


def _update_object(prev_object, new_object):
    for key, value in new_object.__dict__.items():
        if key[0] != '_':
            setattr(new_object, key, [] if isinstance(value, list) else None)
            setattr(prev_object, key, value)


def _get_recordings_for_user_query(user_id, sites, id_blacklist=[], file_name_filter=None):
    query = GlobalRecording.query.filter(
        GlobalRecording.id.not_in(id_blacklist),
        GlobalRecording.site.has(
            and_(
                GlobalSite.id.in_(sites),
                or_(
                    GlobalSite.limit_access != True,
                    GlobalSite.permissions.any(
                        GlobalSitePermission.user_id == user_id
                    )
                )
            )
        ),
        ~GlobalRecording.annotations.any(
            (GlobalRecordingAnnotation.user_id == user_id) &
            (GlobalRecordingAnnotation.status != AnnotationStatusEnum.NOT_READY)
        )
    )

    if file_name_filter:
        query = query.filter(
            GlobalRecording.url.like('{}/%/%{}%'.format(app.config['IMAGE_LAJI_URL'], file_name_filter))
        )

    return query
