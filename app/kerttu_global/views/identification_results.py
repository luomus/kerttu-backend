from app import app, auth
from app.utils.view_utils import stream_response
from app.utils.database_utils import dataframe_to_tsv, dataframe_to_tsv_grouped_by_user
from app.services import lajistore
from app.kerttu_global.services.database import identification_recording_results
from flask import Blueprint
import tempfile
import os
import shutil
import pandas as pd

global_identification_results_page = Blueprint('global_identification_results_page', __name__)


@global_identification_results_page.route('/recordings', methods=['GET'])
@auth.login_required
def record_results():
    os.makedirs(app.config['TMP_FOLDER'], exist_ok=True)
    tmp_folder = tempfile.mkdtemp(dir=app.config['TMP_FOLDER'])
    file_path = recording_results_to_file(tmp_folder)
    return stream_response(file_path, tmp_folder, 'bsg_identification_results.zip', 'application/zip')


@global_identification_results_page.route('/users', methods=['GET'])
@auth.login_required
def user_results():
    os.makedirs(app.config['TMP_FOLDER'], exist_ok=True)
    tmp_folder = tempfile.mkdtemp(dir=app.config['TMP_FOLDER'])
    file_path = user_results_to_file(tmp_folder)
    return stream_response(file_path, tmp_folder, 'bsg_identification_users.tsv', 'text/tab-separated-value')


def recording_results_to_file(tmp_folder):
    data_folder = os.path.join(tmp_folder, 'data')
    os.makedirs(data_folder)

    df = identification_recording_results.recording_results()
    file_path = os.path.join(data_folder, 'recordings.tsv')
    dataframe_to_tsv(df, file_path)

    df = identification_recording_results.annotation_results()
    path = os.path.join(data_folder, 'annotations')
    dataframe_to_tsv_grouped_by_user(df, path)

    df = identification_recording_results.species_annotation_results()
    path = os.path.join(data_folder, 'species_annotations')
    dataframe_to_tsv_grouped_by_user(df, path)

    df = identification_recording_results.species_annotation_boxes_results()
    path = os.path.join(data_folder, 'species_annotation_boxes')
    dataframe_to_tsv_grouped_by_user(df, path)

    path = os.path.join(tmp_folder, 'bsg_identification_results')
    return shutil.make_archive(path, 'zip', data_folder)


def user_results_to_file(tmp_folder):
    user_ids = identification_recording_results.users_with_annotations()
    data = lajistore.get_global_expertise_data(user_ids)
    df = pd.DataFrame(data)

    file_path = os.path.join(tmp_folder, 'bsg_identification_users.tsv')
    dataframe_to_tsv(df, file_path)
    return file_path
