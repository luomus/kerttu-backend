from app import cache
from app.kerttu_global.models.enums import AnnotationStatusEnum
from app.kerttu_global.models.models import GlobalRecordingAnnotation, GlobalRecording, \
    GlobalRecordingSpeciesAnnotation, GlobalRecordingSpeciesAnnotationBox, GlobalSpecies

from app.utils.database_utils import query_to_dataframe
from app.utils.view_utils import requires_user
from app.kerttu_global.services.database.species_name_service import get_common_name_field
from flask import Blueprint, jsonify, request
from sqlalchemy import func
import pandas as pd

global_identification_statistics_page = Blueprint('global_identification_statistics_page', __name__)


@cache.memoize(timeout=600)
@global_identification_statistics_page.route('sites', methods=['GET'])
def site_stats():
    data = GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    ).join(
        GlobalRecordingAnnotation.recording
    ).with_entities(
        GlobalRecording.site_id,
        func.count(GlobalRecordingAnnotation.id)
    ).group_by(
        GlobalRecording.site_id
    ).all()

    results = [{
        'siteId': d[0],
        'count': d[1]
    } for d in data]

    return jsonify({
        'results': results
    })


@cache.memoize(timeout=600)
@global_identification_statistics_page.route('/users', methods=['GET'])
def user_stats():
    data = _get_user_data()

    return jsonify({
        'results': data.rename(columns={
            'user_id': 'userId',
            'annotation_count': 'annotationCount',
            'species_count': 'speciesCount',
            'distinct_species_count': 'distinctSpeciesCount',
            'drawn_boxes_count': 'drawnBoxesCount'
        }).to_dict(orient='records'),
        'totalDistinctSpeciesCount': _get_distinct_species_count()
    })


@global_identification_statistics_page.route('/species', methods=['GET'])
def species_stats():
    lang = request.args.get('lang', 'en', type=str)
    return _get_all_species_stats(lang)


@global_identification_statistics_page.route('/ownSpecies', methods=['GET'])
@requires_user
def own_species_stats(user_id):
    lang = request.args.get('lang', 'en', type=str)
    return _get_species_stats(user_id, lang)


@cache.memoize(timeout=600)
def _get_all_species_stats(lang='en'):
    return _get_species_stats(None, lang)


def _get_species_stats(user_id=None, lang='en'):
    data = _get_species_data(user_id, lang)

    return jsonify({
        'results': data.rename(columns={
            'scientific_name': 'scientificName',
            'common_name': 'commonName',
            'drawn_boxes_count': 'drawnBoxesCount'
        }).to_dict(orient='records')
    })


def _get_user_data():
    df1 = query_to_dataframe(_get_user_query())
    df2 = query_to_dataframe(_get_distinct_species_count_by_user_query())

    df = pd.merge(df1, df2, on='user_id', how='left')
    df = df.fillna(0)
    df = df.sort_values(
        by=['annotation_count', 'species_count', 'distinct_species_count', 'drawn_boxes_count'],
        ascending=False
    )
    return df


def _get_species_data(user_id=None, lang='en'):
    df = query_to_dataframe(_get_species_query(user_id, lang))

    numeric_columns = df.select_dtypes(include=['number']).columns
    non_numeric_columns = df.columns.difference(numeric_columns)

    df[numeric_columns] = df[numeric_columns].fillna(0)
    df[non_numeric_columns] = df[non_numeric_columns].fillna('')

    df = df.sort_values(
        by=['count', 'drawn_boxes_count', 'taxon_order'],
        ascending=[False, False, True]
    )
    df = df.drop(columns=['taxon_order'])
    return df


def _get_user_query():
    query = GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    )

    species_count_subquery = query.join(
        GlobalRecordingAnnotation.species_annotations,
        isouter=True
    ).with_entities(
        GlobalRecordingAnnotation.id.label('annotation_id'),
        func.count(GlobalRecordingSpeciesAnnotation.id).label('species_count')
    ).group_by(GlobalRecordingAnnotation.id).subquery()

    species_box_count_subquery = query.join(
        GlobalRecordingAnnotation.species_annotations,
        isouter=True
    ).join(
        GlobalRecordingSpeciesAnnotation.boxes,
        isouter=True
    ).with_entities(
        GlobalRecordingAnnotation.id.label('annotation_id'),
        func.count(GlobalRecordingSpeciesAnnotationBox.id).label('species_box_count')
    ).group_by(GlobalRecordingAnnotation.id).subquery()

    annotations_with_non_bird_area_subquery = query.filter(
        GlobalRecordingAnnotation.non_bird_area.has()
    ).with_entities(
        GlobalRecordingAnnotation.id.label('annotation_id_with_non_bird_area')
    ).subquery()

    query = query.join(
        species_count_subquery,
        GlobalRecordingAnnotation.id == species_count_subquery.c.annotation_id
    ).join(
        species_box_count_subquery,
        GlobalRecordingAnnotation.id == species_box_count_subquery.c.annotation_id
    ).join(
        annotations_with_non_bird_area_subquery,
        GlobalRecordingAnnotation.id == annotations_with_non_bird_area_subquery.c.annotation_id_with_non_bird_area,
        isouter=True
    ).with_entities(
        GlobalRecordingAnnotation.user_id.label('user_id'),
        func.count(GlobalRecordingAnnotation.id).label('annotation_count'),
        func.sum(species_count_subquery.c.species_count).label('species_count'),
        (
            func.sum(species_box_count_subquery.c.species_box_count) +
            func.count(annotations_with_non_bird_area_subquery.c.annotation_id_with_non_bird_area)
        ).label('drawn_boxes_count')
    ).group_by(
        GlobalRecordingAnnotation.user_id
    )

    return query


def _get_distinct_species_count():
    return GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    ).join(
        GlobalRecordingAnnotation.species_annotations,
    ).with_entities(
        GlobalRecordingSpeciesAnnotation.species_id
    ).distinct().count()


def _get_distinct_species_count_by_user_query():
    return GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    ).join(
        GlobalRecordingAnnotation.species_annotations,
        isouter=True
    ).with_entities(
        GlobalRecordingAnnotation.user_id.label('user_id'),
        func.count(func.distinct(GlobalRecordingSpeciesAnnotation.species_id)).label('distinct_species_count')
    ).group_by(GlobalRecordingAnnotation.user_id)


def _get_species_query(user_id=None, lang='en'):
    common_name_field = get_common_name_field(lang).label('common_name')

    query = GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.status == AnnotationStatusEnum.READY
    )

    if user_id is not None:
        query = query.filter(
            GlobalRecordingAnnotation.user_id == user_id
        )

    query = query.join(
        GlobalRecordingAnnotation.species_annotations
    )

    species_box_count_subquery = query.join(
        GlobalRecordingSpeciesAnnotation.boxes,
        isouter=True
    ).with_entities(
        GlobalRecordingSpeciesAnnotation.id.label('species_annotation_id'),
        func.count(GlobalRecordingSpeciesAnnotationBox.id).label('species_box_count')
    ).group_by(GlobalRecordingSpeciesAnnotation.id).subquery()

    query = query.join(
        GlobalRecordingSpeciesAnnotation.species
    ).join(
        species_box_count_subquery,
        GlobalRecordingSpeciesAnnotation.id == species_box_count_subquery.c.species_annotation_id
    ).with_entities(
        GlobalSpecies.id,
        GlobalSpecies.scientific_name,
        common_name_field,
        GlobalSpecies.taxon_order,
        func.count(GlobalSpecies.id).label('count'),
        func.sum(species_box_count_subquery.c.species_box_count).label('drawn_boxes_count')
    ).group_by(
        GlobalSpecies.id,
        GlobalSpecies.scientific_name,
        common_name_field,
        GlobalSpecies.taxon_order
    )

    return query
