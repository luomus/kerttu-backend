from app.utils.view_utils import requires_user, lock
from app.kerttu_global.models.enums import TaxonTypeEnum
from app.kerttu_global.models.models import GlobalSpecies, GlobalContinent, GlobalOrder, GlobalFamily, GlobalSpeciesLock
from app.kerttu_global.services.database.query_service import get_species_list_query, \
    get_species_with_validation_count_query
from app.kerttu_global.services.database.object_dict_convert_service import species_to_dict, continent_to_dict,\
    family_to_dict, order_to_dict
from flask import Blueprint, request, jsonify
from datetime import datetime
from app import app, db

global_species_page = Blueprint('global_species_page', __name__)


@global_species_page.route('', methods=['GET'])
@requires_user
def get_species_list(user_id):
    page = request.args.get('page', 1, type=int)
    page_size = request.args.get('pageSize', 100, type=int)
    order_by = request.args.get('orderBy', '', type=str)
    lang = request.args.get('lang', 'en', type=str)

    continent = request.args.get('continent', None, type=int)
    order = request.args.get('order', None, type=int)
    family = request.args.get('family', None, type=int)
    taxon_type = request.args.get('taxonType', TaxonTypeEnum.BIRD, type=int)
    include_species_without_audio = True if request.args.get('includeSpeciesWithoutAudio') == 'true' else False
    only_unvalidated = True if request.args.get('onlyUnvalidated') == 'true' else False
    search_query = request.args.get('searchQuery', None, type=str)

    query, has_modifications = get_species_list_query(
        user_id,
        taxon_type,
        continent,
        order,
        family,
        include_species_without_audio,
        only_unvalidated,
        search_query,
        order_by,
        lang
    )
    data = query.paginate(page=page, per_page=page_size, max_per_page=app.config['GLOBAL_MAX_NBR_OF_RESULTS'])

    return jsonify({
        'results': [_species_data_to_dict(item) for item in data.items],
        'currentPage': data.page,
        'lastPage': data.pages,
        'nextPage': data.next_num,
        'pageSize': data.per_page,
        'prevPage': data.prev_num,
        'total': data.total,
        'hasModifications': has_modifications
    })


@global_species_page.route('/<int:species_id>', methods=['GET'])
def get_species(species_id):
    lang = request.args.get('lang', 'en', type=str)
    include_validation_count = request.args.get('includeValidationCount', False, type=bool)

    if not include_validation_count:
        species = GlobalSpecies.query.get(species_id)
        return jsonify(species_to_dict(species, lang))
    else:
        result = get_species_with_validation_count_query(species_id, lang).first()
        return jsonify({
            'id': result[0],
            'scientificName': result[1],
            'commonName': result[2],
            'validationCount': result[3] if result[3] is not None else 0,
            'hasAudio': result[4] if result[4] is not None else False
        })


@global_species_page.route('/lock/<int:species_id>', methods=['POST'])
@requires_user
def lock_species(user_id, species_id):
    with lock('lock_species_{}'.format(species_id)):
        species_lock = GlobalSpeciesLock.query.filter(
            GlobalSpeciesLock.species_id == species_id
        ).first()

        date_now = datetime.now()

        if (
            species_lock is None or species_lock.user_id == user_id or
            (date_now - species_lock.created).days >= app.config['GLOBAL_SPECIES_LOCK_EXPIRES_IN_DAYS']
        ):
            if species_lock is None:
                species_lock = GlobalSpeciesLock(
                    species_id=species_id
                )
                db.session.add(species_lock)

            species_lock.user_id = user_id
            species_lock.created = date_now
            db.session.commit()

            return jsonify({
                'success': True
            })
        else:
            return jsonify({
                'success': False
            })


@global_species_page.route('/unlock/<int:species_id>', methods=['POST'])
@requires_user
def unlock_species(user_id, species_id):
    with lock('lock_species_{}'.format(species_id)):
        species_lock = GlobalSpeciesLock.query.filter(
            GlobalSpeciesLock.species_id == species_id
        ).first()

        if species_lock is not None and species_lock.user_id == user_id:
            db.session.delete(species_lock)
            db.session.commit()

            return jsonify({
                'success': True
            })
        else:
            return jsonify({
                'success': False
            })


@global_species_page.route('/filters', methods=['GET'])
def get_filters():
    continents = GlobalContinent.query.order_by(GlobalContinent.id).all()
    orders = GlobalOrder.query.order_by(GlobalOrder.order_order).all()
    families = GlobalFamily.query.order_by(GlobalFamily.family_order).all()

    return jsonify({
        'continent': [continent_to_dict(continent) for continent in continents],
        'order': [order_to_dict(order) for order in orders],
        'family': [family_to_dict(family) for family in families]
    })


def _species_data_to_dict(result):
    return {
        'id': result[0],
        'scientificName': result[1],
        'commonName': result[2],
        'versionCount': result[3] if result[3] is not None else 0,
        'validationCount': result[4] if result[4] is not None else 0,
        'userHasValidated': True if result[5] is not None else False,
        'hasModifications': True if result[6] is not None else False,
        'hasNotPossibleValidations': True if result[7] is not None else False,
        'isLocked': True if result[8] is not None else False,
        'hasAudio': True if len(result) < 10 or result[9] is not None else False
    }
