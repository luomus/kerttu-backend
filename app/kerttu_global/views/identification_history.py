from app import app
from app.kerttu_global.models.enums import AnnotationStatusEnum
from app.kerttu_global.models.models import GlobalRecordingAnnotation, GlobalRecording, GlobalSite,\
    GlobalRecordingSpeciesAnnotation, GlobalSpecies
from flask import Blueprint, jsonify, request
from app.utils.view_utils import requires_user
from app.kerttu_global.services.database.species_name_service import get_common_name_field
from sqlalchemy import nullsfirst, nullslast, or_
import re

global_identification_history_page = Blueprint('global_identification_history_page', __name__)


@global_identification_history_page.route('', methods=['GET'])
@requires_user
def get_history(user_id):
    page = request.args.get('page', 1, type=int)
    page_size = min(request.args.get('pageSize', 100, type=int), app.config['GLOBAL_MAX_NBR_OF_RESULTS'])
    species_search_query = request.args.get('speciesSearchQuery', None, type=str)
    include_skipped = True if request.args.get('includeSkipped') == 'true' else False
    site = request.args.get('site', None, type=int)
    order_by_string = request.args.get('orderBy', 'annotation.created DESC', type=str)
    lang = request.args.get('lang', 'en', type=str)

    return _get_history(user_id, page, page_size, species_search_query, include_skipped, site, order_by_string, lang)


def _get_history(user_id, page, page_size, species_search_query, include_skipped, site, order_by_string, lang):
    query = _get_base_history_query(user_id, species_search_query, include_skipped, site, lang)

    order_by = _parse_order_by(order_by_string)

    query = query.join(
        GlobalRecordingAnnotation.recording
    ).join(
        GlobalRecording.site
    ).order_by(
        *order_by
    )

    species_list_by_id = _get_species_list_by_id(query, page, page_size, lang)

    data = query.with_entities(
        GlobalRecordingAnnotation.id,
        GlobalRecordingAnnotation.created,
        GlobalRecordingAnnotation.edited,
        GlobalRecordingAnnotation.status,
        GlobalRecording.id,
        GlobalSite.id,
        GlobalSite.name,
        GlobalSite.country
    ).paginate(
        page=page,
        per_page=page_size,
        max_per_page=app.config['GLOBAL_MAX_NBR_OF_RESULTS']
    )

    return jsonify({
        'results': [_history_data_to_json_dict(item, species_list_by_id) for item in data.items],
        'currentPage': data.page,
        'lastPage': data.pages,
        'nextPage': data.next_num,
        'pageSize': data.per_page,
        'prevPage': data.prev_num,
        'total': data.total
    })


def _get_base_history_query(user_id, species_search_query, include_skipped, site, lang):
    query = GlobalRecordingAnnotation.query.filter(
        GlobalRecordingAnnotation.user_id == user_id
    )

    if not include_skipped:
        query = query.filter(
            GlobalRecordingAnnotation.status != AnnotationStatusEnum.SKIPPED
        )

    if site is not None:
        query = query.filter(
            GlobalRecordingAnnotation.recording.has(
                GlobalRecording.site_id == site
            )
        )

    return _filter_by_species_search_query(query, species_search_query, lang)


def _history_data_to_json_dict(result, species_list_by_id):
    annotation_id = result[0]
    species_list = species_list_by_id[annotation_id] if annotation_id in species_list_by_id else []

    return {
        'recording': {
            'id': result[4],
            'site': {
                'id': result[5],
                'name': result[6],
                'country': result[7],
            }
        },
        'annotation': {
            'created': result[1],
            'edited': result[2],
            'species': species_list,
            'status': result[3]
        }
    }


def _parse_order_by(value):
    order_by = []

    for order_string in value.split(','):
        match = re.match(
            r'^(annotation\.created|annotation\.edited|recording\.site\.name|annotation\.status) ' 
            '(ASC|DESC)$',
            order_string
        )
        if match is not None:
            prop = match.group(1)
            direction = match.group(2)

            orders = []
            if prop == 'annotation.created':
                orders.append(GlobalRecordingAnnotation.created)
            elif prop == 'annotation.edited':
                orders.append(GlobalRecordingAnnotation.edited)
            elif prop == 'recording.site.name':
                orders.append(GlobalSite.name)
            elif prop == 'annotation.status':
                orders.append(GlobalRecordingAnnotation.status)

            if direction == 'DESC':
                orders = [nullslast(order.desc()) for order in orders]
            else:
                orders = [nullsfirst(order.asc()) for order in orders]

            order_by += orders

    return order_by


def _get_species_list_by_id(query, page, page_size, lang):
    result_by_id = {}

    subquery = query.offset(max((page - 1) * page_size, 0)).limit(page_size).subquery()

    results = GlobalRecordingSpeciesAnnotation.query.join(
        subquery,
        (GlobalRecordingSpeciesAnnotation.recording_annotation_id == subquery.c.id),
        isouter=False
    ).join(
        GlobalRecordingSpeciesAnnotation.species
    ).with_entities(
        subquery.c.id,
        GlobalSpecies.id,
        get_common_name_field(lang),
        GlobalSpecies.scientific_name
    ).all()

    for result in results:
        annotation_id = result[0]

        if annotation_id not in result_by_id:
            result_by_id[annotation_id] = []

        result_by_id[annotation_id].append({
            'id': result[1],
            'commonName': result[2],
            'scientificName': result[3]
        })

    return result_by_id


def _filter_by_species_search_query(query, search_query, lang):
    if search_query is not None and len(search_query) > 0:
        query = query.filter(
            GlobalRecordingAnnotation.species_annotations.any(
                GlobalRecordingSpeciesAnnotation.species.has(
                    or_(
                        get_common_name_field(lang).ilike('%{}%'.format(search_query)),
                        GlobalSpecies.scientific_name.ilike('%{}%'.format(search_query))
                    )
                )
            )
        )

    return query
