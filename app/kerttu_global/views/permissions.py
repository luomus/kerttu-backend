from app import db, auth
from flask import Blueprint, render_template, redirect, url_for, request
from app.kerttu_global.models.models import GlobalSite, GlobalSitePermission
from app.services.lajiAPI import get_user_name

global_permissions_page = Blueprint('global_permissions_page', __name__, template_folder='../templates')


@global_permissions_page.route('', methods=['GET'])
@auth.login_required
def permission_form():
    users_by_site = {}
    site_name_map = {}
    user_name_map = {}

    sites = GlobalSite.query.filter(GlobalSite.limit_access == True).all()
    for site in sites:
        users_by_site[site.id] = []
        site_name_map[site.id] = site.name

    permissions = GlobalSitePermission.query.all()
    for p in permissions:
        if p.site_id not in users_by_site:
            users_by_site[p.site_id] = []
            site_name_map[p.site_id] = p.site.name
        users_by_site[p.site_id].append(p.user_id)

    user_ids = list(set([p.user_id for p in permissions]))
    for user_id in user_ids:
        user_name_map[user_id] = get_user_name(user_id)

    return render_template(
        'permissions.html',
        users_by_site=users_by_site,
        site_name_map=site_name_map,
        user_name_map=user_name_map
    )


@global_permissions_page.route('/add', methods=['POST'])
@auth.login_required
def add():
    user_id = request.form['userId']
    site_id = request.form['siteId']

    if get_user_name(user_id) is not None:
        permission = GlobalSitePermission(user_id=user_id, site_id=site_id)
        db.session.add(permission)
        db.session.commit()

    return redirect(url_for('global_permissions_page.permission_form'))


@global_permissions_page.route('/remove', methods=['POST'])
@auth.login_required
def remove():
    user_id = request.form['userId']
    site_id = request.form['siteId']

    GlobalSitePermission.query.filter(
        GlobalSitePermission.user_id == user_id,
        GlobalSitePermission.site_id == site_id
    ).delete()
    db.session.commit()

    return redirect(url_for('global_permissions_page.permission_form'))
