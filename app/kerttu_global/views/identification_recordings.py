from app.kerttu_global.models.enums import AnnotationStatusEnum
from app.kerttu_global.models.exceptions import InvalidRecordingAnnotationError
from app.utils.view_utils import *
from app.kerttu_global.services.database import identification_recording_service
from app.kerttu_global.services.database.object_dict_convert_service import identification_recording_to_dict,\
    identification_recording_annotation_from_dict, identification_recording_annotation_to_dict
from flask import Blueprint, jsonify, request, abort
from contextlib import suppress

global_identification_recordings_page = Blueprint('global_identification_recordings_page', __name__)


@global_identification_recordings_page.route('/new', methods=['GET'])
@requires_user
def get_recording(user_id):
    lang = request.args.get('lang', 'en', type=str)
    sites = _string_to_int_list(request.args.get('sites', ''))
    previous_recording_id = request.args.get('previousRecording', None, type=int)
    id_blacklist = _string_to_int_list(request.args.get('excludeRecordings', ''))
    file_name_filter = request.args.get('fileNameFilter', '')

    return _get_recording(user_id, sites, lang, previous_recording_id, id_blacklist, file_name_filter)


@global_identification_recordings_page.route('/<int:recording_id>', methods=['GET'])
@requires_user
def get_old_recording(user_id, recording_id):
    lang = request.args.get('lang', 'en', type=str)

    rec = identification_recording_service.get_recording(recording_id)
    if rec is None:
        return abort(400)

    annotation = identification_recording_service.get_recording_annotation(user_id, rec.id)

    return jsonify({
        'recording': identification_recording_to_dict(rec, lang),
        'annotation': identification_recording_annotation_to_dict(annotation)
    })


@global_identification_recordings_page.route('/<int:recording_id>/annotation', methods=['POST'])
@requires_user
def recording_annotation(user_id, recording_id):
    data = request.json

    is_draft = request.args.get('isDraft', 'false') == 'true'
    skip_recording = request.args.get('skipRecording', 'false') == 'true'

    if skip_recording is True:
        identification_recording_service.set_recording_annotation_status(
            user_id, recording_id, AnnotationStatusEnum.SKIPPED
        )
        return jsonify({'success': True})

    if data is None:
        return abort(400)

    annotation = identification_recording_annotation_from_dict(user_id, recording_id, data)

    if not is_draft:
        if not identification_recording_service.check_recording_annotation_is_valid(annotation):
            return error_400(InvalidRecordingAnnotationError())

        annotation.status = AnnotationStatusEnum.READY
        identification_recording_service.save_recording_annotation(annotation)
    else:
        identification_recording_service.save_recording_annotation(annotation)

    return jsonify({'success': True})


def _get_recording(user_id, selected_sites, lang, previous_recording_id=None, id_blacklist=[], file_name_filter=None):
    rec = identification_recording_service.get_recording_for_user(
        user_id, selected_sites, 3, previous_recording_id, id_blacklist, file_name_filter
    )
    annotation = identification_recording_service.get_recording_annotation(
        user_id, rec.id
    ) if rec is not None else None

    return {
        'recording': identification_recording_to_dict(rec, lang),
        'annotation': identification_recording_annotation_to_dict(annotation)
    }


def _string_to_int_list(string_value):
    values = string_value.split(',')

    result = []
    for s in values:
        with suppress(ValueError):
            value = int(s)
            result.append(value)

    return result
