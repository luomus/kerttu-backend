from app.kerttu_global.models.models import *
from app.kerttu_global.services.database.object_dict_convert_service import validation_recording_to_dict
from flask import Blueprint, jsonify, request
from sqlalchemy import or_

global_recording_page = Blueprint('global_recording_page', __name__)


@global_recording_page.route('/<int:species_id>', methods=['GET'])
def get_recording_list(species_id):
    lang = request.args.get('lang', 'en', type=str)

    audio_metadata = GlobalAudioMetadata.query.filter(
        GlobalAudioMetadata.species.has(or_(
            GlobalSpecies.id == species_id,
            GlobalSpecies.parent_species_id == species_id
        ))
    )

    results = []
    for data in audio_metadata:
        for audio in data.audio:
            results.append(validation_recording_to_dict(audio, data, lang))

    return jsonify({
        'results': results
    })
