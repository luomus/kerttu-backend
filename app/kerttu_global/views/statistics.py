from app import cache
from app.utils.database_utils import query_to_dataframe
from app.kerttu_global.services.database.query_service import get_species_validation_counts_query, get_user_counts_query
from flask import Blueprint, jsonify, request

global_statistics_page = Blueprint('global_statistics_page', __name__)


@global_statistics_page.route('/validations', methods=['GET'])
def validation_stats():
    continent = request.args.get('continent', None, type=int)
    order = request.args.get('order', None, type=int)
    family = request.args.get('family', None, type=int)

    df = _get_validation_stats(continent, order, family)

    return jsonify({
        'results': df.rename(columns={
            'validation_count': 'validationCount'
        }).to_dict(orient='records')
    })


@global_statistics_page.route('/users', methods=['GET'])
def user_stats():
    continent = request.args.get('continent', None, type=int)
    order = request.args.get('order', None, type=int)
    family = request.args.get('family', None, type=int)

    data = _get_user_stats(continent, order, family)

    return jsonify({
        'results': data.rename(columns={
            'user_id': 'userId',
            'species_created_count': 'speciesCreated',
            'species_validated_count': 'speciesValidated'
        }).to_dict(orient='records')
    })


@cache.memoize(timeout=600)
def _get_user_stats(continent, order, family):
    query = get_user_counts_query(continent, order, family)

    return query_to_dataframe(query)


@cache.memoize(timeout=600)
def _get_validation_stats(continent, order, family):
    query = get_species_validation_counts_query(continent, order, family)

    df = query_to_dataframe(query)
    df['validation_count'] = df['validation_count'].fillna(0).astype(int)

    return df
