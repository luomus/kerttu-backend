from app import app
from app.kerttu_global.models.models import *
from app.kerttu_global.models.exceptions import *
from app.kerttu_global.services.database.object_dict_convert_service import format_date
from flask import Blueprint, jsonify
from app.utils.view_utils import requires_user, error_400
from app.utils.database_utils import query_to_dataframe
from datetime import datetime
from flask import request, abort
import pandas as pd
import numpy as np

global_template_page = Blueprint('global_template_page', __name__)


@global_template_page.route('/<int:species_id>', methods=['GET'])
def templates_for_species(species_id):
    query = GlobalVersion.query.filter(
        GlobalVersion.species_id == species_id
    ).join(
        GlobalVersion.validations, isouter=True
    ).join(
        GlobalVersion.templates, isouter=True
    ).join(
        GlobalTemplate.comment, isouter=True
    ).order_by(
        GlobalVersion.created,
        GlobalTemplate.template_number
    ).with_entities(
        GlobalVersion.id.label('version_id'),
        GlobalVersion.user_id.label('version_user_id'),
        GlobalVersion.created.label('version_created'),
        GlobalValidation.user_id.label('validation_user_id'),
        GlobalTemplate.id,
        GlobalTemplate.audio_id,
        GlobalTemplate.x1,
        GlobalTemplate.x2,
        GlobalTemplate.y1,
        GlobalTemplate.y2,
        GlobalTemplate.template_number,
        GlobalComment.type.label('comment_type'),
        GlobalComment.comment.label('comment_comment'),
        GlobalComment.user_id.label('comment_user_id'),
        GlobalComment.created.label('comment_created')
    )

    df = query_to_dataframe(query)

    return jsonify({
        'results': _template_df_to_dict_list(df)
    })


@global_template_page.route('/<int:species_id>', methods=['POST'])
@requires_user
def save_templates_for_species(user_id, species_id):
    if not _user_has_species_lock(user_id, species_id):
        return error_400(SpeciesLockedError())

    data = request.json
    if data is None:
        return abort(400)

    templates = data.get('templates', [])
    comments = data.get('comments', [])
    if type(templates) is not list or type(comments) is not list:
        return abort(400)

    try:
        _add_templates(user_id, species_id, templates, comments)
    except (InvalidTemplateDataError, InvalidCommentDataError) as e:
        return error_400(e)

    return jsonify({
        'success': True
    })


def _user_has_species_lock(user_id, species_id):
    species_lock = GlobalSpeciesLock.query.filter(
        GlobalSpeciesLock.species_id == species_id
    ).first()

    return species_lock is None or species_lock.user_id == user_id


def _add_templates(user_id, species_id, templates, comments):
    if len(templates) > app.config['GLOBAL_REQUIRED_TEMPLATE_COUNT']:
        raise InvalidTemplateDataError()
    while len(templates) < app.config['GLOBAL_REQUIRED_TEMPLATE_COUNT']:
        templates.append(None)

    created = datetime.now()
    last_version = _get_latest_version(species_id)

    replaced_template_ids = _get_replaced_template_ids(last_version, templates)
    has_changes = last_version is None or len(replaced_template_ids) > 0
    removed_template_ids = [template_id for template_id in replaced_template_ids if template_id is not None]

    _add_template_data(user_id, species_id, templates, last_version, has_changes, created)
    _add_comment_data(user_id, comments, removed_template_ids, created)

    db.session.commit()


def _get_latest_version(species_id):
    return GlobalVersion.query.filter(
        GlobalVersion.species_id == species_id
    ).order_by(GlobalVersion.created.desc()).first()


def _get_replaced_template_ids(last_version, new_templates):
    replaced_template_ids = []

    old_ids = [None] * app.config['GLOBAL_REQUIRED_TEMPLATE_COUNT']
    old_templates = last_version.templates if last_version is not None else []
    for template in old_templates:
        old_ids[template.template_number - 1] = template.id

    for idx, template in enumerate(new_templates):
        if template is None:
            if old_ids[idx] is None:
                continue
        elif 'id' in template:
            template_id = template['id']
            if template_id == old_ids[idx]:
                continue
            else:
                raise InvalidTemplateDataError()

        replaced_template_ids.append(old_ids[idx])

    return replaced_template_ids


def _add_template_data(user_id, species_id, templates, last_version, has_changes, created):
    missing_templates = None in templates

    if has_changes:
        _add_new_version(user_id, species_id, templates, missing_templates, created)
    elif last_version is not None and not missing_templates:
        _add_validation(user_id, last_version, created)


def _add_new_version(user_id, species_id, templates, missing_templates, created):
    version = GlobalVersion(
        user_id=user_id,
        species_id=species_id,
        created=created,
        missing_templates=missing_templates
    )

    for idx, data in enumerate(templates):
        if data is None:
            continue
        if 'id' in data:
            template = GlobalTemplate.query.get(data['id'])
            if template.template_number != idx + 1:
                raise InvalidTemplateDataError()
        else:
            template = GlobalTemplate(
                audio_id=data['audioId'],
                x1=data['area']['xRange'][0],
                x2=data['area']['xRange'][1],
                y1=data['area']['yRange'][0],
                y2=data['area']['yRange'][1],
                template_number=idx + 1
            )
            db.session.add(template)

        version.templates.append(template)

    db.session.add(version)


def _add_validation(user_id, last_version, created):
    if last_version.user_id != user_id and GlobalValidation.query.filter(
        GlobalValidation.user_id == user_id,
        GlobalValidation.version_id == last_version.id
    ).first() is None:
        validation = GlobalValidation(
            user_id=user_id,
            version_id=last_version.id,
            created=created
        )
        db.session.add(validation)


def _add_comment_data(user_id, comments, removed_template_ids, created):
    comment_template_ids = [comment['templateId'] for comment in comments]
    if sorted(removed_template_ids) != sorted(comment_template_ids):
        raise InvalidCommentDataError()

    for comment in comments:
        comment_obj = GlobalComment(
            user_id=user_id,
            template_id=comment['templateId'],
            type=comment['type'],
            comment=comment['comment'],
            created=created
        )
        db.session.add(comment_obj)


def _template_df_to_dict_list(df):
    results = []

    for version_id in df['version_id'].unique():
        version_df = df[df['version_id'] == version_id]

        templates = [None] * app.config['GLOBAL_REQUIRED_TEMPLATE_COUNT']

        for template_id in version_df['id'].unique():
            if template_id is None or np.isnan(template_id):
                continue

            template_data = version_df[version_df['id'] == template_id].iloc[0]
            template_idx = int(template_data['template_number']) - 1

            templates[template_idx] = {
                'id': int(template_data['id']),
                'audioId': int(template_data['audio_id']),
                'area': {
                    'xRange': [template_data['x1'], template_data['x2']],
                    'yRange': [template_data['y1'], template_data['y2']]
                },
                'validatedBy': _get_template_validated_by(df, template_id),
                'comment': None if template_data['comment_comment'] is None else {
                    'created': _format_date(template_data['comment_created']),
                    'userId': template_data['comment_user_id'],
                    'comment': template_data['comment_comment'],
                    'type': int(template_data['comment_type'])
                }
            }

        results.append({
            'created': _format_date(version_df['version_created'].iloc[0]),
            'userId': version_df['version_user_id'].iloc[0],
            'templates': templates
        })

    return results


def _get_template_validated_by(df, template_id):
    df = df[df['id'] == template_id]
    result = list(pd.concat([df['version_user_id'], df['validation_user_id']]).unique())
    result = [r for r in result if r is not None]
    return result


def _format_date(date):
    return format_date(date.to_pydatetime())
