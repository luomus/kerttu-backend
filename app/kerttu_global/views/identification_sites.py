from app import cache
from app.kerttu_global.models.models import GlobalSite, GlobalSitePermission
from app.kerttu_global.services.database.object_dict_convert_service import identification_site_to_dict
from flask import Blueprint, jsonify
from app.utils.view_utils import get_user_id
from sqlalchemy.sql import or_

global_identification_sites_page = Blueprint('global_identification_sites_page', __name__)


@global_identification_sites_page.route('', methods=['GET'])
def get_site_list():
    user_id = get_user_id()
    return _get_site_list(user_id)


@cache.memoize(timeout=3600)
def _get_site_list(user_id):
    data = GlobalSite.query.filter(
        or_(
            GlobalSite.limit_access != True,
            GlobalSite.permissions.any(
                GlobalSitePermission.user_id == user_id
            )
        ),
        GlobalSite.recordings.any()
    ).order_by(
        GlobalSite.id
    ).all()

    return jsonify({
        'results': [identification_site_to_dict(item) for item in data]
    })
