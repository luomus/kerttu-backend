import json
from flask import Blueprint, request
from werkzeug.exceptions import HTTPException
import os
import pydub.exceptions
import requests.exceptions
from datetime import datetime

from app import app, api_auth
from app.kerttu_global.services.file_upload_service import upload_file
from app.kerttu_global.services.database.identification_recording_service import get_site_for_device, add_recording_data

global_api_page = Blueprint('global_api_page', __name__)

@global_api_page.route('fileUpload', methods=['POST'])
@api_auth.login_required
def post_identification_recordings():
    system_id = api_auth.current_user()

    data, error_msg = _get_file_upload_request_data()
    if data is None:
        return _error_response(400, error_msg)

    try:
        recording_data = upload_file(data['file'], data['file_name'])
    except pydub.exceptions.CouldntDecodeError:
        return _error_response(400, 'Couldn\'t decode the file')
    except requests.exceptions.HTTPError:
        return _error_response(502, 'File upload failed')

    add_recording_data(recording_data, data['site_id'], system_id, data['file_name'], data['date'])

    return { 'success': True }


@global_api_page.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    response = e.get_response()
    response.data = json.dumps({
        'code': e.code,
        'message': e.name,
        'detail': e.description,
    })
    response.content_type = 'application/json'
    return response


def _get_file_upload_request_data():
    if 'file' not in request.files:
        return None, 'A file is missing'

    file = request.files['file']
    file_name = file.filename

    file_ext = os.path.splitext(file_name)[-1]
    if file_ext not in app.config['UPLOAD_ALLOWED_EXTENSIONS']:
        return None, 'Unsupported file format'

    device_id = request.form.get('deviceID')
    if device_id is None:
        return None, 'A device ID is missing'

    site = get_site_for_device(device_id)
    if site is None:
        return None, 'Invalid device ID'

    date = None
    date_string = request.form.get('date')
    if date_string is not None:
        try:
            date = datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S.%f')
        except ValueError:
            return None, 'Couldn\'t parse the date'

    return {
        'file': file,
        'file_name': file_name,
        'site_id': site.id,
        'date': date
    }, None


def _error_response(code, message):
    return {
        'code': code,
        'message': message
    }, code
