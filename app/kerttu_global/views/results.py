from app import app, auth
from flask import Blueprint, request, render_template
from app.kerttu_global.models.models import GlobalContinent, GlobalOrder, GlobalFamily
from app.kerttu_global.services.database.query_service import get_template_results_query
from app.utils.database_utils import query_to_dataframe
from app.utils.view_utils import stream_response
import tempfile
import os

global_results_page = Blueprint('global_results_page', __name__, template_folder='../templates')


@global_results_page.route('', methods=['GET'])
@auth.login_required
def results():
    continents = GlobalContinent.query.order_by(GlobalContinent.id).all()
    orders = GlobalOrder.query.order_by(GlobalOrder.order_order).all()
    families = GlobalFamily.query.order_by(GlobalFamily.family_order).all()

    return render_template('results.html', continents=continents, orders=orders, families=families)


@global_results_page.route('/download', methods=['POST'])
@auth.login_required
def download():
    continent = request.form.get('continent', None, type=int)
    order = request.form.get('order', None, type=int)
    family = request.form.get('family', None, type=int)
    min_validations = request.form.get('minValidations', 0, type=int)

    df = _get_results_df(continent, order, family, min_validations)

    os.makedirs(app.config['TMP_FOLDER'], exist_ok=True)
    tmp_folder = tempfile.mkdtemp(dir=app.config['TMP_FOLDER'])
    file_path = os.path.join(tmp_folder, 'bsg_results.tsv')
    df.to_csv(file_path, sep='\t', index=False)

    return stream_response(file_path, tmp_folder, 'bsg_results.tsv', 'text/tab-separated-value')


def _get_results_df(continent, order, family, min_validations):
    df = query_to_dataframe(get_template_results_query(continent, order, family, min_validations))
    df['file_name'] = df['url'].str.split('/').str[-1]
    return df
