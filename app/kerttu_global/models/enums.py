from app.models.enums import IntEnum


class TaxonCategoryEnum(IntEnum):
    SPECIES = 0
    ISSF = 1
    FORM = 2
    DOMESTIC = 3
    INTERGRADE = 4


class CommentType(IntEnum):
    REPLACE = 0
    REFRAME = 1


class AnnotationStatusEnum(IntEnum):
    SKIPPED = -1
    NOT_READY = 0
    READY = 1


class TaxonOccurrenceEnum(IntEnum):
    OCCURS = 1
    POSSIBLY_OCCURS = 2


class TaxonTypeEnum(IntEnum):
    BIRD = 0
    BAT = 1
    INSECT = 2
    FROG = 3


class SoundTypeEnum(IntEnum):
    SONG_OR_DISPLAY = 1
    OTHER = 2
