from app import db
from app.kerttu_global.models.enums import *
from app.models.enums import DbIntEnum
from sqlalchemy.schema import Sequence
from sqlalchemy.ext.hybrid import hybrid_property
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash

species_continent = db.Table(
    'global_species_continent', db.Model.metadata,
    db.Column('species_id', db.Integer, db.ForeignKey('global_species.id'), primary_key=True),
    db.Column('continent_id', db.Integer, db.ForeignKey('global_continent.id'), primary_key=True)
)

version_template = db.Table(
    'global_version_template', db.Model.metadata,
    db.Column('version_id', db.Integer, db.ForeignKey('global_version.id'), primary_key=True),
    db.Column('template_id', db.Integer, db.ForeignKey('global_template.id'), primary_key=True)
)


class GlobalSpecies(db.Model):
    id_seq = Sequence('global_species_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    taxon_order = db.Column(db.Integer)
    category = db.Column(DbIntEnum(TaxonCategoryEnum), nullable=False, default=TaxonCategoryEnum.SPECIES)
    species_code = db.Column(db.String(10))
    scientific_name = db.Column(db.String(70), nullable=False)
    common_name = db.Column(db.String(70))
    common_name_es = db.Column(db.String(80))
    common_name_fr = db.Column(db.String(80))
    parent_species_id = db.Column(db.Integer, db.ForeignKey('global_species.id'))
    order_id = db.Column(db.Integer, db.ForeignKey('global_order.id'))
    family_id = db.Column(db.Integer, db.ForeignKey('global_family.id'))
    taxon_type = db.Column(DbIntEnum(TaxonTypeEnum), nullable=False, default=TaxonTypeEnum.BIRD)

    continents = db.relationship('GlobalContinent', secondary=species_continent)
    parent = db.relationship('GlobalSpecies', remote_side=[id], backref='children', uselist=False)
    order = db.relationship('GlobalOrder', lazy=True)
    family = db.relationship('GlobalFamily', lazy=True)


class GlobalOrder(db.Model):
    id_seq = Sequence('global_order_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    order_order = db.Column(db.Integer, nullable=False)
    scientific_name = db.Column(db.String(70), nullable=False)


class GlobalFamily(db.Model):
    id_seq = Sequence('global_family_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    family_order = db.Column(db.Integer, nullable=False)
    scientific_name = db.Column(db.String(70), nullable=False)
    order_id = db.Column(db.Integer, db.ForeignKey('global_order.id'), nullable=False)

    order = db.relationship('GlobalOrder', lazy=True)


class GlobalContinent(db.Model):
    id_seq = Sequence('global_continent_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    name = db.Column(db.String(50), nullable=False)

    species = db.relationship('GlobalSpecies', secondary=species_continent, viewonly=True)


class GlobalAudio(db.Model):
    id_seq = Sequence('global_audio_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    url = db.Column(db.String(100), nullable=False)
    spectrogram_url = db.Column(db.String(100))
    metadata_id = db.Column(db.Integer, db.ForeignKey('global_audio_metadata.id'), nullable=False)

    candidates = db.relationship(
        'GlobalCandidate', backref='audio', lazy=True, order_by='GlobalCandidate.candidate_number'
    )


class GlobalAudioMetadata(db.Model):
    id_seq = Sequence('global_audio_metadata_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    species_id = db.Column(db.Integer, db.ForeignKey('global_species.id'), nullable=False)
    recordist = db.Column(db.String(100))
    country = db.Column(db.String(70))
    state = db.Column(db.String(70))
    location = db.Column(db.String(200))
    year = db.Column(db.Integer)
    month = db.Column(db.Integer)
    day = db.Column(db.Integer)
    asset_id = db.Column(db.String(10), nullable=False)
    specimen_url = db.Column(db.String(70), nullable=False)
    ebird_checklist_id = db.Column(db.String(10))
    ebird_checklist_url = db.Column(db.String(70))
    license_url = db.Column(db.String(70))
    sound_type = db.Column(db.String(100))

    species = db.relationship('GlobalSpecies', backref='audio_metadata', lazy=True)
    audio = db.relationship('GlobalAudio', backref='audio_metadata', lazy=True, order_by='GlobalAudio.url')


class GlobalCandidate(db.Model):
    id_seq = Sequence('global_candidate_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    audio_id = db.Column(db.Integer, db.ForeignKey('global_audio.id'), nullable=False)
    x1 = db.Column(db.Float, nullable=False)
    x2 = db.Column(db.Float, nullable=False)
    y1 = db.Column(db.Float, nullable=False)
    y2 = db.Column(db.Float, nullable=False)
    candidate_number = db.Column(db.Integer, nullable=False)


class GlobalTemplate(db.Model):
    id_seq = Sequence('global_template_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    audio_id = db.Column(db.Integer, db.ForeignKey('global_audio.id'), nullable=False)
    x1 = db.Column(db.Float, nullable=False)
    x2 = db.Column(db.Float, nullable=False)
    y1 = db.Column(db.Float, nullable=False)
    y2 = db.Column(db.Float, nullable=False)
    template_number = db.Column(db.Integer, nullable=False)

    audio = db.relationship('GlobalAudio', lazy=True)
    versions = db.relationship('GlobalVersion', secondary=version_template, viewonly=True)


class GlobalVersion(db.Model):
    id_seq = Sequence('global_version_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), nullable=False)
    species_id = db.Column(db.Integer, db.ForeignKey('global_species.id'), nullable=False)
    missing_templates = db.Column(db.Boolean, nullable=False, default=False)
    created = db.Column(db.DateTime(), nullable=False)

    species = db.relationship('GlobalSpecies', backref='versions', lazy=True)
    templates = db.relationship('GlobalTemplate', secondary=version_template, order_by='GlobalTemplate.template_number')
    validations = db.relationship('GlobalValidation', backref='version', lazy=True)


class GlobalValidation(db.Model):
    id_seq = Sequence('global_validation_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), nullable=False)
    version_id = db.Column(db.Integer, db.ForeignKey('global_version.id'), nullable=False)
    created = db.Column(db.DateTime(), nullable=False)


class GlobalComment(db.Model):
    id_seq = Sequence('global_comment_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), nullable=False)
    template_id = db.Column(db.Integer, db.ForeignKey('global_template.id'), nullable=False)
    type = db.Column(DbIntEnum(CommentType), nullable=False)
    comment = db.Column(db.Text(), nullable=False)
    created = db.Column(db.DateTime(), nullable=False)

    template = db.relationship('GlobalTemplate', backref='comment', lazy=True)


class GlobalSpeciesLock(db.Model):
    id_seq = Sequence('global_species_lock_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), nullable=False)
    species_id = db.Column(db.Integer, db.ForeignKey('global_species.id'), nullable=False, unique=True)
    created = db.Column(db.DateTime(), nullable=False)


class GlobalRecording(db.Model):
    id_seq = Sequence('global_recording_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    url = db.Column(db.String(300), nullable=False)
    site_id = db.Column(db.Integer, db.ForeignKey('global_site.id'), nullable=False)
    date_time = db.Column(db.DateTime)
    x1 = db.Column(db.Float, nullable=False)
    x2 = db.Column(db.Float, nullable=False)
    duration = db.Column(db.Float)

    # for targeted recordings
    target_species_id = db.Column(db.Integer, db.ForeignKey('global_species.id'))
    locality = db.Column(db.String(50))

    group_id = db.Column(db.Integer, db.ForeignKey('global_recording_group.id'))
    group_order_number = db.Column(db.Integer)

    original_recording_name = db.Column(db.String(150)) # deprecated
    original_recording_part = db.Column(db.Integer) # deprecated

    uploaded_by = db.Column(db.String(50))
    uploaded_at = db.Column(db.DateTime, default=datetime.now)

    taxon_type = db.Column(DbIntEnum(TaxonTypeEnum), nullable=False, default=TaxonTypeEnum.BIRD)

    target_species = db.relationship('GlobalSpecies', lazy=True)
    group = db.relationship('GlobalRecordingGroup', lazy=True)
    annotations = db.relationship('GlobalRecordingAnnotation', lazy=True, backref='recording')


class GlobalRecordingGroup(db.Model):
    id_seq = Sequence('global_recording_group_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    name = db.Column(db.String(150))


class GlobalRecordingAnnotation(db.Model):
    id_seq = Sequence('global_recording_annotation_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), nullable=False)
    recording_id = db.Column(db.Integer, db.ForeignKey('global_recording.id'), nullable=False)
    status = db.Column(DbIntEnum(AnnotationStatusEnum), nullable=False,
                       default=AnnotationStatusEnum.NOT_READY)

    is_low_quality = db.Column(db.Boolean, default=False)
    contains_human_speech = db.Column(db.Boolean, default=False)
    contains_unknown_birds = db.Column(db.Boolean, default=False)
    does_not_contain_birds = db.Column(db.Boolean, default=False)
    contains_birds_not_on_list = db.Column(db.Boolean, default=False)
    has_boxes_for_all_bird_sounds = db.Column(db.Boolean, default=False)
    birds_not_on_list = db.Column(db.String(100))
    non_bird_area_id = db.Column(db.Integer, db.ForeignKey('global_area.id'))
    created = db.Column(db.DateTime(), nullable=False)
    edited = db.Column(db.DateTime(), nullable=False)

    non_bird_area = db.relationship('GlobalArea', lazy=True, cascade='all, delete-orphan', single_parent=True)
    species_annotations = db.relationship('GlobalRecordingSpeciesAnnotation', lazy=True, cascade='all, delete-orphan')

    @hybrid_property
    def species_list(self):
        return [species_annotation.species for species_annotation in self.species_annotations]


class GlobalRecordingSpeciesAnnotation(db.Model):
    id_seq = Sequence('global_recording_species_annotation_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    recording_annotation_id = db.Column(db.Integer, db.ForeignKey('global_recording_annotation.id'), nullable=False)

    species_id = db.Column(db.Integer, db.ForeignKey('global_species.id'), nullable=False)
    occurrence = db.Column(DbIntEnum(TaxonOccurrenceEnum), nullable=False)

    species = db.relationship('GlobalSpecies', lazy=True)
    boxes = db.relationship('GlobalRecordingSpeciesAnnotationBox', lazy=True, cascade='all, delete-orphan')


class GlobalRecordingSpeciesAnnotationBox(db.Model):
    id_seq = Sequence('global_recording_species_annotation_box_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    recording_species_annotation_id = db.Column(
        db.Integer, db.ForeignKey('global_recording_species_annotation.id'), nullable=False
    )
    area_id = db.Column(db.Integer, db.ForeignKey('global_area.id'), nullable=False)
    overlaps_with_other_species = db.Column(db.Boolean, default=False)
    sound_type = db.Column(DbIntEnum(SoundTypeEnum))
    group_id = db.Column(db.Integer, db.ForeignKey('global_recording_species_annotation_box_group.id'))

    area = db.relationship('GlobalArea', cascade='all, delete-orphan', single_parent=True)


class GlobalRecordingSpeciesAnnotationBoxGroup(db.Model):
    id_seq = Sequence('global_recording_species_annotation_box_group_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)

    _boxes = db.relationship('GlobalRecordingSpeciesAnnotationBox', backref='group', lazy='dynamic')

    @hybrid_property
    def boxes(self):
        return self._boxes.join(GlobalArea).order_by(GlobalArea.x1)


class GlobalArea(db.Model):
    id_seq = Sequence('global_area_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    x1 = db.Column(db.Float, nullable=False)
    x2 = db.Column(db.Float, nullable=False)
    y1 = db.Column(db.Float, nullable=False)
    y2 = db.Column(db.Float, nullable=False)


class GlobalSite(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    country = db.Column(db.String(50))
    latitude_exact = db.Column(db.Float, nullable=False)
    longitude_exact = db.Column(db.Float, nullable=False)
    latitude_coarse = db.Column(db.Float, nullable=False)
    longitude_coarse = db.Column(db.Float, nullable=False)
    # sites that have limit_access = True are only visible to users with permission
    limit_access = db.Column(db.Boolean, default=False, nullable=False)
    device_id = db.Column(db.String(50))

    recordings = db.relationship('GlobalRecording', lazy=True, backref='site')
    permissions = db.relationship('GlobalSitePermission', lazy=True, backref='site')


class GlobalSitePermission(db.Model):
    id_seq = Sequence('global_site_permission_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), nullable=False)
    site_id = db.Column(db.Integer, db.ForeignKey('global_site.id'), nullable=False)


class APIUser(db.Model):
    id_seq = Sequence('api_user_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    system_id = db.Column(db.String(50), nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)

    # Custom property getter
    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    # Custom property setter
    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

