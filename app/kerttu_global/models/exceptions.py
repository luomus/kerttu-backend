class SpeciesLockedError(Exception):
    pass


class InvalidTemplateDataError(Exception):
    pass


class InvalidCommentDataError(Exception):
    pass


class InvalidRecordingAnnotationError(Exception):
    pass
