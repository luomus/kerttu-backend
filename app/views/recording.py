from app import app, cache
from app.models.enums import TaxonAnnotationEnum, TaxonAnnotationTypeEnum
from app.models.models import RecordingAnnotation, RecordingTaxonAnnotation
from app.models.exceptions import InvalidRecordingIdError, InvalidRecordingAnnotationError, \
    NotEnoughLetterAnnotationsError
from app.utils.general_utils import *
from app.utils.view_utils import *
from app.services.database import user_service, recording_service
from app.services.next import next_recording
from app.services.next.next_letter_template import get_letter_template
from app.services.statistics import letter_stats, recording_stats
from flask import Blueprint, jsonify, request, abort
from functools import wraps
import numpy as np

recording_page = Blueprint('recording_page', __name__)


def requires_valid_recording(f):
    @wraps(f)
    def decorated_function(user_id, recording_id, *args, **kwargs):
        try:
            user_service.check_recording_is_valid(user_id, recording_id)
        except InvalidRecordingIdError as e:
            return error_400(e)

        return f(user_id, recording_id, *args, **kwargs)

    return decorated_function


def requires_valid_recording_annotation(f):
    @wraps(f)
    def decorated_function(user_id, recording_id, *args, **kwargs):
        annotation = recording_service.get_recording_annotation(user_id, recording_id)
        if annotation is not None:
            if (
                    annotation.is_low_quality or
                    annotation.does_not_contain_birds or
                    annotation.contains_unknown_birds or
                    recording_stats.count_of_bird_taxon_annotations(annotation.id) > 0
            ):
                return f(user_id, recording_id, *args, **kwargs)

        return error_400(InvalidRecordingAnnotationError())

    return decorated_function


def requires_enough_letter_annotations(f):
    @wraps(f)
    def decorated_function(user_id, taxon_expertise, *args, **kwargs):
        cache_key = '{}_has_enough_letter_annotations_with_expertise'.format(user_id)
        data = cache.get(cache_key)

        if data is not None and np.array_equal(data, taxon_expertise):
            return f(user_id, taxon_expertise, *args, **kwargs)

        if letter_stats.user_template_annotation_count(user_id) < app.config['REQUIRED_NUMBER_OF_LETTER_ANNOTATIONS']:
            if get_letter_template(user_id, taxon_expertise) is not None:
                return error_400(NotEnoughLetterAnnotationsError())

        cache.set(cache_key, taxon_expertise)
        return f(user_id, taxon_expertise, *args, **kwargs)

    return decorated_function


@recording_page.route('', methods=['GET'])
@requires_user
@requires_taxon_expertise
@requires_enough_letter_annotations
def get_recording(user_id, taxon_expertise):
    return _get_recording(user_id)


@recording_page.route('/next/<int:recording_id>', methods=['GET'])
@requires_user
@requires_valid_recording
@requires_valid_recording_annotation
def get_next_recording(user_id, recording_id):
    user_service.move_to_next_recording(user_id)
    return _get_recording(user_id)


@recording_page.route('/previous/<int:recording_id>', methods=['GET'])
@requires_user
@requires_valid_recording
def get_previous_recording(user_id, recording_id):
    if user_service.user_has_previous_recording(user_id):
        user_service.move_to_previous_recording(user_id)
    return _get_recording(user_id)


@recording_page.route('/annotation/<int:recording_id>', methods=['POST'])
@requires_user
@requires_valid_recording
@requires_taxon_expertise
def recording_annotation(user_id, taxon_expertise, recording_id):
    data = request.json

    if data is None:
        return abort(400)

    annotation = _recording_annotation_from_json_dict(
        user_id, taxon_expertise, recording_id, data
    )

    recording_service.save_recording_annotation(annotation)
    return jsonify({'success': True})


def _get_recording(user_id):
    rec = next_recording.get_recording(user_id)
    annotation = recording_service.get_recording_annotation(
        user_id, rec.id
    ) if rec is not None else None

    return jsonify({
        'recording': _recording_to_json_dict(rec),
        'annotation': _recording_annotation_to_json_dict(annotation),
        'statusInfo': _get_info(user_id)
    })


def _recording_annotation_from_json_dict(user_id, taxon_expertise, recording_id, data):
    annotation_obj = RecordingAnnotation(
        user_id=user_id,
        recording_id=recording_id
    )

    annotation_obj.is_low_quality = data.get('isLowQuality', False)
    annotation_obj.contains_noise_caused_by_human_activity = data.get('containsNoiseCausedByHumanActivity', False)
    annotation_obj.contains_human_speech = data.get('containsHumanSpeech', False)
    annotation_obj.contains_unknown_birds = data.get('containsUnknownBirds', False)
    annotation_obj.does_not_contain_birds = data.get('doesNotContainBirds', False)
    annotation_obj.taxon_annotations = []

    taxon_annotations = data.get('taxonAnnotations', {})
    taxon_annotations['main'] = taxon_annotations['main'] if 'main' in taxon_annotations else []
    taxon_annotations['other'] = taxon_annotations['other'] if 'other' in taxon_annotations else []

    annotated_taxa = [a['taxonId'] for a in taxon_annotations['main']]
    taxa_not_in_expertise = [taxon_id for taxon_id in annotated_taxa if taxon_id not in taxon_expertise]

    for taxon_id in taxon_expertise + taxa_not_in_expertise:
        annotation = TaxonAnnotationEnum.DOES_NOT_OCCUR
        bird = True
        if taxon_id in annotated_taxa:
            taxon_annotation = taxon_annotations['main'][annotated_taxa.index(taxon_id)]
            annotation = taxon_annotation['annotation']
            bird = taxon_annotation['bird']

        annotation_obj.taxon_annotations.append(
            RecordingTaxonAnnotation(
                taxon_id=taxon_id,
                annotation=annotation,
                type=TaxonAnnotationTypeEnum.PRIMARY,
                bird=bird
            )
        )

    for taxon_annotation in taxon_annotations['other']:
        annotation_obj.taxon_annotations.append(
            RecordingTaxonAnnotation(
                taxon_id=taxon_annotation['taxonId'],
                annotation=taxon_annotation['annotation'],
                type=TaxonAnnotationTypeEnum.OTHER,
                bird=taxon_annotation['bird']
            )
        )

    return annotation_obj


def _recording_annotation_to_json_dict(annotation):
    if annotation is not None:
        taxon_annotations = {'main': [], 'other': []}

        for taxon_annotation_obj in annotation.taxon_annotations:
            if taxon_annotation_obj.annotation != TaxonAnnotationEnum.DOES_NOT_OCCUR:
                taxon_annotation = {
                    'taxonId': taxon_annotation_obj.taxon_id,
                    'annotation': taxon_annotation_obj.annotation,
                    'bird': taxon_annotation_obj.bird
                }
                if taxon_annotation_obj.type == TaxonAnnotationTypeEnum.PRIMARY:
                    taxon_annotations['main'].append(taxon_annotation)
                elif taxon_annotation_obj.type == TaxonAnnotationTypeEnum.OTHER:
                    taxon_annotations['other'].append(taxon_annotation)

        return {
            'isLowQuality': annotation.is_low_quality,
            'containsNoiseCausedByHumanActivity': annotation.contains_noise_caused_by_human_activity,
            'containsHumanSpeech': annotation.contains_human_speech,
            'containsUnknownBirds': annotation.contains_unknown_birds,
            'doesNotContainBirds': annotation.does_not_contain_birds,
            'taxonAnnotations': taxon_annotations
        }

    return None


def _recording_to_json_dict(recording):
    if recording is not None:
        return {
            'id': recording.id,
            'audio': audio_to_dict(recording.audio, recording.show_location),
            'xRange': get_time_range_for_x_coordinates([recording.x1, recording.x2])
        }
    return None


def _get_info(user_id):
    return {
        'hasPreviousRecording': user_service.user_has_previous_recording(user_id)
    }
