from app import app, cache
from flask import Blueprint, jsonify

from app.models.enums import TaxonAnnotationEnum
from app.services.statistics import letter_stats, recording_stats
from app.utils.view_utils import get_user_id, requires_user, remove_hidden_users
import pandas as pd
import numpy as np

statistics_page = Blueprint('statistics_page', __name__)


@statistics_page.route('/users', methods=['GET'])
def user_stats():
    user_id = get_user_id()
    data = _get_user_stats_data()
    remove_hidden_users(data, user_id)

    return jsonify(data.rename(columns={
        'user_id': 'userId',
        'count_x': 'letterAnnotationCount',
        'count_y': 'recordingAnnotationCount',
        'total_count': 'totalAnnotationCount'
    }).to_dict(orient='records'))


@statistics_page.route('/user/letter', methods=['GET'])
@requires_user
def user_letter_stats(user_id):
    mean_similarity, df = _get_letter_stats_for_user(user_id)

    return jsonify({
        'meanSimilarity': _replace_nan(mean_similarity),
        'taxonStatistics': _remove_nans(df.rename(columns={
            'taxon_id': 'taxonId',
            'candidate_id_x': 'userAnnotationCount',
            'candidate_id_y': 'commonAnnotationCount',
            'user_precision': 'similarity',
            'agree': 'identifiability'
        }).to_dict(orient='records'))
    })


@statistics_page.route('/user/recording', methods=['GET'])
@requires_user
def user_recording_stats(user_id):
    recording_n, mean_n, consensus = _get_recording_stats_for_user(user_id)

    return jsonify({
        'nbrOfRecordings': _replace_nan(recording_n, 0),
        'meanNbrOfSpecies': _replace_nan(mean_n),
        'consensusWithOthers': _replace_nan(consensus)
    })


def _remove_nans(d_list):
    return [{k: v for k, v in d.items() if not pd.isnull(v)} for d in d_list]


def _replace_nan(value, replace_value=None):
    return value if not pd.isnull(value) else replace_value


@cache.memoize(timeout=3600)
def _get_user_stats_data():
    candidates_per_letter = app.config['TARGET_ANNOTATION_COUNT']

    df1 = letter_stats.template_annotation_count_by_user()
    df1['count'] *= candidates_per_letter

    df2 = recording_stats.recording_annotation_counts_by_user()

    data = pd.merge(df1, df2, on='user_id', how='outer')
    data = data.fillna(0)
    data['total_count'] = data['count_x'] + data['count_y']

    return data


@cache.memoize(timeout=3600)
def _get_letter_stats_for_user(user_id):
    df = _get_letter_stats()

    # agree mean for each taxon
    agree = df.groupby(['taxon_id']).agg({'agree': 'mean'})

    user_df = letter_stats.annotation_results_for_user_by_candidate(user_id)

    # user annotation count for each taxon
    count = user_df.groupby(['taxon_id']).agg({'candidate_id': 'count'})

    common_df = pd.merge(user_df, df, on='candidate_id', how='inner')
    common_df['user_correct'] = (np.round(common_df['majority_vote']) == common_df['annotation']).astype(int)

    # calculate user precision
    user_mean_precision = np.mean(common_df['user_correct'])

    common_df = common_df.groupby(['taxon_id']).agg({'candidate_id': 'count', 'user_correct': 'sum'}).reset_index()
    common_df['user_precision'] = common_df['user_correct'] / common_df['candidate_id']
    common_df = common_df.drop(columns=['user_correct'])

    # merge results
    result = pd.merge(count, common_df, on='taxon_id', how='left')
    result = pd.merge(result, agree, on='taxon_id', how='left')

    return user_mean_precision, result


@cache.memoize(timeout=3600)
def _get_letter_stats():
    df = letter_stats.annotation_counts_by_candidate(3)

    # calculate majority vote and the percentage of users that agree with the majority vote
    df['majority_vote'] = df['yes_count'] / (df['yes_count'] + df['no_count'])
    df['agree'] = 0.5 + np.abs(df['majority_vote'] - 0.5)

    return df


@cache.memoize(timeout=3600)
def _get_recording_stats_for_user(user_id):
    data = recording_stats.filtered_annotation_results_for_user_recordings(user_id)
    if data.empty:
        return None, None, None

    data = data.replace({'annotation': {
        TaxonAnnotationEnum.DOES_NOT_OCCUR: 0,
        TaxonAnnotationEnum.OCCURS: 1,
        TaxonAnnotationEnum.POSSIBLY_OCCURS: 0.5
    }})

    # number of recordings
    recording_n = data['recording_id'].nunique()

    data_user = data[data['user_id'] == user_id].copy()
    data_others = data[~(data['user_id'] == user_id)]

    # calculate mean number of annotations for the user
    annotations_per_recording = data_user.groupby(['recording_id']).agg({'annotation': 'sum'})
    mean_n = annotations_per_recording['annotation'].mean()

    # calculate how similar user annotations are compared to others
    data_user['majority_vote'] = data_user.apply(
        lambda row: np.mean(data_others.loc[
            (data_others['recording_id'] == row['recording_id']) & (data_others['taxon_id'] == row['taxon_id']),
            'annotation'].mean()),
        axis=1
    )
    data_user = data_user.dropna(subset=['majority_vote'])
    data_user = data_user[(data_user['annotation'] > 0) | (data_user['majority_vote'] > 0)]

    data_user['majority_vote'] = round(data_user['majority_vote'])
    data_user['diff'] = abs(data_user['annotation'] - data_user['majority_vote'])
    consensus = 1 - data_user['diff'].dropna().mean()

    return recording_n, mean_n, consensus
