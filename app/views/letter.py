from app import app
from app.models.enums import LetterAnnotationEnum
from app.services.database import user_service, letter_service
from app.services.next.next_letter_candidate import get_letter_candidate, get_next_letter_candidate
from app.services.next.next_letter_template import get_letter_template
from app.services.statistics import letter_stats as stats
from app.models.exceptions import InvalidTemplateIdError, InvalidCandidateIdError
from app.utils.general_utils import *
from app.utils.view_utils import *
from flask import Blueprint, request, jsonify, abort
from functools import wraps

letter_page = Blueprint('letter_page', __name__)


def requires_valid_template(f):
    @wraps(f)
    def decorated_function(user_id, template_id, *args, **kwargs):
        try:
            user_service.check_letter_template_is_valid(user_id, template_id)
        except InvalidTemplateIdError as e:
            return error_400(e)

        return f(user_id, template_id, *args, **kwargs)

    return decorated_function


def requires_valid_candidate(f):
    @wraps(f)
    def decorated_function(user_id, template_id, candidate_id, *args, **kwargs):
        try:
            user_service.check_letter_candidate_is_valid(user_id, template_id, candidate_id)
        except InvalidCandidateIdError as e:
            return error_400(e)

        return f(user_id, template_id, candidate_id, *args, **kwargs)

    return decorated_function


################################################################################

@letter_page.route('/template', methods=['GET'])
@requires_user
@requires_taxon_expertise
def get_template(user_id, taxon_expertise):
    template = get_letter_template(user_id, taxon_expertise)
    return _template_response(user_id, template)


@letter_page.route('/skipTemplate', methods=['PUT'])
@requires_user
@requires_taxon_expertise
def skip_template(user_id, taxon_expertise):
    data = request.get_json()
    if data is None or 'templateId' not in data:
        return abort(400)
    template_id = data['templateId']

    try:
        user_service.check_letter_template_is_valid(user_id, template_id)
    except InvalidTemplateIdError as e:
        return error_400(e)

    letter_service.set_letter_template_skipped(user_id, template_id)
    template = get_letter_template(user_id, taxon_expertise)
    return _template_response(user_id, template)


@letter_page.route('/candidate/<int:template_id>', methods=['GET'])
@requires_user
@requires_valid_template
def get_candidate(user_id, template_id):
    candidate = get_letter_candidate(user_id, template_id)
    return _candidate_response(user_id, template_id, candidate)


@letter_page.route('/nextCandidate/<int:template_id>/<int:candidate_id>', methods=['GET'])
@requires_user
@requires_valid_template
@requires_valid_candidate
def get_next_candidate(user_id, template_id, candidate_id):
    candidate = get_next_letter_candidate(user_id, template_id, candidate_id)
    return _candidate_response(user_id, template_id, candidate)


@letter_page.route('/previousCandidate/<int:template_id>/<int:candidate_id>', methods=['GET'])
@requires_user
@requires_valid_template
@requires_valid_candidate
def get_previous_candidate(user_id, template_id, candidate_id):
    user_service.move_to_previous_letter_candidate(user_id)
    candidate = get_letter_candidate(user_id, template_id)
    return _candidate_response(user_id, template_id, candidate)


@letter_page.route('/annotation/<int:template_id>/<int:candidate_id>', methods=['PUT'])
@requires_user
@requires_valid_template
@requires_valid_candidate
def set_annotation(user_id, template_id, candidate_id):
    data = request.get_json()
    if data is None or not ('annotation' in data and LetterAnnotationEnum.has_value(data['annotation'])):
        abort(400)
    annotation = data['annotation']
    user_service.move_to_next_letter_candidate(user_id, annotation)

    status_info = _get_info(user_id, template_id)
    if status_info['userAnnotationCount'] == status_info['targetAnnotationCount']:
        letter_service.set_letter_template_finished(user_id, template_id)

    return jsonify({
        'annotation': annotation,
        'statusInfo': status_info
    })


################################################################################

def _template_response(user_id, template):
    return jsonify({
        'template': _template_to_dict(template),
        'statusInfo': _get_info(user_id, template.id) if template is not None else None
    })


def _template_to_dict(template):
    if template is not None:
        return {
            'id': template.id,
            'taxonId': template.taxon_id,
            'audio': audio_to_dict(template.audio, template.show_location),
            'name': template.original_file_name[:-4],
            'xRange': get_time_range_for_x_coordinates([template.x1, template.x2]),
            'yRange': get_frequency_range_for_y_coordinates([template.y1, template.y2])
        }
    return None


def _candidate_response(user_id, template_id, candidate):
    return jsonify({
        'candidate': _candidate_to_dict(candidate),
        'statusInfo': _get_info(user_id, template_id)
    })


def _candidate_to_dict(candidate):
    if candidate is not None:
        y_range = [
            candidate.template.y1 + candidate.y_diff,
            candidate.template.y2 + candidate.y_diff
        ]

        return {
            'id': candidate.id,
            'audio': audio_to_dict(candidate.audio, candidate.show_location),
            'xRange': get_time_range_for_x_coordinates([candidate.x1, candidate.x2]),
            'yRange': get_frequency_range_for_y_coordinates(y_range),
            'crossCorrelation': candidate.cross_correlation
        }
    return None


def _get_info(user_id, template_id):
    user_annotation_count = stats.user_annotation_count(user_id, template_id)
    target_annotation_count = app.config['TARGET_ANNOTATION_COUNT']

    return {
        'userAnnotationCount': user_annotation_count,
        'targetAnnotationCount': target_annotation_count,
        'hasPreviousCandidate': user_service.user_has_previous_letter_candidate(
            user_id, template_id
        )
    }
