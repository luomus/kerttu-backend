from app import app, auth
from app.services import result_service
from app.utils.view_utils import stream_response
from flask import Blueprint
import tempfile
import os

result_page = Blueprint('result_page', __name__)


@result_page.route('/letter', methods=['GET'])
@auth.login_required
def letter_results():
    os.makedirs(app.config['TMP_FOLDER'], exist_ok=True)
    tmp_folder = tempfile.mkdtemp(dir=app.config['TMP_FOLDER'])
    file_path = result_service.letter_results_to_file(tmp_folder)
    return stream_response(file_path, tmp_folder, 'kerttu_letter_results.tsv', 'text/tab-separated-value')


@result_page.route('/recording', methods=['GET'])
@auth.login_required
def record_results():
    os.makedirs(app.config['TMP_FOLDER'], exist_ok=True)
    tmp_folder = tempfile.mkdtemp(dir=app.config['TMP_FOLDER'])
    file_path = result_service.recording_results_to_file(tmp_folder)
    return stream_response(file_path, tmp_folder, 'kerttu_recording_results.zip', 'application/zip')


@result_page.route('/user', methods=['GET'])
@auth.login_required
def user_results():
    os.makedirs(app.config['TMP_FOLDER'], exist_ok=True)
    tmp_folder = tempfile.mkdtemp(dir=app.config['TMP_FOLDER'])
    file_path = result_service.user_results_to_file(tmp_folder)
    return stream_response(file_path, tmp_folder, 'kerttu_users.tsv', 'text/tab-separated-value')
