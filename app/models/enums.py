from app import db
import enum


class DbIntEnum(db.TypeDecorator):
    impl = db.Integer
    cache_ok = True

    def __init__(self, enum_type, *args, **kwargs):
        super(DbIntEnum, self).__init__(*args, **kwargs)
        self._enum_type = enum_type

    def process_bind_param(self, value, dialect):
        return self._get_value_as_integer(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return None
        return self._enum_type(value)

    def _get_value_as_integer(self, value):
        if value is None:
            return None
        elif isinstance(value, enum.IntEnum):
            return value.value
        elif isinstance(value, int):
            if self._enum_type.has_value(value):
                return value
            raise TypeError('invalid enum value')
        raise TypeError('invalid enum value')


class IntEnum(enum.IntEnum):
    @classmethod
    def has_value(cls, value):
        values = [item.value for item in cls]
        return value in values


class LetterAnnotationEnum(IntEnum):
    YES = 1
    NO = 0
    UNSURE = -1


class LetterTemplateStatusEnum(IntEnum):
    NOT_READY = 0
    READY = 1
    SKIPPED = -1


class RecordingAnnotationStatusEnum(IntEnum):
    NOT_READY = 0
    READY = 1


class TaxonAnnotationEnum(IntEnum):
    OCCURS = 1
    POSSIBLY_OCCURS = 2
    DOES_NOT_OCCUR = 0


class TaxonAnnotationTypeEnum(IntEnum):
    PRIMARY = 0
    OTHER = 1
