class TaxonExpertiseMissingError(Exception):
    pass


class InvalidTemplateIdError(Exception):
    pass


class InvalidCandidateIdError(Exception):
    pass


class InvalidRecordingIdError(Exception):
    pass


class InvalidRecordingAnnotationError(Exception):
    pass


class NotEnoughLetterAnnotationsError(Exception):
    pass
