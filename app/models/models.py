from sqlalchemy.schema import Sequence
from app.models.enums import *
from sqlalchemy.ext.associationproxy import association_proxy


class User(db.Model):
    id = db.Column(db.String(50), primary_key=True)

    current_template_id = db.Column(db.Integer, db.ForeignKey('letter_template.id'))

    current_candidate_id = db.Column(db.Integer, db.ForeignKey('letter_candidate.id'))
    next_candidate_id = db.Column(db.Integer, db.ForeignKey('letter_candidate.id'))
    previous_candidate_id = db.Column(db.Integer, db.ForeignKey('letter_candidate.id'))

    current_recording_id = db.Column(db.Integer, db.ForeignKey('recording.id'))
    next_recording_id = db.Column(db.Integer, db.ForeignKey('recording.id'))
    previous_recording_id = db.Column(db.Integer, db.ForeignKey('recording.id'))


class Audio(db.Model):
    id_seq = Sequence('audio_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    file_name = db.Column(db.String(50), nullable=False)
    date_time = db.Column(db.DateTime)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    municipality = db.Column(db.String(50))


class LetterTemplate(db.Model):
    id_seq = Sequence('letter_template_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    audio_id = db.Column(db.Integer, db.ForeignKey('audio.id'), nullable=False)
    taxon_id = db.Column(db.String(50), nullable=False)
    x1 = db.Column(db.Integer, nullable=False)
    x2 = db.Column(db.Integer, nullable=False)
    y1 = db.Column(db.Integer, nullable=False)
    y2 = db.Column(db.Integer, nullable=False)
    original_file_name = db.Column(db.String(50), nullable=False)
    show_location = db.Column(db.Boolean, default=False, nullable=False)
    removed = db.Column(db.Boolean, default=False, nullable=False)  # only removed from new users

    audio = db.relationship('Audio', lazy=True)
    status = db.relationship('LetterTemplateStatus', backref='template', lazy=True)


class LetterCandidate(db.Model):
    id_seq = Sequence('letter_candidate_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    audio_id = db.Column(db.Integer, db.ForeignKey('audio.id'), nullable=False)
    template_id = db.Column(db.Integer, db.ForeignKey('letter_template.id'), nullable=False)
    cross_correlation = db.Column(db.Float, nullable=False)
    x1 = db.Column(db.Integer, nullable=False)
    x2 = db.Column(db.Integer, nullable=False)
    y_diff = db.Column(db.Integer, nullable=False)

    audio = db.relationship('Audio', lazy=True)
    template = db.relationship('LetterTemplate', lazy=True)
    show_location = association_proxy('template', 'show_location')

    db.Index('ix_lettercand_template_corre', template_id, cross_correlation)


class LetterAnnotation(db.Model):
    id_seq = Sequence('letter_annotation_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), db.ForeignKey('user.id'), nullable=False)
    template_id = db.Column(db.Integer, db.ForeignKey('letter_template.id'), nullable=False)
    candidate_id = db.Column(db.Integer, db.ForeignKey('letter_candidate.id'), nullable=False)
    annotation = db.Column(DbIntEnum(LetterAnnotationEnum), nullable=False)

    template = db.relationship('LetterTemplate', lazy=True)
    candidate = db.relationship('LetterCandidate', lazy=True)


class LetterTemplateStatus(db.Model):
    id_seq = Sequence('letter_template_status_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), db.ForeignKey('user.id'), nullable=False)
    template_id = db.Column(db.Integer, db.ForeignKey('letter_template.id'), nullable=False)
    status = db.Column(DbIntEnum(LetterTemplateStatusEnum), nullable=False, default=LetterTemplateStatusEnum.NOT_READY)


class Recording(db.Model):
    id_seq = Sequence('recording_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    from_candidate_id = db.Column(db.Integer, db.ForeignKey('letter_candidate.id'), nullable=False)
    audio_id = db.Column(db.Integer, db.ForeignKey('audio.id'), nullable=False)
    x1 = db.Column(db.Integer, nullable=False)
    x2 = db.Column(db.Integer, nullable=False)
    show_location = db.Column(db.Boolean, default=False, nullable=False)

    audio = db.relationship('Audio', lazy=True)
    annotations = db.relationship('RecordingAnnotation', lazy=True, backref='recording')


class RecordingAnnotation(db.Model):
    id_seq = Sequence('recording_annotation_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    user_id = db.Column(db.String(50), db.ForeignKey('user.id'), nullable=False)
    recording_id = db.Column(db.Integer, db.ForeignKey('recording.id'), nullable=False)
    status = db.Column(DbIntEnum(RecordingAnnotationStatusEnum), nullable=False,
                       default=RecordingAnnotationStatusEnum.NOT_READY)

    is_low_quality = db.Column(db.Boolean, default=False)
    contains_noise_caused_by_human_activity = db.Column(db.Boolean, default=False)
    contains_human_speech = db.Column(db.Boolean, default=False)
    contains_unknown_birds = db.Column(db.Boolean, default=False)
    does_not_contain_birds = db.Column(db.Boolean, default=False)

    taxon_annotations = db.relationship('RecordingTaxonAnnotation', lazy=True, cascade='all, delete-orphan')


class RecordingTaxonAnnotation(db.Model):
    id_seq = Sequence('recording_taxon_annotation_id_seq')

    id = db.Column(db.Integer, id_seq, server_default=id_seq.next_value(), primary_key=True)
    recording_annotation_id = db.Column(db.Integer, db.ForeignKey('recording_annotation.id'), nullable=False)

    taxon_id = db.Column(db.String(50), nullable=False)
    annotation = db.Column(DbIntEnum(TaxonAnnotationEnum), nullable=False)
    type = db.Column(DbIntEnum(TaxonAnnotationTypeEnum), nullable=False)
    bird = db.Column(db.Boolean, default=True)
