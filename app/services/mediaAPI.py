from app import app
import requests
from requests.auth import HTTPBasicAuth

MEDIA_API_URL = app.config['MEDIA_API_URL']
MEDIA_API_USER = app.config['MEDIA_API_USER']
MEDIA_API_PASSWORD = app.config['MEDIA_API_PASSWORD']

FILE_UPLOAD_PATH = 'api/fileUpload'
AUDIO_PATH = 'api/audio'


def upload_file(file, file_name):
    files = {file_name: file}

    r = requests.post(
        '{}{}'.format(MEDIA_API_URL, FILE_UPLOAD_PATH),
        files=files,
        auth=HTTPBasicAuth(MEDIA_API_USER, MEDIA_API_PASSWORD),
        params={'mediaClass': 'AUDIO'}
    )
    r.raise_for_status()

    temp_id = r.json()[0]['id']
    data = [{
        'tempFileId': temp_id,
        'meta': {
            'license': 'MZ.intellectualRightsARR',
            'rightsOwner': '',
            'secret': True
        }
    }]

    r = requests.post(
        '{}{}'.format(MEDIA_API_URL, AUDIO_PATH),
        json=data,
        auth=HTTPBasicAuth(MEDIA_API_USER, MEDIA_API_PASSWORD)
    )
    r.raise_for_status()

    return r.json()[0]