from app import app
from app.models.enums import RecordingAnnotationStatusEnum
from app.services.database import user_service, letter_service, recording_service
from app.services.statistics import letter_stats
import numpy as np
import random


def get_recording(user_id):
    recording = user_service.get_user_current_recording(user_id)
    if recording is not None:
        return recording

    recording = _get_new_recording(user_id)
    user_service.set_user_current_recording(user_id, recording)

    return recording


def _get_new_recording(user_id):
    if np.random.uniform(0, 1) < app.config['RECORDING_OVERLAP_PARAM']:
        recording = recording_service.get_random_recording_with_annotations_for_user(user_id)
        if recording is not None:
            return recording

    return _get_recording_from_letter_annotations(user_id)


def _get_recording_from_letter_annotations(user_id, tries=0):
    if tries > app.config['NEXT_RECORDING_MAX_TRIES']:
        return None

    taxon_id = _get_random_species_with_annotations()
    if taxon_id is None:
        return None

    letter_template = letter_service.get_random_letter_template_with_annotations(taxon_id)
    letter_annotation = letter_service.get_random_yes_annotation(letter_template.id)

    if letter_annotation is None:
        return _get_recording_from_letter_annotations(user_id, tries + 1)

    recording = _get_recording_from_candidate(letter_annotation.candidate_id)

    annotation = recording_service.get_recording_annotation(user_id, recording.id)
    if annotation is not None and annotation.status == RecordingAnnotationStatusEnum.READY:
        return _get_recording_from_letter_annotations(user_id, tries + 1)

    return recording


def _get_random_species_with_annotations():
    taxon_ids = letter_stats.taxa_with_annotations()
    if len(taxon_ids) == 0:
        return None

    return taxon_ids[random.randrange(0, len(taxon_ids))]


def _get_recording_from_candidate(candidate_id):
    recording = recording_service.get_recording_by_candidate_id(candidate_id)

    if recording is None:
        candidate = letter_service.get_letter_candidate(candidate_id)
        audio_id = candidate.audio_id
        x1, x2 = _get_coordinates(candidate)
        show_location = candidate.show_location
        recording = recording_service.create_recording(candidate_id, audio_id, x1, x2, show_location)

    return recording


# Get x-coordinates that are in the same format than letter x-coordinates for
# the recording. See function app.views.utils.get_time_range_for_x_coordinates
# for converting these coordinates to seconds.
def _get_coordinates(candidate, min_index=1, max_index=5999, length=998):
    middle_point = round((candidate.x1 + candidate.x2) / 2)

    x1 = middle_point - (length / 2)
    x2 = middle_point + (length / 2)

    if x1 < min_index:
        x2 = x2 + (min_index - x1)
        x1 = min_index
    if x2 > max_index:
        x1 = x1 - (x2 - max_index)
        x2 = max_index

    return x1, x2
