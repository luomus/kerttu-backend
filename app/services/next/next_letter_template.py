from app import app
from app.services.database import user_service, letter_service
from app.services.statistics import letter_stats as stats
import random
import pandas as pd


def get_letter_template(user_id, taxon_expertise):
    template = user_service.get_user_current_letter_template(user_id)
    if not _template_exists_and_not_finished(user_id, template):
        template = letter_service.get_unfinished_letter_template(user_id)

        if not _template_exists_and_not_finished(user_id, template):
            taxon_id = _get_target_taxon(user_id, taxon_expertise)
            template = _get_next_letter_template(user_id, taxon_id) if taxon_id is not None else None

    user_service.set_user_current_letter_template(user_id, template)
    if template is not None:
        letter_service.set_letter_template_started(user_id, template.id)
    return template


def _template_exists_and_not_finished(user_id, template):
    if template is None:
        return False

    user_annotation_count = stats.user_annotation_count(user_id, template.id)
    if user_annotation_count < app.config['TARGET_ANNOTATION_COUNT']:
        return True
    else:
        letter_service.set_letter_template_finished(user_id, template.id)
        return False


def _get_target_taxon(user_id, taxon_expertise):
    taxon_ids = stats.taxa_for_user(user_id, taxon_expertise)
    if len(taxon_ids) == 0:
        return None

    counts = _get_template_annotation_counts(user_id, taxon_ids)

    counts['count_sum'] = counts['count'] + counts['by_user_count']
    smallest_sum = counts['count_sum'].min()
    taxon_ids = counts[counts['count_sum'] == smallest_sum]['taxon_id'].to_list()

    return taxon_ids[random.randrange(0, len(taxon_ids))]


def _get_next_letter_template(user_id, taxon_id):
    data = stats.templates_for_user_with_annotations(user_id, taxon_id)

    target_user_count = min(
        app.config['TARGET_USER_COUNT'],
        stats.annotated_templates_exclude_removed_count(taxon_id)
    )

    if not data.empty:
        templates = data[data['count'] < target_user_count]
        if not templates.empty:
            return letter_service.get_letter_template(
                int(templates.sample(n=1)['id'].iloc[0])
            )

    template = letter_service.get_random_unstarted_letter_template(user_id, taxon_id)

    if template is None and not data.empty:
        return letter_service.get_letter_template(
            int(data.sample(n=1)['id'].iloc[0])
        )

    return template


def _get_template_annotation_counts(user_id, taxon_ids):
    counts = stats.template_annotation_count_per_taxon(taxon_ids)

    user_counts = counts[counts['user_id'] == user_id].copy()
    user_counts['by_user_count'] = user_counts['count']
    user_counts = user_counts.drop(columns=['user_id', 'count'])

    counts.loc[counts['count'] > 5, 'count'] = 5  # limit count by single user
    counts = counts.groupby('taxon_id').agg({'count': sum}).reset_index()
    counts = counts.merge(user_counts, how='left')
    counts = counts.merge(pd.DataFrame({'taxon_id': taxon_ids}), how='right')
    counts = counts.fillna(0).astype({'count': int, 'by_user_count': int})

    return counts
