import warnings

import numpy as np
import statsmodels.api as sm

from app import app
from app.models.enums import LetterAnnotationEnum
from app.services.database import user_service, letter_service
from app.services.statistics import letter_stats as stats


def get_letter_candidate(user_id, template_id):
    candidate = user_service.get_user_current_letter_candidate(user_id, template_id)
    if candidate is not None:
        return candidate

    candidate = _get_next_letter_candidate(user_id, template_id)
    user_service.set_user_current_letter_candidate(user_id, candidate)
    return candidate


def get_next_letter_candidate(user_id, template_id, current_candidate_id):
    candidate = user_service.get_user_next_letter_candidate(user_id, template_id)
    if candidate is not None:
        return candidate

    candidate = _get_next_letter_candidate(user_id, template_id, current_candidate_id)
    user_service.set_user_next_letter_candidate(user_id, candidate)
    return candidate


def _get_next_letter_candidate(user_id, template_id, current_candidate_id=None):
    user_data = stats.user_annotation_data(user_id, template_id, True)

    annotated_candidates = user_data['id'].to_list()
    if current_candidate_id is not None:
        annotated_candidates.append(current_candidate_id)

    user_data = user_data[user_data['annotation'] != LetterAnnotationEnum.UNSURE].copy()

    target_correlation = _get_target_correlation(user_data)
    if target_correlation is None:
        return None

    users, annotation_data = stats.annotation_data_for_template(template_id)
    if user_id in users:
        return None

    if len(users) > 0:
        annotation_data = _add_user_data_to_annotation_data(annotation_data, user_data, user_id)
        candidate = _get_next_letter_candidate_when_many_users(
            user_id, users, template_id, annotation_data, target_correlation, annotated_candidates
        )
    else:
        candidate = _get_letter_candidate_with_closest_correlation(
            template_id, target_correlation, annotated_candidates
        )

    return candidate


def _add_user_data_to_annotation_data(annotation_data, user_data, user_id):
    user_data[user_id] = 1
    user_data = user_data.drop(columns=['annotation'])

    result = annotation_data.merge(user_data, on=['id', 'cross_correlation'], how='outer')
    result = result.fillna(0)
    result.loc[result[user_id] == 1, 'total_user_count'] += 1

    return result


def _get_target_correlation(user_data):
    total_count = len(user_data)
    yes_count = len(user_data[user_data['annotation'] == LetterAnnotationEnum.YES])
    no_count = total_count - yes_count

    if total_count >= app.config['TARGET_ANNOTATION_COUNT']:
        return None

    if total_count < 10:
        return 1 - (total_count / 10) + np.random.uniform(-0.025, 0.025)
    elif yes_count < 4:
        return 1
    elif no_count < 4:
        return 0
    else:
        fitted_model = _fit_probit_model(user_data)

        predict_x = np.linspace(0, 1, 101)
        predict_y = fitted_model.predict(sm.add_constant(predict_x))

        prob_diff = np.abs(predict_y - np.random.uniform(0, 1))
        index = np.argwhere(prob_diff == np.amin(prob_diff)).flatten()
        index = index[-1] if index[0] == 0 else index[0]
        return predict_x[index]


def _fit_probit_model(data):
    model = sm.Probit(data['annotation'], sm.add_constant(data['cross_correlation']))

    # send email if there are warnings
    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter('always')

        fitted_model = model.fit(method='bfgs', maxiter=100)

        if len(w):
            app.logger.warning('Probit model warning: {}'.format(
                w[0].message,
            ))

        return fitted_model


def _get_next_letter_candidate_when_many_users(
    user_id, users, template_id, annotation_data, target_correlation, annotated_candidates
):
    proportions = _get_proportions_by_category(user_id, users, annotation_data)
    target_proportions = _get_target_proportions_by_category(users)

    annotation_data = annotation_data[
        ~(annotation_data['id'].isin(annotated_candidates))
    ].copy()
    max_row = _get_row_with_highest_priority(
        users, annotation_data, proportions, target_proportions, target_correlation
    )
    max_outside = _get_candidate_with_highest_priority_outside_annotation_data(
        template_id,
        proportions[len(users)],
        target_proportions[len(users)],
        target_correlation,
        annotation_data['id'].to_list() + annotated_candidates
    )

    if max_outside is not None and (max_row is None or max_outside['priority'] > max_row['priority']):
        return max_outside['candidate']
    elif max_row is not None:
        return letter_service.get_letter_candidate(int(max_row['id']))
    else:
        return None


def _get_proportions_by_category(user_id, users, data):
    counts = np.zeros(len(users) + 3)

    for i, other_user_id in enumerate(users):
        counts[i] = len(data[(data[other_user_id] == 1) & (data[user_id] == 1) & (data['total_user_count'] == 2)])

    counts[len(users)] = len(data[(data[user_id] == 1) & (data['total_user_count'] == 1)])
    counts[len(users) + 1] = len(data[data['total_user_count'] == len(users) + 1])

    proportions = counts / max(np.sum(counts), np.array(1))
    return proportions


def _get_target_proportions_by_category(users):
    expected_user_count = app.config['TARGET_USER_COUNT']
    target_prop = app.config['TARGET_PROPORTION']

    target_proportions = np.zeros(len(users) + 3)

    target1 = (1 - target_prop) * np.exp(-len(users) / expected_user_count)
    target2 = target_prop
    target3 = (1 - target1 - target2) / len(users)

    if len(users) > 1:
        target_proportions[0:len(users)] = target3
        target_proportions[len(users)] = target1
        target_proportions[len(users) + 1] = target2
    else:
        target_proportions[0:len(users)] = 0
        target_proportions[len(users)] = target1
        target_proportions[len(users) + 1] = target2 + target3

    return target_proportions


def _get_row_with_highest_priority(
    users, data, proportions, target_proportions, target_correlation
):
    if data.empty:
        return None

    _set_next_category_to_data(users, data)
    data['priority'] = _get_priority_value(
        proportions[data['next_category']],
        target_proportions[data['next_category']],
        data['cross_correlation'],
        target_correlation
    )
    return data.loc[data['priority'].idxmax()]


def _set_next_category_to_data(users, data):
    data['next_category'] = len(users) + 2

    for i, other_user_id in enumerate(users):
        data.loc[(data[other_user_id] == 1) & (data['total_user_count'] == 1), 'next_category'] = i
    data.loc[data['total_user_count'] == len(users), 'next_category'] = len(users) + 1


def _get_candidate_with_highest_priority_outside_annotation_data(
        template_id, proportion, target_proportion, target_correlation, filter_by
):
    max_candidate = _get_letter_candidate_with_closest_correlation(
        template_id,
        target_correlation,
        filter_by
    )
    if max_candidate is None:
        return None

    max_priority = _get_priority_value(
        proportion,
        target_proportion,
        max_candidate.cross_correlation,
        target_correlation
    )

    return {'candidate': max_candidate, 'priority': max_priority}


def _get_priority_value(
        proportion,
        target_proportion,
        cross_correlation,
        target_correlation
):
    return (
            np.exp(target_proportion - proportion) *
            np.exp(-app.config['WEIGHTING_PARAM'] * np.abs(cross_correlation - target_correlation))
    )


def _get_letter_candidate_with_closest_correlation(
        template_id,
        target_correlation,
        filter_by
):
    candidate = stats.candidate_with_closest_correlation(template_id, target_correlation, filter_by)
    return candidate
