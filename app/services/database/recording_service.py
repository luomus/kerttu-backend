from app import db
from app.models.enums import RecordingAnnotationStatusEnum
from app.models.models import Recording, RecordingAnnotation
from app.utils.database_utils import pick_random_from_query


################################################################################
# Methods for getting recording

def get_recording(recording_id):
    return Recording.query.get(recording_id)


def get_recording_by_candidate_id(candidate_id):
    return Recording.query.filter(
        Recording.from_candidate_id == candidate_id
    ).first()


def get_random_recording_with_annotations_for_user(user_id):
    query = Recording.query.filter(
        Recording.annotations.any(status=RecordingAnnotationStatusEnum.READY),
        ~Recording.annotations.any(
            (RecordingAnnotation.user_id == user_id) &
            (RecordingAnnotation.status != RecordingAnnotationStatusEnum.NOT_READY)
        )
    )

    return pick_random_from_query(query)


def create_recording(candidate_id, audio_id, x1, x2, show_location):
    recording_obj = Recording(
        from_candidate_id=candidate_id,
        audio_id=audio_id,
        x1=x1,
        x2=x2,
        show_location=show_location
    )

    db.session.add(recording_obj)
    db.session.commit()

    return recording_obj


################################################################################
# Methods related to recording annotations

def get_recording_annotation(user_id, recording_id):
    return RecordingAnnotation.query.filter(
        RecordingAnnotation.user_id == user_id,
        RecordingAnnotation.recording_id == recording_id
    ).first()


def save_recording_annotation(recording_annotation):
    previous_annotation = get_recording_annotation(
        recording_annotation.user_id,
        recording_annotation.recording_id
    )
    if previous_annotation is not None:
        taxon_annotations = []
        prev_taxon_annotations = previous_annotation.taxon_annotations

        _update_object(previous_annotation, recording_annotation)

        for taxon_annotation in recording_annotation.taxon_annotations:
            existing_annotations = [a for a in prev_taxon_annotations if a.taxon_id == taxon_annotation.taxon_id]
            if len(existing_annotations) > 0:
                existing_annotation = existing_annotations[0]
                _update_object(existing_annotation, taxon_annotation)
                taxon_annotations.append(existing_annotation)
            else:
                taxon_annotations.append(taxon_annotation)

        previous_annotation.taxon_annotations = taxon_annotations
    else:
        db.session.add(recording_annotation)

    db.session.commit()


def set_recording_annotation_status(user_id, recording_id, status):
    annotation = get_recording_annotation(user_id, recording_id)
    annotation.status = status
    db.session.commit()


################################################################################
# Helper functions

def _update_object(prev_object, new_object):
    for key, value in new_object.__dict__.items():
        if key[0] != '_':
            setattr(prev_object, key, value)
