from app import db
from app.models.enums import RecordingAnnotationStatusEnum
from app.models.models import User, LetterTemplateStatusEnum
from app.models.exceptions import InvalidTemplateIdError, InvalidCandidateIdError, InvalidRecordingIdError
from app.services.database import letter_service, recording_service


################################################################################
# Methods for getting users

def get_all_user_ids():
    users = User.query.all()
    return [user.id for user in users]


################################################################################
# Methods for setting/getting user properties

def set_user_current_letter_template(user_id, template):
    template_id = template.id if template is not None else None
    _set_user_property(user_id, 'current_template_id', template_id)
    db.session.commit()


def get_user_current_letter_template(user_id):
    template_id = _get_user_property(user_id, 'current_template_id')

    if template_id is not None:
        template_status = letter_service.get_letter_template_status(user_id, template_id)
        if template_status is None or template_status.status == LetterTemplateStatusEnum.NOT_READY:
            return letter_service.get_letter_template(template_id)


def set_user_current_letter_candidate(user_id, candidate):
    candidate_id = candidate.id if candidate is not None else None

    _set_user_property(user_id, 'current_candidate_id', candidate_id)
    db.session.commit()


def set_user_next_letter_candidate(user_id, candidate):
    candidate_id = candidate.id if candidate is not None else None

    _set_user_property(user_id, 'next_candidate_id', candidate_id)
    db.session.commit()


def get_user_current_letter_candidate(user_id, template_id):
    return _get_user_letter_candidate(user_id, template_id, 'current')


def get_user_next_letter_candidate(user_id, template_id):
    return _get_user_letter_candidate(user_id, template_id, 'next')


def get_user_previous_letter_candidate(user_id, template_id):
    return _get_user_letter_candidate(user_id, template_id, 'previous')


def set_user_current_recording(user_id, recording):
    recording_id = recording.id if recording is not None else None
    _set_user_property(user_id, 'current_recording_id', recording_id)
    db.session.commit()


def get_user_current_recording(user_id):
    recording_id = _get_user_property(user_id, 'current_recording_id')

    if recording_id is not None:
        annotation = recording_service.get_recording_annotation(user_id, recording_id)
        if annotation is None or annotation.status == RecordingAnnotationStatusEnum.NOT_READY:
            return recording_service.get_recording(recording_id)


################################################################################
# Methods for checking whether user has previous letter candidate / recording

def user_has_previous_letter_candidate(user_id, template_id):
    return get_user_previous_letter_candidate(user_id, template_id) is not None


def user_has_previous_recording(user_id):
    return _get_user_property(user_id, 'previous_recording_id') is not None


################################################################################
# Methods for validity checking

def check_letter_template_is_valid(user_id, template_id):
    template = get_user_current_letter_template(user_id)
    if template is None or template.id != template_id:
        raise InvalidTemplateIdError()


def check_letter_candidate_is_valid(user_id, template_id, candidate_id):
    candidate = get_user_current_letter_candidate(user_id, template_id)
    if candidate is None or candidate.id != candidate_id:
        raise InvalidCandidateIdError()


def check_recording_is_valid(user_id, recording_id):
    recording = get_user_current_recording(user_id)
    if recording is None or recording.id != recording_id:
        raise InvalidRecordingIdError()


################################################################################
# Methods for moving to next or previous letter candidate or recording

def move_to_next_letter_candidate(user_id, annotation):
    current = _get_user_property(user_id, 'current_candidate_id')
    following = _get_user_property(user_id, 'next_candidate_id')

    if current is not None:
        letter_service.set_letter_annotation(user_id, current, annotation)

        _set_user_property(user_id, 'previous_candidate_id', current)
        _set_user_property(user_id, 'current_candidate_id', following)
        _set_user_property(user_id, 'next_candidate_id', None)
        db.session.commit()


def move_to_previous_letter_candidate(user_id):
    previous = _get_user_property(user_id, 'previous_candidate_id')
    current = _get_user_property(user_id, 'current_candidate_id')

    if previous is not None:
        letter_service.delete_letter_annotation(user_id, previous)

        _set_user_property(user_id, 'previous_candidate_id', None)
        _set_user_property(user_id, 'current_candidate_id', previous)
        _set_user_property(user_id, 'next_candidate_id', current)
        db.session.commit()


def move_to_next_recording(user_id):
    current = _get_user_property(user_id, 'current_recording_id')
    following = _get_user_property(user_id, 'next_recording_id')

    if current is not None:
        recording_service.set_recording_annotation_status(user_id, current, RecordingAnnotationStatusEnum.READY)

        _set_user_property(user_id, 'previous_recording_id', current)
        _set_user_property(user_id, 'current_recording_id', following)
        _set_user_property(user_id, 'next_recording_id', None)
        db.session.commit()


def move_to_previous_recording(user_id):
    previous = _get_user_property(user_id, 'previous_recording_id')
    current = _get_user_property(user_id, 'current_recording_id')

    if previous is not None:
        recording_service.set_recording_annotation_status(user_id, previous, RecordingAnnotationStatusEnum.NOT_READY)

        _set_user_property(user_id, 'previous_recording_id', None)
        _set_user_property(user_id, 'current_recording_id', previous)
        _set_user_property(user_id, 'next_recording_id', current)
        db.session.commit()


################################################################################
# Helper functions

def _get_user_property(user_id, prop):
    user = User.query.get(user_id)
    if user is None:
        return None

    return getattr(user, prop)


def _set_user_property(user_id, prop, value):
    user = User.query.get(user_id)
    if user is None:
        user = User(id=user_id)
        db.session.add(user)

    setattr(user, prop, value)


def _get_user_letter_candidate(user_id, template_id, candidate_type):
    prop = '{}_candidate_id'.format(candidate_type)
    candidate_id = _get_user_property(user_id, prop)

    if (
            candidate_id is not None and
            (candidate_type == 'previous' or letter_service.get_letter_annotation(user_id, candidate_id) is None)
    ):
        candidate = letter_service.get_letter_candidate(candidate_id)
        if candidate is not None and candidate.template_id == template_id:
            return candidate
