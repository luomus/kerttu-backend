from app import db, cache
from app.models.enums import LetterAnnotationEnum, LetterTemplateStatusEnum
from app.models.models import LetterTemplate, LetterCandidate, LetterAnnotation, LetterTemplateStatus
from app.services.statistics import letter_stats as letter_stats
from app.utils.database_utils import pick_random_from_query


################################################################################
# Methods for getting letter template or candidate

def get_letter_template(recording_id):
    return LetterTemplate.query.get(recording_id)


def get_letter_candidate(recording_id):
    return LetterCandidate.query.get(recording_id)


################################################################################
# Methods related to letter annotations

def set_letter_annotation(user_id, candidate_id, annotation):
    template_id = get_letter_candidate(candidate_id).template_id

    if annotation != LetterAnnotationEnum.UNSURE:
        cache.delete_memoized(
            letter_stats.user_annotation_count,
            user_id,
            template_id
        )

    annotation_obj = LetterAnnotation.query.filter(
        LetterAnnotation.user_id == user_id,
        LetterAnnotation.candidate_id == candidate_id
    ).first()

    if annotation_obj is None:
        annotation_obj = LetterAnnotation(
            user_id=user_id,
            template_id=template_id,
            candidate_id=candidate_id,
            annotation=annotation
        )
        db.session.add(annotation_obj)
    else:
        annotation_obj.annotation = annotation
    db.session.commit()


def get_letter_annotation(user_id, candidate_id):
    annotation_obj = LetterAnnotation.query.filter(
        LetterAnnotation.user_id == user_id,
        LetterAnnotation.candidate_id == candidate_id
    ).first()

    if annotation_obj is None:
        return None
    return annotation_obj.annotation


def delete_letter_annotation(user_id, candidate_id):
    annotation_obj = LetterAnnotation.query.filter(
        LetterAnnotation.user_id == user_id,
        LetterAnnotation.candidate_id == candidate_id
    ).first()

    if annotation_obj is not None:
        if annotation_obj.annotation != LetterAnnotationEnum.UNSURE:
            cache.delete_memoized(
                letter_stats.user_annotation_count,
                user_id,
                get_letter_candidate(candidate_id).template_id
            )
        db.session.delete(annotation_obj)

    db.session.commit()


################################################################################
# Methods related to letter template status

def get_letter_template_status(user_id, template_id):
    status_obj = LetterTemplateStatus.query.filter(
        LetterTemplateStatus.user_id == user_id,
        LetterTemplateStatus.template_id == template_id
    ).first()

    return status_obj


def get_unfinished_letter_template(user_id):
    status_obj = LetterTemplateStatus.query.filter(
        LetterTemplateStatus.user_id == user_id,
        LetterTemplateStatus.status == LetterTemplateStatusEnum.NOT_READY
    ).first()

    if status_obj is not None:
        return get_letter_template(status_obj.template_id)


def set_letter_template_started(user_id, template_id):
    _set_letter_template_status(user_id, template_id, LetterTemplateStatusEnum.NOT_READY)


def set_letter_template_finished(user_id, template_id):
    cache.delete_memoized(letter_stats.annotation_data_for_template, template_id)
    _set_letter_template_status(user_id, template_id, LetterTemplateStatusEnum.READY)


def set_letter_template_skipped(user_id, template_id):
    _set_letter_template_status(user_id, template_id, LetterTemplateStatusEnum.SKIPPED)


################################################################################
# Methods for getting random templates or annotations

def get_random_unstarted_letter_template(user_id, taxon_id):
    templates = LetterTemplate.query.filter(
        LetterTemplate.taxon_id == taxon_id,
        LetterTemplate.removed != True,
        ~LetterTemplate.status.any(status=LetterTemplateStatusEnum.READY),
        ~LetterTemplate.status.any(
            (LetterTemplateStatus.user_id == user_id) &
            (LetterTemplateStatus.status != LetterTemplateStatusEnum.NOT_READY)
        )
    )

    return pick_random_from_query(templates)


def get_random_letter_template_with_annotations(taxon_id):
    templates = LetterTemplate.query.filter(
        LetterTemplate.taxon_id == taxon_id,
        LetterTemplate.status.any(status=LetterTemplateStatusEnum.READY)
    )

    return pick_random_from_query(templates)


def get_random_yes_annotation(template_id):
    annotations = LetterAnnotation.query.filter(
        LetterAnnotation.template_id == template_id,
        LetterAnnotation.annotation == LetterAnnotationEnum.YES
    )

    return pick_random_from_query(annotations)


################################################################################
# Helper functions


def _set_letter_template_status(user_id, template_id, status):
    status_obj = get_letter_template_status(user_id, template_id)

    if status_obj is None:
        status_obj = LetterTemplateStatus(
            user_id=user_id,
            template_id=template_id,
            status=status
        )
        db.session.add(status_obj)
    else:
        status_obj.status = status
    db.session.commit()
