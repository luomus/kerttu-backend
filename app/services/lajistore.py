from app import app, cache
import requests
from requests.auth import HTTPBasicAuth
from app.services import lajiAPI

LAJISTORE_URL = app.config['LAJISTORE_URL']
LAJISTORE_USER = app.config['LAJISTORE_USER']
LAJISTORE_PASSWORD = app.config['LAJISTORE_PASSWORD']
LAJISTORE_PAGE_SIZE = app.config['LAJISTORE_PAGE_SIZE']

PROFILE_PATH = 'profile'

USER_ID_FIELD = 'userID'
NAME_VISIBLE_FILED = 'nameVisibleInKerttu'
SKILL_LEVEL_FIELD = 'finnishBirdSongRecognitionSkillLevel'
ACTIVITY_LEVEL_FIELD = 'birdwatchingActivityLevel'
TAXON_EXPERTISE_FIELD = 'taxonExpertise'
GLOBAL_SKILL_LEVELs_FIELD = 'birdSongRecognitionSkillLevels'


def get_visible_users(user_ids):
    visible_users = []
    missing_from_cache = []

    for user_id in user_ids:
        visible = cache.get('user_visible_{}'.format(user_id))

        if visible is None:
            missing_from_cache.append(user_id)
        elif visible is True:
            visible_users.append(user_id)

    for user_profile in _get_all_user_profiles(missing_from_cache, [USER_ID_FIELD, NAME_VISIBLE_FILED]):
        user_id = user_profile[USER_ID_FIELD]
        visible = user_profile.get(NAME_VISIBLE_FILED, False)

        if visible is True:
            visible_users.append(user_id)

        cache.set('user_visible_{}'.format(user_id), visible, timeout=3600)

    return visible_users


def get_expertise_data(user_ids):
    data = []
    fields = [USER_ID_FIELD, SKILL_LEVEL_FIELD, ACTIVITY_LEVEL_FIELD, TAXON_EXPERTISE_FIELD]

    for user_profile in _get_all_user_profiles(user_ids, fields):
        user_data = {
            'user_id': user_profile.get(USER_ID_FIELD, None),
            'finnish_bird_song_recognition_skill_level': user_profile.get(SKILL_LEVEL_FIELD, None),
            'birdwatching_activity_level': user_profile.get(ACTIVITY_LEVEL_FIELD, None),
            'taxon_expertise': user_profile.get(TAXON_EXPERTISE_FIELD, [])
        }
        data.append(user_data)

    return data


def get_global_expertise_data(user_ids):
    continent_labels = lajiAPI.get_continent_labels()

    data = []
    fields = [USER_ID_FIELD, GLOBAL_SKILL_LEVELs_FIELD, ACTIVITY_LEVEL_FIELD]

    for user_profile in _get_all_user_profiles(user_ids, fields):
        user_data = {
            'user_id': user_profile.get(USER_ID_FIELD, None),
            'birdwatching_activity_level': user_profile.get(ACTIVITY_LEVEL_FIELD, None),
        }

        skill_levels = user_profile.get(GLOBAL_SKILL_LEVELs_FIELD, [])
        for skill_level in skill_levels:
            area = skill_level['birdSongRecognitionArea']
            level = skill_level['birdSongRecognitionSkillLevel']
            continent_name = '_'.join(continent_labels[area].split(' '))
            user_data['bird_song_recognition_skill_level_{}'.format(continent_name)] = level

        data.append(user_data)

    return data


def _get_all_user_profiles(user_ids, fields):
    result = []

    start_index = 0
    while start_index < len(user_ids):
        user_ids_part = user_ids[start_index:start_index + LAJISTORE_PAGE_SIZE]

        query = ['userID:{}'.format(user_id) for user_id in user_ids_part]
        params = {'q': ' OR '.join(query), 'fields': ','.join(fields), 'page_size': LAJISTORE_PAGE_SIZE}
        result += _get_all_pages('{}{}'.format(LAJISTORE_URL, PROFILE_PATH), params)

        start_index += LAJISTORE_PAGE_SIZE

    return result


def _get_all_pages(url, params, result=None):
    response = _get(url, params)
    members = response['member']

    result = result + members if result else members
    if response['currentPage'] >= response['lastPage']:
        return result
    else:
        params['page'] = response['currentPage'] + 1
        return _get_all_pages(url, params, result)


def _get(url, params):
    raw = requests.get(url, params=params, auth=HTTPBasicAuth(LAJISTORE_USER, LAJISTORE_PASSWORD))
    return raw.json()
