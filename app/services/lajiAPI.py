from app import app, cache
import requests

LAJIAPI_URL = app.config['LAJIAPI_URL']
ACCESS_TOKEN = app.config['LAJIAPI_TOKEN']

PERSON_PATH = 'person'
AREA_PATH = 'areas'


@cache.memoize(timeout=30 * 60)
def get_user_id(person_token):
    if person_token is None:
        return None

    params = {'access_token': ACCESS_TOKEN}
    result = requests.get(
        '{}{}/{}'.format(LAJIAPI_URL, PERSON_PATH, person_token),
        params=params
    ).json()

    if 'error' in result:
        return None
    else:
        return result['id']


def get_user_taxon_expertise(person_token):
    if person_token is None:
        return None

    params = {'access_token': ACCESS_TOKEN}
    result = requests.get(
        '{}{}/{}/profile'.format(LAJIAPI_URL, PERSON_PATH, person_token),
        params=params
    ).json()

    required_fields = [
        'taxonExpertise',
        'finnishBirdSongRecognitionSkillLevel',
        'birdwatchingActivityLevel'
    ]

    for field in required_fields:
        if field not in result or len(result[field]) < 1:
            return None

    return result['taxonExpertise']


@cache.memoize(timeout=5 * 60 * 60)
def get_continent_labels():
    labels = {}

    params = {
        'access_token': ACCESS_TOKEN,
        'type': 'continent',
        'page': 1,
        'pageSize': 200
    }

    result = requests.get(
        '{}{}'.format(LAJIAPI_URL, AREA_PATH),
        params=params
    ).json()

    for continent in result['results']:
        labels[continent['id']] = continent['name']

    return labels


def get_user_name(user_id):
    if user_id is None:
        return None

    params = {'access_token': ACCESS_TOKEN}
    result = requests.get(
        '{}{}/by-id/{}'.format(LAJIAPI_URL, PERSON_PATH, user_id),
        params=params
    ).json()

    if 'error' in result:
        return None
    else:
        return result['fullName']
