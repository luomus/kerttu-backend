import numpy as np

from app.models.enums import TaxonAnnotationEnum, RecordingAnnotationStatusEnum
from app.models.models import Recording, RecordingAnnotation, RecordingTaxonAnnotation, Audio
from app.utils.database_utils import query_to_dataframe
from app.utils.general_utils import start_x_coordinate_to_seconds, end_x_coordinate_to_seconds
from sqlalchemy import func, not_, and_, or_


################################################################################
# Simple counts

def count_of_bird_taxon_annotations(recording_annotation_id):
    return RecordingTaxonAnnotation.query.filter(
        RecordingTaxonAnnotation.recording_annotation_id == recording_annotation_id,
        RecordingTaxonAnnotation.bird == True,
        RecordingTaxonAnnotation.annotation != TaxonAnnotationEnum.DOES_NOT_OCCUR
    ).count()


def user_recording_annotation_count(user_id):
    return RecordingAnnotation.query.filter(
        RecordingAnnotation.user_id == user_id,
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
    ).count()


def recording_annotation_count():
    return RecordingAnnotation.query.filter(
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
    ).count()


################################################################################
# More complex queries, return Pandas dataframe

def recording_annotation_counts_by_user():
    query = RecordingAnnotation.query.filter(
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
    ).with_entities(
        RecordingAnnotation.user_id,
        func.count(RecordingAnnotation.user_id).label('count')
    ).group_by(RecordingAnnotation.user_id)

    return query_to_dataframe(query)


def filtered_annotation_results_for_user_recordings(user_id, user_limit=3):
    query = _get_annotation_results_for_user_recordings_query(
        user_id, user_limit
    )

    return query_to_dataframe(query)


def recording_results():
    query = Recording.query.filter(
        Recording.annotations.any(
            RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
        )
    ).join(
        Recording.audio
    ).with_entities(
        Recording.id.label('recording_id'),
        Recording.audio_id,
        Audio.file_name,
        Recording.x1,
        Recording.x2
    )

    df = query_to_dataframe(query)

    df['x1_seconds'] = np.around(start_x_coordinate_to_seconds(df['x1']), 2)
    df['x2_seconds'] = np.around(end_x_coordinate_to_seconds(df['x2']), 2)

    return df


def annotation_results():
    query = RecordingAnnotation.query.filter(
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
    ).join(Recording).with_entities(
        Recording.id,
        RecordingAnnotation.user_id,
        RecordingAnnotation.is_low_quality,
        RecordingAnnotation.contains_noise_caused_by_human_activity,
        RecordingAnnotation.contains_human_speech,
        RecordingAnnotation.contains_unknown_birds,
        RecordingAnnotation.does_not_contain_birds
    )

    df = query_to_dataframe(query)
    df = df.rename(columns={'id': 'recording_id'})

    return df


def taxon_annotation_results():
    query = RecordingTaxonAnnotation.query.join(RecordingAnnotation).filter(
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY
    ).join(Recording).with_entities(
        Recording.id,
        RecordingAnnotation.user_id,
        RecordingTaxonAnnotation.taxon_id,
        RecordingTaxonAnnotation.annotation,
        RecordingTaxonAnnotation.type
    )

    df = query_to_dataframe(query)
    df = df.rename(columns={'id': 'recording_id'})

    return df


def _get_annotation_results_for_user_recordings_query(user_id, user_limit):
    # select ready annotations from recordings user has annotated
    query = RecordingAnnotation.query.filter(
        RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY,
        RecordingAnnotation.recording.has(
            Recording.annotations.any(
                and_(
                    RecordingAnnotation.status == RecordingAnnotationStatusEnum.READY,
                    RecordingAnnotation.user_id == user_id
                )
            )
        ),
        or_(
            RecordingAnnotation.contains_unknown_birds == False,
            RecordingAnnotation.taxon_annotations.any(
                RecordingTaxonAnnotation.annotation != TaxonAnnotationEnum.DOES_NOT_OCCUR
            )
        )
    )

    # filter out recordings that have less than three users
    sub_query = query.with_entities(
        RecordingAnnotation.recording_id, func.count(RecordingAnnotation.user_id)
    ).group_by(
        RecordingAnnotation.recording_id
    ).having(func.count(RecordingAnnotation.user_id) >= user_limit).subquery()

    query = query.join(
        sub_query,
        (RecordingAnnotation.recording_id == sub_query.c.recording_id)
    )

    query = query.join(RecordingAnnotation.taxon_annotations)

    # filter "does not occur" annotations if there are unknown birds
    query = query.filter(not_(
        and_(
            RecordingAnnotation.contains_unknown_birds == True,
            RecordingTaxonAnnotation.annotation == TaxonAnnotationEnum.DOES_NOT_OCCUR
        )
    ))

    # filter out species that only have "does not occurs" annotations
    sub_query2 = query.filter(
        RecordingTaxonAnnotation.annotation != TaxonAnnotationEnum.DOES_NOT_OCCUR
    ).with_entities(
        RecordingTaxonAnnotation.taxon_id
    ).group_by(
        RecordingTaxonAnnotation.taxon_id
    ).subquery()

    query = query.join(
        sub_query2,
        (RecordingTaxonAnnotation.taxon_id == sub_query2.c.taxon_id)
    )

    # include fields user_id, annotation, recording_id, taxon_id
    query = query.join(
        RecordingAnnotation.recording
    ).with_entities(
        RecordingAnnotation.user_id,
        RecordingTaxonAnnotation.annotation,
        Recording.id.label('recording_id'),
        RecordingTaxonAnnotation.taxon_id
    )

    return query
