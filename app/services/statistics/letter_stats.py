from app import cache
from app.models.enums import LetterAnnotationEnum, LetterTemplateStatusEnum
from app.models.models import LetterTemplate, LetterCandidate, LetterAnnotation, LetterTemplateStatus
from app.utils.database_utils import query_to_dataframe
import pandas as pd
from sqlalchemy.sql import func


################################################################################
# Simple counts

@cache.memoize()
def user_annotation_count(user_id, template_id):
    annotations = _user_annotations_for_template_query(user_id, template_id)
    return annotations.count()


def user_template_annotation_count(user_id):
    return LetterTemplateStatus.query.filter(
        (LetterTemplateStatus.user_id == user_id) &
        (LetterTemplateStatus.status == LetterTemplateStatusEnum.READY)
    ).count()


def template_annotation_count():
    return LetterTemplateStatus.query.filter(
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY
    ).count()


def annotated_templates_exclude_removed_count(taxon_id):
    return LetterTemplate.query.filter(
        LetterTemplate.taxon_id == taxon_id,
        LetterTemplate.removed != True,
        LetterTemplate.status.any(
            LetterTemplateStatus.status == LetterTemplateStatusEnum.READY
        )
    ).count()

################################################################################
# Lists


@cache.memoize()
def taxa_with_annotations():
    taxa = LetterTemplate.query.filter(
        LetterTemplate.status.any(
            LetterTemplateStatus.status == LetterTemplateStatusEnum.READY
        )
    ).with_entities(LetterTemplate.taxon_id).distinct().all()

    return [taxon for (taxon,) in taxa]


def taxa_for_user(user_id, taxon_expertise):
    taxa = LetterTemplate.query.filter(
        LetterTemplate.taxon_id.in_(taxon_expertise),
        LetterTemplate.removed != True,
        ~LetterTemplate.status.any(
            (LetterTemplateStatus.user_id == user_id) &
            (LetterTemplateStatus.status != LetterTemplateStatusEnum.NOT_READY)
        )
    ).with_entities(LetterTemplate.taxon_id).distinct().all()

    return [taxon for (taxon,) in taxa]


################################################################################
# More complex queries, return Pandas dataframe

def template_annotation_count_by_user():
    query = LetterTemplateStatus.query.filter(
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY
    ).with_entities(
        LetterTemplateStatus.user_id,
        func.count(LetterTemplateStatus.user_id).label('count')
    ).group_by(LetterTemplateStatus.user_id)

    return query_to_dataframe(query)


def template_annotation_count_per_taxon(taxon_ids):
    query = LetterTemplateStatus.query.join(LetterTemplateStatus.template).filter(
        LetterTemplate.taxon_id.in_(taxon_ids),
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY
    ).with_entities(
        LetterTemplate.taxon_id,
        LetterTemplateStatus.user_id,
        func.count(LetterTemplateStatus.template_id).label('count')
    ).group_by(
        LetterTemplate.taxon_id,
        LetterTemplateStatus.user_id
    )

    return query_to_dataframe(query)


def templates_for_user_with_annotations(user_id, taxon_id):
    query = LetterTemplate.query.filter(
        LetterTemplate.taxon_id == taxon_id,
        LetterTemplate.removed != True,
    ).join(
        LetterTemplate.status
    ).filter(
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY,
        ~LetterTemplate.status.any(
            (LetterTemplateStatus.user_id == user_id) &
            (LetterTemplateStatus.status != LetterTemplateStatusEnum.NOT_READY)
        )
    ).with_entities(
        LetterTemplate.id,
        func.count(LetterTemplateStatus.user_id).label('count')
    ).group_by(
        LetterTemplate.id
    )

    return query_to_dataframe(query)


def user_annotation_data(user_id, template_id, include_unsure=False):
    annotations = _user_annotations_for_template_query(user_id, template_id, include_unsure)
    return query_to_dataframe(
        annotations.join(LetterAnnotation.candidate).with_entities(
            LetterCandidate.id, LetterCandidate.cross_correlation, LetterAnnotation.annotation
        )
    )


# Returns a tuple (users, df)
# Users contains a list of users who have annotated the template.
# Df is a dataframe with following columns:
#   id: id of a candidate
#   cross_correlation: cross correlation of a candidate
#   total_user_count: total number of users who have gave either yes or no annotation to the candidate
# It has also columns for each user id in users with values 0 (has not annotated the candidate) or 1 (has annotated
# the candidate).
@cache.memoize()
def annotation_data_for_template(template_id):
    query = _ready_annotations_query(None, template_id).join(
        LetterAnnotation.candidate
    ).with_entities(
        LetterCandidate.id,
        LetterCandidate.cross_correlation,
        LetterAnnotation.user_id
    )

    df = query_to_dataframe(query)

    if df.empty:
        return [], None

    users = df['user_id'].unique()

    df = df.groupby('id').agg({'user_id': list, 'cross_correlation': 'first'}).reset_index()

    result = pd.get_dummies(df['user_id'].apply(pd.Series).stack()).groupby(level=0).sum()
    result['id'] = df['id']
    result['cross_correlation'] = df['cross_correlation']
    result['total_user_count'] = df['user_id'].str.len()
    return users, result


def candidate_with_closest_correlation(template_id, cross_correlation, filter_by=None):
    base_query = LetterCandidate.query.filter(
        LetterCandidate.template_id == template_id
    )
    if filter_by is not None and len(filter_by) > 0:
        base_query = base_query.filter(
            LetterCandidate.id.not_in(filter_by)
        )

    candidate1 = base_query.filter(
        LetterCandidate.cross_correlation < cross_correlation
    ).order_by(
        LetterCandidate.cross_correlation.desc()
    ).first()

    candidate2 = base_query.filter(
        LetterCandidate.cross_correlation >= cross_correlation
    ).order_by(
        LetterCandidate.cross_correlation.asc()
    ).first()

    if candidate1 is None:
        return candidate2
    elif candidate2 is None:
        return candidate1
    elif abs(candidate1.cross_correlation - cross_correlation) < abs(candidate2.cross_correlation - cross_correlation):
        return candidate1
    else:
        return candidate2


def annotation_results():
    query = _ready_annotations_query(None, None, True).join(
        LetterAnnotation.template
    ).join(
        LetterAnnotation.candidate
    ).with_entities(
        LetterTemplate.original_file_name,
        LetterTemplate.taxon_id,
        LetterCandidate.audio_id,
        LetterAnnotation.user_id,
        LetterAnnotation.annotation
    )

    df = query_to_dataframe(query)
    if df.empty:
        return pd.DataFrame()

    df = df.groupby(['original_file_name', 'taxon_id', 'audio_id', 'annotation']).agg(
        {'user_id': list}
    ).unstack().reset_index()

    col_names = {
        LetterAnnotationEnum.YES: 'persons_yes',
        LetterAnnotationEnum.NO: 'persons_no',
        LetterAnnotationEnum.UNSURE: 'persons_uncertain'
    }
    df.columns = [
        col_names[col[1]] if col[0] == 'user_id' else col[0] for col in df.columns.values
    ]
    for col in ['persons_yes', 'persons_no', 'persons_uncertain']:
        if col in df:
            df.loc[df[col].isnull(), [col]] = df.loc[df[col].isnull(), [col]].apply(lambda x: [], axis=1)

    return df


def annotation_results_for_user_by_candidate(user_id):
    query = _ready_annotations_query(user_id).join(
        LetterAnnotation.template
    ).with_entities(
        LetterTemplate.taxon_id,
        LetterAnnotation.candidate_id,
        LetterAnnotation.annotation
    )

    return query_to_dataframe(query)


# Returns a dataframe with columns:
#  taxon_id: taxon id of the template
#  candidate_id: candidate id
#  yes_count: count of yes annotations
#  no_count: count of no annotations
def annotation_counts_by_candidate(user_limit):
    ready_annotations = _ready_annotations_query()

    sub_query = ready_annotations.with_entities(
        LetterAnnotation.candidate_id, func.count(LetterAnnotation.user_id)
    ).group_by(
        LetterAnnotation.candidate_id
    ).having(func.count(LetterAnnotation.user_id) >= user_limit).subquery()

    filtered_annotations = ready_annotations.join(
        sub_query,
        (LetterAnnotation.candidate_id == sub_query.c.candidate_id)
    )

    query = filtered_annotations.join(
        LetterAnnotation.template
    ).with_entities(
        LetterTemplate.taxon_id,
        LetterAnnotation.candidate_id,
        LetterAnnotation.annotation,
        func.count(LetterAnnotation.user_id).label('count')
    ).group_by(
        LetterTemplate.taxon_id,
        LetterAnnotation.candidate_id,
        LetterAnnotation.annotation
    )

    df = query_to_dataframe(query)
    df = df.groupby(['taxon_id', 'candidate_id', 'annotation'])['count'].sum().unstack('annotation')
    df = df.rename(columns={
        0: 'no_count',
        1: 'yes_count'
    })
    for col_name in ['no_count', 'yes_count']:
        df[col_name] = df[col_name].fillna(0).astype(int)
    return df


################################################################################
# Helper functions

def _user_annotations_for_template_query(user_id, template_id, include_unsure=False):
    query = LetterAnnotation.query.filter(
        LetterAnnotation.template_id == template_id,
        LetterAnnotation.user_id == user_id
    )
    if not include_unsure:
        query = query.filter(LetterAnnotation.annotation != LetterAnnotationEnum.UNSURE)

    return query


def _ready_annotations_query(user_id=None, template_id=None, include_unsure=False):
    query = LetterTemplateStatus.query.filter(
        LetterTemplateStatus.status == LetterTemplateStatusEnum.READY
    )

    if user_id is not None:
        query = query.filter(
            LetterTemplateStatus.user_id == user_id
        )

    if template_id is not None:
        query = query.filter(
            LetterTemplateStatus.template_id == template_id
        )

    query = query.join(
        LetterAnnotation,
        (LetterTemplateStatus.user_id == LetterAnnotation.user_id) &
        (LetterTemplateStatus.template_id == LetterAnnotation.template_id)
    )

    if not include_unsure:
        query = query.filter(LetterAnnotation.annotation != LetterAnnotationEnum.UNSURE)

    return query