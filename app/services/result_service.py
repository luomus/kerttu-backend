from app.services.statistics import letter_stats, recording_stats
from app.services.database import user_service
from app.services import lajistore
from app.utils.database_utils import dataframe_to_tsv, dataframe_to_tsv_grouped_by_user
import shutil
import os
import pandas as pd


def letter_results_to_file(tmp_folder):
    df = letter_stats.annotation_results()
    file_path = os.path.join(tmp_folder, 'kerttu_letter_results.tsv')
    dataframe_to_tsv(df, file_path, ['persons_yes', 'persons_no', 'persons_uncertain'])
    return file_path


def recording_results_to_file(tmp_folder):
    data_folder = os.path.join(tmp_folder, 'data')
    os.makedirs(data_folder)

    df = recording_stats.recording_results()
    file_path = os.path.join(data_folder, 'recordings.tsv')
    dataframe_to_tsv(df, file_path)

    df = recording_stats.annotation_results()
    path = os.path.join(data_folder, 'annotations')
    dataframe_to_tsv_grouped_by_user(df, path)

    df = recording_stats.taxon_annotation_results()
    path = os.path.join(data_folder, 'taxon_annotations')
    dataframe_to_tsv_grouped_by_user(df, path)

    path = os.path.join(tmp_folder, 'kerttu_recording_results')
    return shutil.make_archive(path, 'zip', data_folder)


def user_results_to_file(tmp_folder):
    user_ids = user_service.get_all_user_ids()
    data = lajistore.get_expertise_data(user_ids)
    df = pd.DataFrame(data)

    file_path = os.path.join(tmp_folder, 'kerttu_users.tsv')
    dataframe_to_tsv(df, file_path, ['taxon_expertise'])
    return file_path
