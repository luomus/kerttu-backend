from app import app, auth, api_auth
from app.kerttu_global.models.models import APIUser
from werkzeug.security import generate_password_hash, check_password_hash


@auth.verify_password
def verify_password(username, password):
    users = {
        app.config['AUTH_USER']: generate_password_hash(app.config['AUTH_PASSWORD'])
    }

    if username in users and check_password_hash(users.get(username), password):
        return username

@api_auth.verify_password
def verify_password(username, password):
    user = APIUser.query.filter(APIUser.system_id == username).first()

    if user is not None and user.verify_password(password):
        return username


@api_auth.error_handler
def auth_error(status):
    return {
        'code': status,
        'message': 'Unauthorized Access'
    }, status
