from app import app

SPECT_FS = 16000  # parameters of a spectrogram in a Matlab script that generated letter templates and candidates
SPECT_NPERSEG = 256
SPECT_NOVERLAP = 256 - 160


# get audio file url from the original file name
def get_audio_file_location(orig_path):
    return '{}/{}/{}.mp3'.format(
        app.config['IMAGE_LAJI_URL'],
        orig_path.split('_')[0],
        orig_path.split('.')[0]
    )


# convert Matlab spectrogram coordinates to seconds
def get_time_range_for_x_coordinates(spect_range):
    return [
        start_x_coordinate_to_seconds(spect_range[0]),
        end_x_coordinate_to_seconds(spect_range[1])
    ]


def start_x_coordinate_to_seconds(coordinate):
    return (coordinate - 1) * ((SPECT_NPERSEG - SPECT_NOVERLAP) / SPECT_FS)


def end_x_coordinate_to_seconds(coordinate):
    return (coordinate - 1) * ((SPECT_NPERSEG - SPECT_NOVERLAP) / SPECT_FS) + SPECT_NPERSEG / SPECT_FS


# convert Matlab spectrogram coordinates to hertz
def get_frequency_range_for_y_coordinates(spect_range):
    return [
        (spect_range[0] - 1) / SPECT_NPERSEG * SPECT_FS,
        (spect_range[1] - 1) / SPECT_NPERSEG * SPECT_FS,
    ]
