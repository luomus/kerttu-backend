from flask import request, abort, jsonify, Response
from app import app
from app.services import lajiAPI, lajistore
from app.models.exceptions import TaxonExpertiseMissingError
from functools import wraps
from multiprocessing import Lock
from app.utils.general_utils import get_audio_file_location
import shutil
import os

locks = {}


def requires_user(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        user_id = get_user_id()
        if user_id is None:
            return abort(400)

        with lock(user_id):
            return f(user_id, *args, **kwargs)

    return decorated_function


def requires_taxon_expertise(f):
    @wraps(f)
    def decorated_function(user_id, *args, **kwargs):
        taxon_expertise = get_user_taxon_expertise()
        if taxon_expertise is None:
            return error_400(TaxonExpertiseMissingError())

        return f(user_id, taxon_expertise, *args, **kwargs)

    return decorated_function


def error_400(e):
    response = jsonify({'message': type(e).__name__})
    response.status_code = 400
    return response


def stream_response(file_path, tmp_folder, file_name, mimetype, chunk_size=8192):
    def generate():
        try:
            with open(file_path, 'rb') as fd:
                while True:
                    buf = fd.read(chunk_size)
                    if buf:
                        yield buf
                    else:
                        break
        finally:
            shutil.rmtree(tmp_folder)

    r = Response(generate(), mimetype=mimetype)
    r.headers.add('Content-Disposition', 'attachment; filename={}'.format(file_name))
    r.headers.add('Content-Length', str(os.path.getsize(file_path)))
    return r


def get_user_id():
    person_token = request.args.get('personToken')
    user_id = lajiAPI.get_user_id(person_token)
    return user_id


def get_user_taxon_expertise():
    person_token = request.args.get('personToken')
    taxon_expertise = lajiAPI.get_user_taxon_expertise(person_token)
    return taxon_expertise


def audio_to_dict(audio, show_location=False):
    result = {
        'url': get_audio_file_location(audio.file_name),
        'duration': app.config['AUDIO_DURATION']
    }

    if audio.date_time:
        result['dateTime'] = audio.date_time.isoformat()
    if audio.municipality and show_location:
        result['municipality'] = audio.municipality
    if audio.longitude and audio.latitude and show_location:
        result['geometry'] = {
            'type': 'Point',
            'coordinates': [audio.longitude, audio.latitude]
        }

    return result


def remove_hidden_users(data, user_id=None):
    visible_users = lajistore.get_visible_users(data['user_id'].unique())

    if user_id is not None and user_id not in visible_users:
        visible_users = visible_users.copy()
        visible_users.append(user_id)

    data.loc[~data['user_id'].isin(visible_users), 'user_id'] = None


def lock(key):
    if key not in locks:
        locks[key] = Lock()

    return locks[key]
