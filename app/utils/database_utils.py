import pandas as pd
import random
import os


def query_to_dataframe(query):
    return pd.read_sql(query.statement, query.session.bind)


def pick_random_from_query(query):
    query_count = query.count()
    if query_count == 0:
        return None

    rand = random.randrange(0, query_count)
    return query[rand]


def dataframe_to_tsv_grouped_by_user(df, path):
    os.makedirs(path)

    users = df['user_id'].unique()
    for user in users:
        data = df[df['user_id'] == user]
        data = data.drop(columns=['user_id'])
        file_path = os.path.join(path, '{}.tsv'.format(user))
        dataframe_to_tsv(data, file_path)


def dataframe_to_tsv(df, file_path, list_fields=[]):
    for field in list_fields:
        if field in df:
            df[field] = [','.join(value) for value in df[field]]

    df.to_csv(file_path, sep='\t', index=False)
