from flask import Flask
from config import Config
from flask_cors import CORS
from flask_httpauth import HTTPBasicAuth
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_caching import Cache
import memcache
memcache.SERVER_MAX_VALUE_LENGTH = 1024 * 1024 * 5

app = Flask(__name__)
app.config.from_object(Config)
CORS(app, resources={r'/*': {'origins': '*', 'methods': ['GET', 'POST', 'PUT']}})
auth = HTTPBasicAuth()
api_auth = HTTPBasicAuth()

db = SQLAlchemy(app)
migrate = Migrate(app, db, compare_type=True)
cache = Cache(app)

from app.views.index import index_page
from app.kerttu_global.views.species import global_species_page
from app.kerttu_global.views.recordings import global_recording_page
from app.kerttu_global.views.templates import global_template_page
from app.kerttu_global.views.statistics import global_statistics_page
from app.kerttu_global.views.results import global_results_page
from app.kerttu_global.views.identification_recordings import global_identification_recordings_page
from app.kerttu_global.views.identification_sites import global_identification_sites_page
from app.kerttu_global.views.identification_statistics import global_identification_statistics_page
from app.kerttu_global.views.identification_results import global_identification_results_page
from app.kerttu_global.views.identification_history import global_identification_history_page
from app.kerttu_global.views.api import global_api_page

app.register_blueprint(index_page)
app.register_blueprint(global_species_page, url_prefix='/global/species')
app.register_blueprint(global_recording_page, url_prefix='/global/recordings')
app.register_blueprint(global_template_page, url_prefix='/global/templates')
app.register_blueprint(global_statistics_page, url_prefix='/global/statistics')
app.register_blueprint(global_results_page, url_prefix='/global/results')
app.register_blueprint(global_identification_recordings_page, url_prefix='/global/identification/recordings')
app.register_blueprint(global_identification_sites_page, url_prefix='/global/identification/sites')
app.register_blueprint(global_identification_statistics_page, url_prefix='/global/identification/statistics')
app.register_blueprint(global_identification_results_page, url_prefix='/global/identification/results')
app.register_blueprint(global_identification_history_page, url_prefix='/global/identification/history')

app.register_blueprint(global_api_page, url_prefix='/api')

if app.config['ENABLE_OLD_ROUTES']:
    from app.views.letter import letter_page
    from app.views.recording import recording_page
    from app.views.result import result_page
    from app.views.statistics import statistics_page

    app.register_blueprint(letter_page, url_prefix='/letter')
    app.register_blueprint(recording_page, url_prefix='/recording')
    app.register_blueprint(result_page, url_prefix='/result')
    app.register_blueprint(statistics_page, url_prefix='/statistics')


if app.config['ENABLE_GLOBAL_PERMISSIONS_ROUTE']:
    from app.kerttu_global.views.permissions import global_permissions_page

    app.register_blueprint(global_permissions_page, url_prefix='/global/permissions')


# No caching
@app.after_request
def add_header(response):
    if 'Cache-Control' not in response.headers:
        response.headers['Cache-Control'] = 'no-store'
    return response


from app import basic_auth
from app.models import models
from scripts import *
