"""empty message

Revision ID: 30fb39e2023c
Revises: 925d300b206c
Create Date: 2021-07-28 14:25:16.398199

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '30fb39e2023c'
down_revision = '925d300b206c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('create sequence global_letter_id_seq start with 1 increment by 1 nocache nocycle')
    op.create_table('global_letter',
    sa.Column('id', sa.Integer(), server_default=sa.text('global_letter_id_seq.nextval'), nullable=False),
    sa.Column('audio_id', sa.Integer(), nullable=False),
    sa.Column('x1', sa.Float(), nullable=False),
    sa.Column('x2', sa.Float(), nullable=False),
    sa.Column('y1', sa.Float(), nullable=False),
    sa.Column('y2', sa.Float(), nullable=False),
    sa.ForeignKeyConstraint(['audio_id'], ['global_audio.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('global_letter')
    op.execute(sa.schema.DropSequence(sa.Sequence('global_letter_id_seq')))
    # ### end Alembic commands ###
