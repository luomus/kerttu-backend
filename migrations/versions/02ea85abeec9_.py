"""empty message

Revision ID: 02ea85abeec9
Revises: dad26d6dbea1
Create Date: 2023-06-15 12:11:41.990026

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '02ea85abeec9'
down_revision = 'dad26d6dbea1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_recording', schema=None) as batch_op:
        batch_op.alter_column('recording_type', new_column_name='taxon_type', existing_type=sa.INTEGER())

    with op.batch_alter_table('global_species', schema=None) as batch_op:
        batch_op.add_column(sa.Column('taxon_type', sa.INTEGER(), nullable=True))
        batch_op.alter_column('taxon_order',
               existing_type=sa.INTEGER(),
               nullable=True)
        batch_op.alter_column('species_code',
               existing_type=sa.VARCHAR(length=10),
               nullable=True)
        batch_op.alter_column('common_name',
               existing_type=sa.VARCHAR(length=70),
               nullable=True)
        batch_op.alter_column('common_name_es',
               existing_type=sa.VARCHAR(length=80),
               nullable=True)
        batch_op.alter_column('order_id',
               existing_type=sa.INTEGER(),
               nullable=True)
        batch_op.alter_column('family_id',
               existing_type=sa.INTEGER(),
               nullable=True)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_species', schema=None) as batch_op:
        batch_op.alter_column('family_id',
               existing_type=sa.INTEGER(),
               nullable=False)
        batch_op.alter_column('order_id',
               existing_type=sa.INTEGER(),
               nullable=False)
        batch_op.alter_column('common_name_es',
               existing_type=sa.VARCHAR(length=80),
               nullable=False)
        batch_op.alter_column('common_name',
               existing_type=sa.VARCHAR(length=70),
               nullable=False)
        batch_op.alter_column('species_code',
               existing_type=sa.VARCHAR(length=10),
               nullable=False)
        batch_op.alter_column('taxon_order',
               existing_type=sa.INTEGER(),
               nullable=False)
        batch_op.drop_column('taxon_type')

    with op.batch_alter_table('global_recording', schema=None) as batch_op:
        batch_op.alter_column('taxon_type', nullable=True, new_column_name='recording_type')

    # ### end Alembic commands ###
