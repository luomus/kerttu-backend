"""empty message

Revision ID: 8c09c0ec7995
Revises: cb8194c13536
Create Date: 2025-01-14 14:16:14.555935

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8c09c0ec7995'
down_revision = 'cb8194c13536'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_recording_species_annotation_box', schema=None) as batch_op:
        batch_op.add_column(sa.Column('sound_type', sa.Integer(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_recording_species_annotation_box', schema=None) as batch_op:
        batch_op.drop_column('sound_type')

    # ### end Alembic commands ###
