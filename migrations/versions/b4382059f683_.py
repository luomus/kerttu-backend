"""empty message

Revision ID: b4382059f683
Revises: 80d6d678d130
Create Date: 2021-08-11 11:14:49.364037

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b4382059f683'
down_revision = '80d6d678d130'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('global_template_validation', sa.Column('template_order', sa.Integer(), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('global_template_validation', 'template_order')
    # ### end Alembic commands ###
