"""empty message

Revision ID: 3a9cd035bd47
Revises: 02ea85abeec9
Create Date: 2023-06-15 12:28:23.062023

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3a9cd035bd47'
down_revision = '02ea85abeec9'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_recording', schema=None) as batch_op:
        batch_op.alter_column('taxon_type',
               existing_type=sa.INTEGER(),
               nullable=False)

    with op.batch_alter_table('global_species', schema=None) as batch_op:
        batch_op.alter_column('taxon_type',
               existing_type=sa.INTEGER(),
               nullable=False)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_species', schema=None) as batch_op:
        batch_op.alter_column('taxon_type',
               existing_type=sa.INTEGER(),
               nullable=True)

    with op.batch_alter_table('global_recording', schema=None) as batch_op:
        batch_op.alter_column('taxon_type',
               existing_type=sa.INTEGER(),
               nullable=True)

    # ### end Alembic commands ###
