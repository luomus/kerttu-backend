"""empty message

Revision ID: 91ff440f93f8
Revises: 7d930e513562
Create Date: 2020-10-14 15:07:05.737449

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '91ff440f93f8'
down_revision = '7d930e513562'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('sys_c00550328', 'letter_annotation', type_='foreignkey')
    op.drop_column('letter_annotation', 'template_id')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('letter_annotation', sa.Column('template_id', sa.INTEGER(), nullable=False))
    op.create_foreign_key('sys_c00550328', 'letter_annotation', 'letter_template', ['template_id'], ['id'])
    # ### end Alembic commands ###
