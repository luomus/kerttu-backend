"""empty message

Revision ID: b80709572040
Revises: 8f3a80be0d68
Create Date: 2022-02-05 16:15:49.287664

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b80709572040'
down_revision = '8f3a80be0d68'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('global_recording_species_annotation', sa.Column('x1', sa.Float(), nullable=True))
    op.add_column('global_recording_species_annotation', sa.Column('x2', sa.Float(), nullable=True))
    op.add_column('global_recording_species_annotation', sa.Column('y1', sa.Float(), nullable=True))
    op.add_column('global_recording_species_annotation', sa.Column('y2', sa.Float(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('global_recording_species_annotation', 'y2')
    op.drop_column('global_recording_species_annotation', 'y1')
    op.drop_column('global_recording_species_annotation', 'x2')
    op.drop_column('global_recording_species_annotation', 'x1')
    # ### end Alembic commands ###
