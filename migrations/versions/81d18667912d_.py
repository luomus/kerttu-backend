"""empty message

Revision ID: 81d18667912d
Revises: 6d90cda11295
Create Date: 2023-02-20 11:17:32.147711

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '81d18667912d'
down_revision = '6d90cda11295'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('global_recording', 'original_recording_name',
               existing_type=sa.VARCHAR(length=70),
               type_=sa.String(length=80),
               existing_nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('global_recording', 'original_recording_name',
               existing_type=sa.String(length=80),
               type_=sa.VARCHAR(length=70),
               existing_nullable=True)
    # ### end Alembic commands ###
