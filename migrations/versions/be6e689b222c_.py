"""empty message

Revision ID: be6e689b222c
Revises: fdace246c0f6
Create Date: 2020-10-21 10:16:55.451257

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'be6e689b222c'
down_revision = 'fdace246c0f6'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('recording', sa.Column('from_candidate_id', sa.Integer(), nullable=False))
    op.create_foreign_key(None, 'recording', 'letter_candidate', ['from_candidate_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'recording', type_='foreignkey')
    op.drop_column('recording', 'from_candidate_id')
    # ### end Alembic commands ###
