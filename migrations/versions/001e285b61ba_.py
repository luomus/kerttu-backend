"""empty message

Revision ID: 001e285b61ba
Revises: 51473376f00c
Create Date: 2020-03-12 17:43:17.038025

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '001e285b61ba'
down_revision = '51473376f00c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('create sequence letter_annotation_id_seq start with 1 increment by 1 nocache nocycle')
    op.create_table('letter_annotation',
    sa.Column('id', sa.Integer(), server_default=sa.text('letter_annotation_id_seq.nextval'), nullable=False),
    sa.Column('user_id', sa.String(length=50), nullable=False),
    sa.Column('taxon_id', sa.String(length=50), nullable=False),
    sa.Column('template_id', sa.String(length=50), nullable=False),
    sa.Column('candidate_id', sa.String(length=50), nullable=False),
    sa.Column('annotation', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute(sa.schema.DropSequence(sa.Sequence('letter_annotation_id_seq')))
    op.drop_table('letter_annotation')
    # ### end Alembic commands ###
