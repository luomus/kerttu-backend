"""empty message

Revision ID: 925d300b206c
Revises: d54bd33a82a0
Create Date: 2021-06-03 16:26:07.210452

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '925d300b206c'
down_revision = 'd54bd33a82a0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('create sequence global_annotation_id_seq start with 1 increment by 1 nocache nocycle')
    op.execute('create sequence global_letter_annotation_id_seq start with 1 increment by 1 nocache nocycle')
    op.create_table('global_annotation',
    sa.Column('id', sa.Integer(), server_default=sa.text('global_annotation_id_seq.nextval'), nullable=False),
    sa.Column('user_id', sa.String(length=50), nullable=False),
    sa.Column('species_id', sa.Integer(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['species_id'], ['global_species.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('global_letter_annotation',
    sa.Column('id', sa.Integer(), server_default=sa.text('global_letter_annotation_id_seq.nextval'), nullable=False),
    sa.Column('annotation_id', sa.Integer(), nullable=False),
    sa.Column('letter_id', sa.Integer(), nullable=True),
    sa.Column('annotation', sa.Integer(), nullable=False),
    sa.Column('x1', sa.Float(), nullable=True),
    sa.Column('x2', sa.Float(), nullable=True),
    sa.Column('y1', sa.Float(), nullable=True),
    sa.Column('y2', sa.Float(), nullable=True),
    sa.Column('notes', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['annotation_id'], ['global_annotation.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('global_letter_annotation')
    op.drop_table('global_annotation')
    op.execute(sa.schema.DropSequence(sa.Sequence('global_letter_annotation_id_seq')))
    op.execute(sa.schema.DropSequence(sa.Sequence('global_annotation_id_seq')))
    # ### end Alembic commands ###
