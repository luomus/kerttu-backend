"""empty message

Revision ID: 39abdc573db8
Revises: 0aa89e8b2c9a
Create Date: 2024-04-30 12:36:20.225984

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '39abdc573db8'
down_revision = '0aa89e8b2c9a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_recording', schema=None) as batch_op:
        batch_op.add_column(sa.Column('uploaded_by', sa.String(length=50), nullable=True))
        batch_op.add_column(sa.Column('uploaded_at', sa.DateTime(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_recording', schema=None) as batch_op:
        batch_op.drop_column('uploaded_at')
        batch_op.drop_column('uploaded_by')

    # ### end Alembic commands ###
