"""empty message

Revision ID: 4aa91eac7640
Revises: 3a9cd035bd47
Create Date: 2023-06-15 15:12:25.321178

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4aa91eac7640'
down_revision = '3a9cd035bd47'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_recording', schema=None) as batch_op:
        batch_op.alter_column('original_recording_name',
               existing_type=sa.VARCHAR(length=80),
               type_=sa.String(length=100),
               existing_nullable=True)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('global_recording', schema=None) as batch_op:
        batch_op.alter_column('original_recording_name',
               existing_type=sa.String(length=100),
               type_=sa.VARCHAR(length=80),
               existing_nullable=True)

    # ### end Alembic commands ###
