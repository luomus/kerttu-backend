"""empty message

Revision ID: 0be4078b6a96
Revises: 1623e5c9e791
Create Date: 2021-03-15 14:18:39.744857

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0be4078b6a96'
down_revision = '1623e5c9e791'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('letter_annotation', sa.Column('template_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'letter_annotation', 'letter_template', ['template_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'letter_annotation', type_='foreignkey')
    op.drop_column('letter_annotation', 'template_id')
    # ### end Alembic commands ###
