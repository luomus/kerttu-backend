# Kerttu backend #

Backend for Kerttu service.

## Documentation
- [Wiki page](https://wiki.helsinki.fi/display/luomusict/Kerttu+backend)
- [Api documentation](doc/kerttu-api.md)

## Development

1. Create Python virtualenv and install requirements

        python3.10 -m venv env
        source env/bin/activate
        pip install -r requirements.txt

2. Install Oracle Instant Client if not installed

3. Fill in environment variables to file ".env". You can use file "env-template" as a reference.

4. Run development server

        flask run

### Database

When changing database models, run database migrations
```bash
flask db migrate
flask db upgrade
```
Before the upgrade command, check that the migration file (located in migrations/versions) is correct and edit if not.

Undo latest migration with
```bash
flask db downgrade
```

### Scripts

Scripts folder contains various scripts:
- load_audio_files, load_letter_data, load_annotation_data: these three scripts were originally used to load letter data to database from text files
- test: runs tests in tests folder

Run scripts with
```bash
flask <script_name>
```

### Tests

Run tests with
```bash
flask test # all tests
flask test <test_file_name> # one file
flask test <test_file_name>.<test_class_name>.<test_name> # one test
```

## Deployment

1. Follow development instructions to download the project and install requirements

2. Create a systemd Unit File /etc/systemd/system/kerttu-backend.service

        [Unit]
        ...

        [Service]
        PIDFile=<pid file>
        User=xxxxx
        Group=xxxxx
        WorkingDirectory=<project directory>

        ExecStart=<project directory>/env/bin/gunicorn --name kerttu-backend --pid <pid file> --bind 0.0.0.0:8080 wsgi:app
        ExecReload=/bin/kill -s HUP $MAINPID
        ExecStop=/bin/kill -s TERM $MAINPID
        PrivateTmp=true

        [Install]
        ...

3.	Start or stop server

        sudo systemctl start/restart/status/stop kerttu-backend
