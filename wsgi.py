import logging
from logging.handlers import SMTPHandler
from ratelimitingfilter import RateLimitingFilter
import os
from dotenv import load_dotenv

load_dotenv('.env')

from app import app

# Add logging handlers

formatter = logging.Formatter(
    '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
)

# Add file handler

os.makedirs(app.config['LOG_FOLDER'], exist_ok=True)
handler = logging.FileHandler(os.path.join(app.config['LOG_FOLDER'], 'error.log'))
handler.setLevel(logging.ERROR)
handler.setFormatter(formatter)
app.logger.addHandler(handler)

# Add email handler

mail_handler = SMTPHandler(
    mailhost=app.config['EMAIL_HOST'],
    fromaddr=app.config['SERVER_EMAIL'],
    toaddrs=[app.config['ADMIN_EMAIL']],
    subject='Kerttu Backend Error'
)

mail_handler.setLevel(logging.ERROR)
mail_handler.setFormatter(formatter)
mail_handler.addFilter(
    RateLimitingFilter(rate=1, per=60 * 1, burst=1, match='auto')
)
app.logger.addHandler(mail_handler)
