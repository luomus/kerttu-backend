import os
import cx_Oracle


class Config(object):
    dsn = cx_Oracle.makedsn(
        os.environ['ORACLE_HOST'],
        os.environ['ORACLE_PORT'],
        service_name=os.environ['ORACLE_SERVICE_NAME']
    )
    SQLALCHEMY_DATABASE_URI = 'oracle://{}:{}@{}'.format(
        os.environ['KERTTU_ORACLE_USER'],
        os.environ['KERTTU_ORACLE_PASSWORD'],
        dsn
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    CACHE_TYPE = 'memcached'
    CACHE_MEMCACHED_SERVERS = ['127.0.0.1:11211']
    CACHE_DEFAULT_TIMEOUT = 5 * 60 * 60
    CACHE_KEY_PREFIX = 'kerttu'

    TMP_FOLDER = 'tmp'
    LOG_FOLDER = 'logs'

    MAX_CONTENT_LENGTH = 10 * 1024 * 1024 # 10 Mb

    IMAGE_LAJI_URL = 'https://image.laji.fi'

    LAJIAPI_URL = os.environ['KERTTU_LAJIAPI_URL']
    LAJIAPI_TOKEN = os.environ['KERTTU_LAJIAPI_TOKEN']

    LAJISTORE_URL = os.environ['KERTTU_LAJISTORE_URL']
    LAJISTORE_USER = os.environ['KERTTU_LAJISTORE_USER']
    LAJISTORE_PASSWORD = os.environ['KERTTU_LAJISTORE_PASSWORD']
    LAJISTORE_PAGE_SIZE = 100

    MEDIA_API_URL = os.environ['KERTTU_MEDIA_API_URL']
    MEDIA_API_USER = os.environ['KERTTU_MEDIA_API_USER']
    MEDIA_API_PASSWORD = os.environ['KERTTU_MEDIA_API_PASSWORD']

    AUTH_USER = os.environ['KERTTU_AUTH_USER']
    AUTH_PASSWORD = os.environ['KERTTU_AUTH_PASSWORD']

    EMAIL_HOST = '127.0.0.1'
    SERVER_EMAIL = os.environ.get('KERTTU_SERVER_EMAIL')
    ADMIN_EMAIL = os.environ.get('KERTTU_ADMIN_EMAIL')

    DEFAULT_ANNOTATIONS_MADE_BY_USER = os.environ.get('DEFAULT_ANNOTATIONS_MADE_BY_USER')

    UPLOAD_ALLOWED_EXTENSIONS = ['.mp3', '.aac']

    AUDIO_DURATION = 60 # duration of Kerttu audio files

    # parameters for getting next letter template/candidate
    TARGET_ANNOTATION_COUNT = 100  # target number of annotations for a template per user
    TARGET_USER_COUNT = 5  # target number of users per template
    TARGET_PROPORTION = 0.25  # target propotion of candidates that all users train
    WEIGHTING_PARAM = 3  # determines how important target correlation is compared to the desired overlap between users when selecting a next candidate

    # parameters for getting next recording
    REQUIRED_NUMBER_OF_LETTER_ANNOTATIONS = 20  # user has to annotate this much letters in order to annotate recordings
    NEXT_RECORDING_MAX_TRIES = 10
    RECORDING_OVERLAP_PARAM = 0.2  # determines how much overlap recording annotations have

    # parameters for Kerttu global
    GLOBAL_SPECIES_LOCK_EXPIRES_IN_DAYS = 1
    GLOBAL_REQUIRED_TEMPLATE_COUNT = 10

    GLOBAL_MAX_NBR_OF_RESULTS = 1000

    # some routes are disabled on default
    ENABLE_OLD_ROUTES = False
    ENABLE_GLOBAL_PERMISSIONS_ROUTE = False


class TestingConfig(Config):
    TESTING = True

    SQLALCHEMY_DATABASE_URI = 'oracle://{}:{}@{}'.format(
        os.environ.get('KERTTU_TESTING_ORACLE_USER', ''),
        os.environ.get('KERTTU_TESTING_ORACLE_PASSWORD', ''),
        Config.dsn
    )

    CACHE_TYPE = 'null'

    AUTH_USER = 'test_user'
    AUTH_PASSWORD = 'test_password'

    # tests assume these parameters
    TARGET_ANNOTATION_COUNT = 100  # target number of annotations for a template per user
    TARGET_USER_COUNT = 5  # target number of users per template
    TARGET_PROPORTION = 0.25  # target propotion of candidates that all users train
    WEIGHTING_PARAM = 3  # determines how important target correlation is compared to the desired overlap between users when selecting a next candidate

    REQUIRED_NUMBER_OF_LETTER_ANNOTATIONS = 0
